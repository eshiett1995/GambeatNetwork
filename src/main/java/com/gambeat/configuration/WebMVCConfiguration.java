package com.gambeat.configuration;

import com.gambeat.site.interceptors.LockedPageInterceptor;
import com.gambeat.site.interceptors.OtherPagesInterceptor;
import com.gambeat.site.interceptors.WelcomePageInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Oto-obong on 31/10/2017.
 */

@Configuration
public class WebMVCConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    OtherPagesInterceptor otherPagesInterceptor;

    @Autowired
    WelcomePageInterceptor welcomePageInterceptor;

    @Autowired
    LockedPageInterceptor lockedPageInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(otherPagesInterceptor).excludePathPatterns("/welcome")
                .excludePathPatterns("/welcome/login")
                .excludePathPatterns("/welcome/signup")
                .excludePathPatterns("/signout")
                .excludePathPatterns("/locked")
                .excludePathPatterns("/locked/unlock")
                .excludePathPatterns("/gameserver/login")
                .excludePathPatterns("/gameserver/money")
                .excludePathPatterns("/gameserver/**")
                .excludePathPatterns("/chatserver/**")
                .excludePathPatterns("/admin/rest/**")
                .excludePathPatterns("/api/third-party/**")
                .excludePathPatterns("/ws");



        registry.addInterceptor(welcomePageInterceptor).addPathPatterns("/welcome")
                .excludePathPatterns("/ws")
                .excludePathPatterns("/api/third-party/**")
                .excludePathPatterns("/admin/rest/**");

        registry.addInterceptor(lockedPageInterceptor).addPathPatterns("/locked")
                .addPathPatterns("/locked/unlock")
                .excludePathPatterns("/api/third-party/**")
                .excludePathPatterns("/ws")
                .excludePathPatterns("/admin/rest/**");
    }


    @Primary
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        return bCryptPasswordEncoder;
    }
}
