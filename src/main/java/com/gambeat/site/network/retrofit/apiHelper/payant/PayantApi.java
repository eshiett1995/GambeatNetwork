package com.gambeat.site.network.retrofit.apiHelper.payant;

import com.gambeat.site.entities.User;
import com.gambeat.site.payment.payant.models.request.WithdrawObject;
import com.gambeat.site.payment.payant.models.response.WithdrawResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.List;

public interface PayantApi {

    @POST("/wallets/withdraw/{reference_code}")
    public Call<WithdrawResponse> WithdrawFromWallet(@Path("reference_code") String reference_code, @Body WithdrawObject withdrawObject);
}
