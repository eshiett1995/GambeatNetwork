package com.gambeat.site.network.retrofit;

import com.gambeat.site.payment.payant.models.response.WithdrawResponse;

public interface RetrofitCallback<T> {

    void onCompleted(Throwable t, T result,boolean status);
}
