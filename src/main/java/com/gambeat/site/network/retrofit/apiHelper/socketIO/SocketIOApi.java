package com.gambeat.site.network.retrofit.apiHelper.socketIO;

import com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.third_party.jsonmodel.SocketIOFriendRequestModel;
import com.gambeat.site.controllers.third_party.jsonmodel.SocketIOMessageModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SocketIOApi {

    @POST("/message")
    public Call<ResponseModel> SendMessageToSocketIOServer(@Body SocketIOMessageModel socketIOMessageModel);

    @POST("/friend-request")
    public Call<ResponseModel> SendFriendRequestToSocketIOServer(@Body SocketIOFriendRequestModel socketIOFriendRequestModel);

    //@POST("/general-notification")
    //public Call<ResponseModel> SendFriendRequestToSocketIOServer(@Body SocketIOFriendRequestModel socketIOFriendRequestModel);


}
