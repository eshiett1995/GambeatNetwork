package com.gambeat.site.network.retrofit.apiHelper.firebase;


import com.gambeat.site.pushnotification.firebase.models.request.FirebaseNotificationModel;
import com.gambeat.site.pushnotification.firebase.models.response.FirebaseResponseModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface FirebaseApi {

    @POST("/v1/projects/gambeat-network/messages:send")
    Call<FirebaseResponseModel> SendGenericNotification(@Body FirebaseNotificationModel firebaseNotificationModel);


}
