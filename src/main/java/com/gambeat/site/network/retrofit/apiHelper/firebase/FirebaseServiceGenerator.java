package com.gambeat.site.network.retrofit.apiHelper.firebase;

import com.gambeat.site.pushnotification.firebase.service.DefaultFirebaseService;
import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FirebaseServiceGenerator {

    private static final String BASE_URL = "https://fcm.googleapis.com";

    private static Retrofit.Builder builder
            = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create( new GsonBuilder()
                    .setLenient()
                    .create()));

    private static Retrofit retrofit = builder.build();

    private static OkHttpClient.Builder httpClient
            = new OkHttpClient.Builder();

    public static <S> S createService(Class<S> FirebaseApi) {

       // String c = new DefaultFirebaseService().getAccessToken();

            httpClient.interceptors().clear();
            httpClient.addInterceptor( chain -> {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header("Authorization","Bearer " + new DefaultFirebaseService().getAccessToken())
                        .build();
                return chain.proceed(request);
            });
            builder.client(httpClient.build());
            retrofit = builder.build();

        return retrofit.create(FirebaseApi);
    }
}
