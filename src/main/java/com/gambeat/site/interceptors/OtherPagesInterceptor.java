package com.gambeat.site.interceptors;

import com.gambeat.site.controllers.SwitchViewController;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Oto-obong on 24/08/2017.
 */

@Component
public class OtherPagesInterceptor extends HandlerInterceptorAdapter {


    @Autowired
    DefaultCookieService defaultCookieService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;


    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {


        String patch = httpServletRequest.getRequestURI();

        if(defaultCookieService.hasValidCookie(httpServletRequest)){

            if (defaultUserDAOService.isLocked(this.defaultCookieService.getUser("gambeat", httpServletRequest))){



                httpServletResponse.sendRedirect("/locked");

                return false;

            }else{

                return true;

            }

        }else{

            httpServletResponse.sendRedirect("/welcome");

            return false;

        }

    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
