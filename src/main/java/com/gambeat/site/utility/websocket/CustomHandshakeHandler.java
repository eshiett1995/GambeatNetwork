package com.gambeat.site.utility.websocket;

import com.gambeat.site.entities.User;
import com.gambeat.site.services.CookieService;
import com.gambeat.site.services.UserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Map;
import java.util.UUID;

public class CustomHandshakeHandler extends DefaultHandshakeHandler {

    @Autowired
    CookieService cookieService;

    @Autowired
    UserDAOService userDAOService;

    // Custom class for storing principal
    @Override
    protected Principal determineUser(ServerHttpRequest request,
                                      WebSocketHandler wsHandler,
                                      Map<String, Object> attributes) {
        HttpSession session = null;

        Cookie cookie = null;

        ServletServerHttpRequest httpServletRequest = (ServletServerHttpRequest) request;

        HttpSession x = httpServletRequest.getServletRequest().getSession();

        for (Cookie foundCookie: httpServletRequest.getServletRequest().getCookies()) {

            if(foundCookie.getName().equalsIgnoreCase("gambeat")){

                // Gets the session ID from the request
                session = httpServletRequest.getServletRequest().getSession(true);

                attributes.put("username", session.getAttribute(foundCookie.getValue()));

                cookie = foundCookie;

            }

        }

        //return new StompPrincipal(UUID.randomUUID().toString())

        // Generate principal with UUID as name
        return new StompPrincipal((String) session.getAttribute(cookie.getValue()));
    }
}

