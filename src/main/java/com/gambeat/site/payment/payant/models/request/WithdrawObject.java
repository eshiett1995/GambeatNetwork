package com.gambeat.site.payment.payant.models.request;

public class WithdrawObject {

    public WithdrawObject(String settlement_bank, String account_number, String amount, String passcode) {

        this.settlement_bank = settlement_bank;

        this.account_number = account_number;

        this.amount = amount;

        this.passcode = passcode;

    }

    private String settlement_bank;

    private String account_number;

    private String amount;

    private String passcode;

    public String getSettlement_bank() {

        return settlement_bank;
    }

    public void setSettlement_bank(String settlement_bank) {

        this.settlement_bank = settlement_bank;

    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }
}
