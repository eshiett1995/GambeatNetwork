package com.gambeat.site.payment.payant.models.response;

public class WithdrawResponse {


       private String status;

       private String message;

       private TransactionReference data;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TransactionReference getData() {
        return data;
    }

    public void setData(TransactionReference data) {
        this.data = data;
    }
}
