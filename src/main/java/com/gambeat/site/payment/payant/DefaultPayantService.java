package com.gambeat.site.payment.payant;

import com.gambeat.site.network.retrofit.RetrofitCallback;
import com.gambeat.site.network.retrofit.apiHelper.payant.PayantApi;
import com.gambeat.site.network.retrofit.apiHelper.payant.PayantServiceGenerator;
import com.gambeat.site.payment.payant.models.PayantConstants;
import com.gambeat.site.payment.payant.models.request.WithdrawObject;
import com.gambeat.site.payment.payant.models.response.WithdrawResponse;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

@Service
public class DefaultPayantService implements PayantService {


    //Transfer transfer = new Transfer("c164ec46390d3324d36d64e2c6f0f401c99451755b9c2e98aab466dd", Implementation.DEMO);

    private PayantApi payantApi = PayantServiceGenerator.createService(PayantApi.class, PayantConstants.PAYANT_PRIVATE_KEY);


    @Override
    public Response<WithdrawResponse> makeWalletWithdrawal(String bankCode, String accountNumber, String amount) throws IOException {

        Call<WithdrawResponse> callAsync = payantApi.WithdrawFromWallet(PayantConstants.WALLET_PASSCODE, new WithdrawObject(bankCode,accountNumber,amount, PayantConstants.WALLET_PASSCODE));

        Response<WithdrawResponse> response =  callAsync.execute();

        return response;
    }
}
