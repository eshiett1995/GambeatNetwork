package com.gambeat.site.payment.payant;

import com.gambeat.site.payment.payant.models.response.WithdrawResponse;
import retrofit2.Response;

import java.io.IOException;

public interface PayantService {


    Response<WithdrawResponse> makeWalletWithdrawal(String bankCode, String accountNumber, String amount) throws IOException;


}
