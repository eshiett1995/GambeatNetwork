package com.gambeat.site.payment.payant.models.response;

public class TransactionReference {

     private String company_id;

     private String wallet_id;

     private String wallet_reference_code;

     private String currency;

     private String amount;

     private String reference_code;

     private String type;

     private String channel;

     private String status;

     private String disburse_recipients;

     private String Stringcreated_at;

     private String updated_at;

     private int id;

     private String transaction_date;

     private String payment_id;

     private String referrer;

     private String gateway_response;

     private String disburse_status;

     private String disburse_ref;

     private String disburse_response;

     private String disburse_recipients_response;

     private String settlement_bank;

     private String account_number;

     private String account_name;


    public String getCompany_id() {

        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(String wallet_id) {
        this.wallet_id = wallet_id;
    }

    public String getWallet_reference_code() {
        return wallet_reference_code;
    }

    public void setWallet_reference_code(String wallet_reference_code) {
        this.wallet_reference_code = wallet_reference_code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReference_code() {
        return reference_code;
    }

    public void setReference_code(String reference_code) {
        this.reference_code = reference_code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDisburse_recipients() {
        return disburse_recipients;
    }

    public void setDisburse_recipients(String disburse_recipients) {
        this.disburse_recipients = disburse_recipients;
    }

    public String getStringcreated_at() {
        return Stringcreated_at;
    }

    public void setStringcreated_at(String stringcreated_at) {
        Stringcreated_at = stringcreated_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(String transaction_date) {
        this.transaction_date = transaction_date;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public String getGateway_response() {
        return gateway_response;
    }

    public void setGateway_response(String gateway_response) {
        this.gateway_response = gateway_response;
    }

    public String getDisburse_status() {
        return disburse_status;
    }

    public void setDisburse_status(String disburse_status) {
        this.disburse_status = disburse_status;
    }

    public String getDisburse_ref() {
        return disburse_ref;
    }

    public void setDisburse_ref(String disburse_ref) {
        this.disburse_ref = disburse_ref;
    }

    public String getDisburse_response() {
        return disburse_response;
    }

    public void setDisburse_response(String disburse_response) {
        this.disburse_response = disburse_response;
    }

    public String getDisburse_recipients_response() {
        return disburse_recipients_response;
    }

    public void setDisburse_recipients_response(String disburse_recipients_response) {
        this.disburse_recipients_response = disburse_recipients_response;
    }

    public String getSettlement_bank() {
        return settlement_bank;
    }

    public void setSettlement_bank(String settlement_bank) {
        this.settlement_bank = settlement_bank;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }
}
