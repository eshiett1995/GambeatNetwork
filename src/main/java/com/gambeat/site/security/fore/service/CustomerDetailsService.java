package com.gambeat.site.security.fore.service;

import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class CustomerDetailsService implements UserDetailsService {

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

       User user = defaultUserDAOService.getByUsername(username);

        if (user == null) {

            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));

        } else {

            return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), AuthorityUtils.createAuthorityList("ROLE_USER"));


        }
    }
}
