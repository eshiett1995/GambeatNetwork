package com.gambeat.site.security.fore.service;

import com.gambeat.site.services.implementation.DefaultCookieService;
import com.sun.javafx.binding.StringFormatter;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static com.gambeat.site.security.SecurityConstants.HEADER;
import static com.gambeat.site.security.SecurityConstants.SECRET;
import static com.gambeat.site.security.SecurityConstants.TOKEN_PREFIX;

public class JWTAuthorizationFilter  extends OncePerRequestFilter {
    
    private final CustomerDetailsService customerDetailsService;

    @Autowired
    DefaultCookieService defaultCookieService;
    
    
    public JWTAuthorizationFilter(AuthenticationManager authenticationManager, CustomerDetailsService customerDetailsService) {

       // super(authenticationManager);

        this.customerDetailsService = customerDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        String header = request.getHeader(HEADER);

        if(Objects.isNull(header) || !header.startsWith(TOKEN_PREFIX)){

            if(request.getRequestURI().equalsIgnoreCase("/welcome/signup") ||
               request.getRequestURI().equalsIgnoreCase("/welcome/login")  ||
               request.getRequestURI().contains("/websocket-example")  ||
               request.getRequestURI().contains("/api/third-party")){

                chain.doFilter(request,response);


            }else{

                return;

            }

        }

        UsernamePasswordAuthenticationToken usernamePasswordAuth = getAuthenticationToken(request);

        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuth);

        chain.doFilter(request,response);

    }

    private UsernamePasswordAuthenticationToken getAuthenticationToken(HttpServletRequest request){

        String token = request.getHeader(HEADER);

        if(Objects.isNull(token)) return null;

        String username = Jwts.parser().setSigningKey(SECRET)
                .parseClaimsJws(token.replace(TOKEN_PREFIX,""))
                .getBody()
                .getSubject();


        UserDetails userDetails = customerDetailsService.loadUserByUsername(username);

        return username != null ? new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities()) : null;
    }
}
