package com.gambeat.site.security;

public class SecurityConstants {

    public static final String SECRET = "P@ssw0rd@2018";

    public static final  String HEADER = "Authorization";

    public static final  String TOKEN_PREFIX = "Bearer ";

    public static final  long EXPIRATION_TIME = 864_000_000L;
}
