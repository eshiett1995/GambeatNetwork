package com.gambeat.site.pushnotification.firebase.models.request;

public class FirebaseNotificationModel {

    private Message message;

    public Message getMessage() {
        return message;
    }

    public FirebaseNotificationModel setMessage(Message message) {

        this.message = message;

        return this;

    }
}
