package com.gambeat.site.pushnotification.firebase.service;

import com.gambeat.site.entities.*;
import com.gambeat.site.pushnotification.firebase.models.request.FirebaseNotificationModel;
import com.gambeat.site.pushnotification.firebase.models.response.FirebaseResponseModel;
import retrofit2.Response;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface FirebaseService {

    String getAccessToken() throws IOException;

    Response<FirebaseResponseModel> sendNotification(FirebaseNotificationModel firebaseNotificationModel);

    FirebaseNotificationModel composeFirebaseNotificationModel(String token, String title, String body, Map<String, String> data);

    FirebaseNotificationModel composeFirebaseNotificationModel(String token, String title, String body);

    List<FirebaseNotificationModel> composeCompetitionStartedNotification(User user, Competition competition);

    List<FirebaseNotificationModel> composeMatchLostNotification(User loser, User winner, Competition competition);

    List<FirebaseNotificationModel> composeMatchWonNotification(User winner, User loser, Competition competition);

    List<FirebaseNotificationModel> composeFriendKnockedOutOfCompetitionNotification(User user, User loser, Competition competition);

    List<FirebaseNotificationModel> composeFriendWonCompetitionNotification(User user, User winner, Competition competition);

    List<FirebaseNotificationModel> composeStageStartedNotification(User user, User opponent, Gamification gamification);

    List<FirebaseNotificationModel> composeFriendRequestAcceptedNotification(User user, User receiver, FriendRequest friendRequest);

    List<FirebaseNotificationModel> composeTransactionNotification(User user, double amount, Transactions transactions);

    List<FirebaseNotificationModel> composeCompetitionEnteredNotification(User user, Competition competition);

    List<FirebaseNotificationModel> composeCompetitionRegistrationDeniedNotification(User user, String reason);

    List<FirebaseNotificationModel> composeCompetitionCouldNotStartNotification(User user, Competition competition);

    List<FirebaseNotificationModel> composeFriendRequestGottenNotification(User user, User receiver, FriendRequest friendRequest);
}
