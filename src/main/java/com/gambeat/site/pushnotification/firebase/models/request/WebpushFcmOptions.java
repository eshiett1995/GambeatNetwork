package com.gambeat.site.pushnotification.firebase.models.request;

public class WebpushFcmOptions {

    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
