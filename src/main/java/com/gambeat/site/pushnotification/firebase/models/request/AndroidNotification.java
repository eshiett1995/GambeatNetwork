package com.gambeat.site.pushnotification.firebase.models.request;

import java.util.ArrayList;
import java.util.List;

public class AndroidNotification {

    private  String title;

    private String body;

    private String icon;

    private String color;

    private String sound;

    private String tag;

    private String click_action;

    private String body_loc_key;

    private List<String> body_loc_args = new ArrayList<>();

    private String title_loc_key;

    private List<String> title_loc_args = new ArrayList<>();

    private String channel_id;
}
