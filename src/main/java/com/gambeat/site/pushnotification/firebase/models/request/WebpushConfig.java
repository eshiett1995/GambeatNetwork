package com.gambeat.site.pushnotification.firebase.models.request;

import java.util.Map;

public class WebpushConfig {

    private Map<String, Object> headers;

    private Map<String, Object> data;

    private Notification notification;

    private WebpushFcmOptions fcm_options;

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public WebpushFcmOptions getFcm_options() {
        return fcm_options;
    }

    public void setFcm_options(WebpushFcmOptions fcm_options) {
        this.fcm_options = fcm_options;
    }
}
