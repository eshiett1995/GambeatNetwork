package com.gambeat.site.pushnotification.firebase.models.request;

import java.util.Map;

public class ApnsConfig {

    private Map<String, String> headers;

    private Object payload;

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
