package com.gambeat.site.pushnotification.firebase.models.request;

public class Notification {

    private String title;

    private String body;

    public String getTitle() {

        return title;

    }

    public Notification setTitle(String title) {

        this.title = title;

        return this;
    }

    public String getBody() {
        return body;
    }

    public Notification setBody(String body) {

        this.body = body;

        return this;
    }
}
