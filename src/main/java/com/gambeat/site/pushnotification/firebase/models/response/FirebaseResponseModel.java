package com.gambeat.site.pushnotification.firebase.models.response;

import java.util.ArrayList;
import java.util.List;

public class FirebaseResponseModel {

    private int multicast_id;

    private int success;

    private int failure;

    private List<Result> results = new ArrayList<>();

    public int getMulticast_id() {
        return multicast_id;
    }

    public void setMulticast_id(int multicast_id) {
        this.multicast_id = multicast_id;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFailure() {
        return failure;
    }

    public void setFailure(int failure) {
        this.failure = failure;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
