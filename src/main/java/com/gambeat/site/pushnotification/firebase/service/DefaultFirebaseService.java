package com.gambeat.site.pushnotification.firebase.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gambeat.site.entities.*;
import com.gambeat.site.network.retrofit.apiHelper.firebase.FirebaseApi;
import com.gambeat.site.network.retrofit.apiHelper.firebase.FirebaseServiceGenerator;
import com.gambeat.site.pushnotification.firebase.models.request.FirebaseNotificationModel;
import com.gambeat.site.pushnotification.firebase.models.request.Message;
import com.gambeat.site.pushnotification.firebase.models.request.Notification;
import com.gambeat.site.pushnotification.firebase.models.response.FirebaseResponseModel;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import static com.gambeat.site.entities.Transactions.TRANSACTIONTYPE.*;

@Service
public class DefaultFirebaseService implements FirebaseService {

    private static final String MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";

    private static final String[] SCOPES = { MESSAGING_SCOPE };


    private static final String FIREBASE_SERVER_FILE = "C:\\Users\\Java\\IdeaProjects\\GambeatNetwork (gitlab)-WithMongo\\src\\main\\resources\\firebase\\gambeat-network-firebase-adminsdk-6tvh0-5fcd316fe1.json" ;

    private FirebaseApi firebaseApi = FirebaseServiceGenerator.createService(FirebaseApi.class);


    public String getAccessToken() {
        GoogleCredential googleCredential = null;
        try {
            googleCredential = GoogleCredential
                    .fromStream(new FileInputStream(FIREBASE_SERVER_FILE))
                    .createScoped(Arrays.asList(SCOPES));
        } catch (IOException e) {

            e.printStackTrace();

        }
        try {
            googleCredential.refreshToken();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return googleCredential.getAccessToken();
    }

    @Override
    public Response<FirebaseResponseModel> sendNotification(FirebaseNotificationModel firebaseNotificationModel) {

        Call<FirebaseResponseModel> callAsync = firebaseApi.SendGenericNotification(firebaseNotificationModel);

        Response<FirebaseResponseModel> response = null;
        try {

            response = callAsync.execute();

        } catch (IOException e) {

            e.printStackTrace();
        }

        return response;
    }

    @Override
    public FirebaseNotificationModel composeFirebaseNotificationModel(String token, String title, String body, Map<String, String> data) {

        FirebaseNotificationModel firebaseNotificationModel = new FirebaseNotificationModel();

        firebaseNotificationModel.setMessage(
                new Message().setToken(token)
                        .setNotification(new Notification().setTitle(title).setBody(body))
                .setData(data)

        );

        return firebaseNotificationModel;
    }

    @Override
    public FirebaseNotificationModel composeFirebaseNotificationModel(String token, String title, String body) {

        FirebaseNotificationModel firebaseNotificationModel = new FirebaseNotificationModel();

        firebaseNotificationModel.setMessage(
                new Message().setToken(token)
                        .setNotification(new Notification().setTitle(title).setBody(body))
                        );

        return firebaseNotificationModel;
    }

    @Override
    public List<FirebaseNotificationModel> composeCompetitionStartedNotification(User user, Competition competition) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Competition Started";

        String body = String.format("Hello s%, the s% competition has started", user.getUserName(), competition.getCompetitionName());

        Map<String, String> data = new ObjectMapper().convertValue(competition, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeMatchLostNotification(User loser, User winner, Competition competition) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Loser";

        String body = String.format("You have been eliminated from the s% competition, after being defeated by s%", competition.getCompetitionName(), winner.getUserName());

                Map<String, String> data = new ObjectMapper().convertValue(competition, Map.class);

        for (String token: loser.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeMatchWonNotification(User winner, User loser, Competition competition) {
        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Winner";

        String body = String.format("You have progressed to the next stage after defeating s% in the s% competition", loser.getUserName(), competition.getCompetitionName());

        Map<String, String> data = new ObjectMapper().convertValue(competition, Map.class);

        for (String token: winner.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeFriendKnockedOutOfCompetitionNotification(User user, User loser, Competition competition) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "The Black Sheep";

        String body = String.format("s% has been eliminated from the s% competition", loser.getUserName(), competition.getCompetitionName() );

        Map<String, String> data = new ObjectMapper().convertValue(competition, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeFriendWonCompetitionNotification(User user, User winner, Competition competition) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Friend Of A Winner";

        String body = String.format("s% has won the s% competition, congrats, you are a friend of a winner", winner.getUserName(), competition.getCompetitionName());

        Map<String, String> data = new ObjectMapper().convertValue(competition, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeStageStartedNotification(User user, User opponent, Gamification gamification) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Gamification";

        String body = String.format("You have been paired against s% in the s% competition", opponent.getUserName(), gamification.getCompetition().getCompetitionName());

        Map<String, String> data = new ObjectMapper().convertValue(gamification, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeFriendRequestAcceptedNotification(User user, User receiver, FriendRequest friendRequest) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Friend Request Accepted";

        String body = String.format("s% has accepted your friend request", receiver.getUserName());

        Map<String, String> data = new ObjectMapper().convertValue(friendRequest, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeTransactionNotification(User user, double amount, Transactions transactions) {

        String notificationHeader = "";

        switch (transactions.getTransactiontype()){

            case DEPOSIT:

                notificationHeader = String.format("Wallet (s%)", "Deposit");

                break;

            case WITHDRAW:

                notificationHeader = String.format("Wallet (s%)", "Withdrawal");

                break;

            case LOSS:

                notificationHeader = String.format("Wallet (s%)", "Loss");

                break;

            case WIN:

                notificationHeader = String.format("Wallet (s%)", "Win");

                break;

            case DEBIT:

                notificationHeader = String.format("Wallet (s%)", "Debit");

                break;

            case REVERSAL:

                notificationHeader = String.format("Wallet (s%)", "Reversal");

                break;

            default :
                notificationHeader = "Wallet";
        }


        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = notificationHeader;

        String body = transactions.getDetail();

        Map<String, String> data = new ObjectMapper().convertValue(transactions, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeCompetitionEnteredNotification(User user, Competition competition) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Competition Entry";

        String body = String.format("You have successfully entered into the s% competition. Best of luck", competition.getCompetitionName());

        Map<String, String> data = new ObjectMapper().convertValue(competition, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeCompetitionRegistrationDeniedNotification(User user, String reason) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Competition Registration Denied";

        String body = reason;

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeCompetitionCouldNotStartNotification(User user, Competition competition) {

        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Competition Could Not Start";

        String body = "Competition could not start as it does'nt have the minimum number of contestants required";

        Map<String, String> data = new ObjectMapper().convertValue(competition, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;
    }

    @Override
    public List<FirebaseNotificationModel> composeFriendRequestGottenNotification(User user, User receiver, FriendRequest friendRequest) {


        List<FirebaseNotificationModel> firebaseNotificationModelList = new ArrayList<>();

        String title = "Friend Request";

        String body = String.format("s% has accepted your friend request", receiver.getUserName());

        Map<String, String> data = new ObjectMapper().convertValue(friendRequest, Map.class);

        for (String token: user.getFcmToken()) {

            firebaseNotificationModelList.add(this.composeFirebaseNotificationModel(token,title, body, data));

        }

        return firebaseNotificationModelList;

    }

}
