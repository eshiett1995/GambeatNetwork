package com.gambeat.site.pushnotification.firebase.models.request;

import java.util.Map;

public class Message {

    private Notification notification;

    private Map<String, String> data;

    private AndroidConfig android;

    private WebpushConfig webpush;

    private ApnsConfig apns;

    private String token;

    private String topic;

    private String condition;

    public Notification getNotification() {
        return notification;

    }

    public Message setNotification(Notification notification) {

        this.notification = notification;

        return this;
    }

    public Map<String, String> getData() {

        return data;

    }

    public Message setData(Map<String, String> data) {

        this.data = data;

        return this;

    }

    public String getToken() {
        return token;
    }

    public Message setToken(String token) {
        this.token = token;
        return this;
    }

    public AndroidConfig getAndroid() {
        return android;
    }

    public Message setAndroid(AndroidConfig android) {
        this.android = android;
        return this;
    }

    public WebpushConfig getWebpush() {
        return webpush;
    }

    public Message setWebpush(WebpushConfig webpush) {

        this.webpush = webpush;

        return this;

    }

    public ApnsConfig getApns() {
        return apns;
    }

    public Message setApns(ApnsConfig apns) {

        this.apns = apns;

        return this;

    }

    public String getTopic() {
        return topic;
    }

    public Message setTopic(String topic) {

        this.topic = topic;
        return this;

    }

    public String getCondition() {
        return condition;
    }

    public Message setCondition(String condition) {

        this.condition = condition;

        return this;

        }
}
