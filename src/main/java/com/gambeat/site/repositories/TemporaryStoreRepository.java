package com.gambeat.site.repositories;

import com.gambeat.site.entities.TemporaryStore;
import com.gambeat.site.entities.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.math.BigInteger;

/**
 * Created by Oto-obong on 29/10/2017.
 */
public interface TemporaryStoreRepository extends MongoRepository<TemporaryStore,String> {

    TemporaryStore getTemporaryStoreByIdAndToken(long id, String Token);
}
