package com.gambeat.site.repositories;

import com.gambeat.site.entities.Enum.FriendRequestStatus;
import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.User;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Oto-obong on 15/08/2017.
 */


public interface FriendRequestRepository extends MongoRepository<FriendRequest, String> {

    List<FriendRequest> getFriendRequestsBySenderAndFriendRequestStatus(User user,FriendRequestStatus status);

    List<FriendRequest> getFriendRequestsByRecipientAndFriendRequestStatus(User user,FriendRequestStatus status);

    List<FriendRequest> getByRecipientOrSenderAndFriendRequestStatus(User recipient, User sender, FriendRequestStatus friendRequestStatus);

    FriendRequest getFriendRequestByRecipientAndSender(User receiver, User sender);

    Page<FriendRequest> getByRecipientOrSenderAndFriendRequestStatus(User recipient, User sender, FriendRequestStatus friendRequestStatus, Pageable pageable);

    int countAllByRecipientOrSenderAndFriendRequestStatus(User recipient, User sender, FriendRequestStatus friendRequestStatus);





}
