package com.gambeat.site.repositories;

import com.gambeat.site.entities.Punter;
import org.bson.types.ObjectId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

/**
 * Created by Oto-obong on 15/08/2017.
 */


public interface PunterRepository extends MongoRepository<Punter,String> {

}
