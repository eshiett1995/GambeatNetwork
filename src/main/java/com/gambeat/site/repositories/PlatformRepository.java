package com.gambeat.site.repositories;


import com.gambeat.site.entities.Platform;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlatformRepository extends MongoRepository<Platform, String> {

    Platform findByName(String name);

}
