package com.gambeat.site.repositories;

import com.gambeat.site.entities.Activations;
import org.bson.types.ObjectId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;


@Repository
public interface ActivationsRepository extends MongoRepository<Activations,String> {

    List<Activations> getAllByActivationTokenEquals(String token);

    Activations getFirstByActivationToken(String token);

}
