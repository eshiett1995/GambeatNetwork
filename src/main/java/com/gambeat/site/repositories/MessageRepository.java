package com.gambeat.site.repositories;

import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Oto-obong on 15/08/2017.
 */


@Repository
public interface MessageRepository extends MongoRepository<Message, String> {

    Page<Message> findMessageByReceiverAndSenderOrSenderAndReceiverOrderByDateCreatedDesc(User Receiver1, User Sender1, User Sender2, User Receiver2, Pageable pageable);



    Page<Message>  findDistinctByReceiverAndSenderIn(List<Message> messages, Pageable pageable);

   /**  @Query(value = "SELECT * FROM message WHERE id IN( SELECT max(id) FROM  message WHERE receiver = ?1 OR sender = ?2  group by receiver,sender) AND (receiver = ?1 OR sender= ?2)  \n-- #pageable\n",
            nativeQuery = true)
    Page<Message>  findDistinctMessages(User sender, User receiver, Pageable pageable);**/

    int countMessagesBySenderAndReceiverAndIsRead(User sender, User receiver, boolean IsRead);

    int countMessagesByReceiverAndIsRead(User receiver, boolean IsRead);


   /** @Query(value = "select m.*\n" +
            "from message m\n" +
            "where ?1 in (m.sender, m.receiver) and\n" +
            "      m.id = (select id\n" +
            "              from message m2\n" +
            "              where (m2.sender = m.sender and m2.receiver = m.receiver) or\n" +
            "                    (m2.receiver = m.sender and m2.sender = m.receiver)\n" +
            "              order by m2.time desc, m2.id desc\n" +
            "              limit 1\n" +
            "             )  \n-- #pageable\n",
            nativeQuery = true)
    Page<Message>  findMostRecentMessageFromConversation(User user, Pageable pageable); **/




}
