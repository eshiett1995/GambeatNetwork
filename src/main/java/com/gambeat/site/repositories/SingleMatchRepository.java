package com.gambeat.site.repositories;

import com.gambeat.site.entities.SingleMatch;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SingleMatchRepository extends MongoRepository<SingleMatch,String> {}
