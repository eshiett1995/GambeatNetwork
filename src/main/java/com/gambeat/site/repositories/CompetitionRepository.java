package com.gambeat.site.repositories;

import com.gambeat.site.entities.Competition;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Game;
import com.gambeat.site.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CompetitionRepository extends MongoRepository<Competition,String> {

    List<Competition> getAllByPuntersPlayer(User user);

    Competition findCompetitionByCompetitionName(String name);

    Competition findCompetitionByGameAndEntryFeeAndCompetitionStatus(Game game, double entryFee, Enum.CompetitionStatus competitionStatus);

}
