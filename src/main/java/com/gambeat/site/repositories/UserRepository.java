package com.gambeat.site.repositories;

import com.gambeat.site.entities.User;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

/**
 * Created by Oto-obong on 16/08/2017.
 */


@Repository
public interface UserRepository extends MongoRepository<User,String> {

    User getUserByEmail(String email);

    User getUserById(String id);

    User getUserByUserName(String userName);

    User getUserByPhoneNumber(String phoneNumber);

    Page<User> findUsersByUserNameContainingOrFirstNameContainingOrLastNameContaining(String UserName, String FirstName, String LastName, Pageable pageable);


}
