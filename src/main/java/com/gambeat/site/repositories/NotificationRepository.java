package com.gambeat.site.repositories;

import com.gambeat.site.entities.Notifications;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.math.BigInteger;

public interface NotificationRepository  extends MongoRepository<Notifications, String> {

}
