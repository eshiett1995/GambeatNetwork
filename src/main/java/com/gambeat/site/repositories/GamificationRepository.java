package com.gambeat.site.repositories;

import com.gambeat.site.entities.Gamification;
import org.bson.types.ObjectId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;

public interface GamificationRepository extends MongoRepository<Gamification, String> {

}
