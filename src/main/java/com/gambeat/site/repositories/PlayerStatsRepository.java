package com.gambeat.site.repositories;

import com.gambeat.site.entities.PlayerStats;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.math.BigInteger;

/**
 * Created by Oto-obong on 17/10/2017.
 */
public interface PlayerStatsRepository extends MongoRepository<PlayerStats, String> {

}
