package com.gambeat.site.repositories;

import com.gambeat.site.entities.Game;
import com.gambeat.site.entities.Genre;
import com.gambeat.site.entities.Platform;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GameRepository extends MongoRepository<Game, String> {

    Game findGameByName(String name);

    Page<Game> findGamesByNameContaining(String name, Pageable pageable);

    Page<Game> findGamesByPlatforms(Platform platform, Pageable pageable);

    Page<Game> findGamesByGenres(Genre genre, Pageable pageable);

    Page<Game> findGamesByNameContainingAndGenres(String name,Genre genre, Pageable pageable);

    Page<Game> findGamesByNameContainingAndPlatforms(String name,Platform platform, Pageable pageable);

    Page<Game> findGamesByPlatformsAndGenres(Platform platform, Genre genre, Pageable pageable);

    Page<Game> findGamesByNameContainingAndPlatformsAndGenres(String name, Platform platform, Genre genre, Pageable pageable);
}
