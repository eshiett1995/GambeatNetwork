package com.gambeat.site.repositories;

import com.gambeat.site.entities.Transactions;
import com.gambeat.site.entities.User;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;


@Repository
public interface TransactionRepository extends MongoRepository<Transactions,String> {

    Transactions getByIdAndAmountAndTransactiontype(String id, double amount, Transactions.TRANSACTIONTYPE transactiontype);

    Page<Transactions> getTransactionsByPlayer(User user, Pageable pageable);

}
