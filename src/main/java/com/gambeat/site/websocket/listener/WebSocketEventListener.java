package com.gambeat.site.websocket.listener;


import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.FriendRequestService;
import com.gambeat.site.services.UserDAOService;
import com.gambeat.site.services.implementation.DefaultMessageService;
import com.gambeat.site.websocket.jsonmodel.WSChatModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.Collection;
import java.util.List;


@Component
public class WebSocketEventListener {

    private Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    DefaultMessageService defaultMessageService;

    @Autowired
    UserDAOService userDAOService;

    @Autowired
    FriendRequestService friendRequestService;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {

        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String username =  headerAccessor.getUser().getName();

       User user =  userDAOService.getByUsername(username);

       user.setOnlineStatus(Enum.OnlineStatus.ONLINE);

       userDAOService.updateUser(user);

       // messagingTemplate.convertAndSend("/topic/public", user);

       List<User> friendsOnline = friendRequestService.getFriendsOnline(user);

       if (!friendsOnline.isEmpty()){

           for (User friend: friendsOnline) {

               // count the total unread messages for this user to each of his friends before sending the object to them
               user.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(user, friend, false));

               messagingTemplate.convertAndSendToUser(friend.getUserName(),"/topic/friend/online", user);

           }

       }



        logger.info("Received a new web socket connection");

    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {

        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String username = headerAccessor.getUser().getName();

        if(username != null) {
            logger.info("User Disconnected : " + username);

            User user =  userDAOService.getByUsername(username);

            user.setOnlineStatus(Enum.OnlineStatus.OFFLINE);

            userDAOService.updateUser(user);

            List<User> friendsOnline = friendRequestService.getFriendsOnline(user);

            if (!friendsOnline.isEmpty()){

                for (User friend: friendsOnline) {

                    messagingTemplate.convertAndSendToUser(friend.getUserName(),"/topic/friend/offline", user);

                }

            }

        }
    }

}
