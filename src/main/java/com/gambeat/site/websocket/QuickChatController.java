package com.gambeat.site.websocket;


import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.MessageService;
import com.gambeat.site.services.UserDAOService;
import com.gambeat.site.services.implementation.DefaultMessageService;
import com.gambeat.site.websocket.jsonmodel.WSChatModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
public class QuickChatController {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    UserDAOService userDAOService;

    @Autowired
    DefaultMessageService defaultMessageService;



    @MessageMapping("/quick-chat/send/message")
    public void sendMessage(@Payload Message message) {


        User sender =  userDAOService.getUser(message.getSender().getId());

        User recipient = userDAOService.getUser(message.getReceiver().getId());

        message.setSender(sender).setReceiver(recipient);

        message.setDateCreated();

        message.setMessageStatus(Enum.MessageStatus.DELIVERED);

        Message savedMessage = defaultMessageService.save(message);

        simpMessagingTemplate.convertAndSendToUser(recipient.getUserName(),"/topic/quick-chat/receive/message", savedMessage);

    }

    @MessageMapping("/quick-chat/seen/message")
    public void markMessageAsRead(@Payload Message message) {

        System.out.println("here");

        message.setMessageStatus(Enum.MessageStatus.READ);

        message.setRead(true);

        Message updatedMessage = defaultMessageService.update(message);

        simpMessagingTemplate.convertAndSendToUser(message.getSender().getUserName(),"/topic/quick-chat/sent/message/seen", updatedMessage);
    }

}
