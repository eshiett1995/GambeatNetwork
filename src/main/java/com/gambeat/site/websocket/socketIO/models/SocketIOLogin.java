package com.gambeat.site.websocket.socketIO.models;

public class SocketIOLogin {

    String username;

    String password;

    public String getUsername() {

        return username;

    }

    public void setUsername(String username) {

        this.username = username;

    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
