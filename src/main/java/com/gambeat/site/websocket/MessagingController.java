package com.gambeat.site.websocket;


import com.corundumstudio.socketio.SocketIOClient;
import com.gambeat.site.controllers.restcontroller.jsonmodel.FriendAndMessagesModel;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.UserDAOService;
import com.gambeat.site.services.implementation.DefaultMessageService;
import com.gambeat.site.websocket.jsonmodel.WSChatModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.math.BigInteger;
import java.util.Objects;

import static com.gambeat.GambeatNetworkApplication.server;

@Controller
public class MessagingController {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    UserDAOService userDAOService;

    @Autowired
    DefaultMessageService defaultMessageService;


    @MessageMapping("/messaging/send/message")
    public void sendMessage(@Payload Message message) {


        User sender =  userDAOService.getUser(message.getSender().getId());

        User recipient = userDAOService.getUser(message.getReceiver().getId());

        message.setSender(sender).setReceiver(recipient);

        message.setDateCreated();

        message.setMessageStatus(Enum.MessageStatus.DELIVERED);

        Message savedMessage = defaultMessageService.save(message);

        FriendAndMessagesModel friendAndMessagesModel = new FriendAndMessagesModel();

        friendAndMessagesModel.setFriend(savedMessage.getSender()
                .setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(savedMessage.getSender(), savedMessage.getReceiver(), false)))
                .getMessages().add(savedMessage);

        simpMessagingTemplate.convertAndSendToUser(recipient.getUserName(),"/topic/messaging/receive/message", friendAndMessagesModel);

        simpMessagingTemplate.convertAndSendToUser(recipient.getUserName(),"/topic/quick-chat/receive/message", savedMessage);

        if(Objects.nonNull(server)){

            SocketIOClient client = server.getAllClients().stream().filter(

                    socketIOClient -> String.valueOf(socketIOClient.get("name")).equals(recipient.getUserName())

            ).findFirst().get();

            client.sendEvent("receive message", savedMessage);
        }

    }

    /**@MessageMapping("/chat.sendMessage")
    @SendToUser("/queue/user")
    public WSChatModel sendMessage(@Payload WSChatModel wsChatModel) {
        return wsChatModel;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/friend/online")
    public User addUser(@Payload WSChatModel wsChatModel,
                               SimpMessageHeaderAccessor headerAccessor) {
        //Add username in web socket session
        headerAccessor.getSessionAttributes().put("username", "");

        User v = userDAOService.getUser(String.valueOf(1));

        //simpMessagingTemplate.convertAndSend("/topic/public", v);

        return v;
    }**/

}
