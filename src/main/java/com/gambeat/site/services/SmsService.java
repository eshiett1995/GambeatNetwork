package com.gambeat.site.services;

import com.gambeat.site.entities.User;
import com.twilio.rest.api.v2010.account.Message;

public interface SmsService {

    Message SendSms(String recipientNumber, String senderNumber, String message);

    Message ComposeWithdrawalTokenSms(User user, String senderNumber, String token);

    Message ComposeChangePasswordTokenSms(User user, String token);

    Message ComposeChangeEmailTokenSms(User user, String token);


}
