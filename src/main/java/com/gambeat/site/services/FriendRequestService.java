package com.gambeat.site.services;

import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Oto-obong on 16/08/2017.
 */
public interface FriendRequestService {

    FriendRequest save(FriendRequest friendRequest);

    void addFriendRequest (FriendRequest friendRequest);

    List<FriendRequest> getAllFriendRequest (User user);

    List<FriendRequest> getAllPendingFriendRequests(User user);

    List<FriendRequest> getAllFriendRequest (Long userID);

    FriendRequest findFriendRequestById(int id);

    FriendRequest findFriendRequestByRecipientAndSender(User recipient, User sender);

    FriendRequest findFriendRequestByUsers(User receiver, User sender);

    void deleteFriendRequestById(String id);

    void deleteFriendRequest(FriendRequest friendRequest);

    void updateFriendRequest(FriendRequest friendRequest);

    List<User> getFriends(User user);


    List<User> getFriends(User user,List<FriendRequest> friendRequests);

    List<User> getFriendsOnline(User user);

    List<User> getMutualFriends(User firstUser, User secondUser);

    Page<FriendRequest> getAcceptedFriendRequestPages(User user, Enum.FriendRequestStatus requestStatus, PageRequest pageRequest);

    int countAllFriends(User user);

}
