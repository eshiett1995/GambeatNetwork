package com.gambeat.site.services;

import com.gambeat.site.entities.User;
import com.gambeat.site.utility.email.EmailStatus;

/**
 * Created by Oto-obong on 28/10/2017.
 */

public interface MailNotificationService {

    EmailStatus SignUpConfirmation(User user, String url);

    EmailStatus BroadCastViaEmail(String message);

    EmailStatus BroadCastViaEmail(User user, String message);

    EmailStatus SendTokenViaEmail(User user, String token);



}
