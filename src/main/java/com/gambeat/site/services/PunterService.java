package com.gambeat.site.services;

import com.gambeat.site.entities.DefaultEntity;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.Punter;
import com.gambeat.site.entities.User;

import java.util.List;


/**
 * Created by Oto-obong on 16/08/2017.
 */


public interface PunterService{

    void addPunter (Punter punter);

    List<Punter> getAllPunter (User user);

    List<Punter> getAllPunter(Punter userID);

    Punter findPunterById(int id);

    void deletePunter(int punterID);

    void deletePunter(Punter punter);

}
