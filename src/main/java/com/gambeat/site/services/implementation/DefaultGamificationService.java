package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.Gamification;
import com.gambeat.site.repositories.GamificationRepository;
import com.gambeat.site.services.GamificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultGamificationService implements GamificationService {

    @Autowired
    GamificationRepository gamificationRepository;

    @Override
    public Gamification save(Gamification gamification) {

        return gamificationRepository.save(gamification);

    }
}
