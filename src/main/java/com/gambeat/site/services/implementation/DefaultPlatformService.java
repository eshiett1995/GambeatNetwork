package com.gambeat.site.services.implementation;


import com.gambeat.site.entities.Platform;
import com.gambeat.site.repositories.PlatformRepository;
import com.gambeat.site.services.PlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultPlatformService implements PlatformService {

    @Autowired
    PlatformRepository platformRepository;


    @Override
    public Platform findPlatformByName(String name) {

        return platformRepository.findByName(name);

    }

    @Override
    public Platform save(Platform platform) {

        return platformRepository.save(platform);

    }

    @Override
    public Platform update(Platform platform) {

        return platformRepository.save(platform);

    }
}
