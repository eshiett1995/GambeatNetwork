package com.gambeat.site.services.implementation;

import com.gambeat.site.controllers.restcontroller.jsonmodel.TransactionModel;
import com.gambeat.site.network.retrofit.RetrofitCallback;
import com.gambeat.site.payment.payant.models.response.WithdrawResponse;
import com.gambeat.site.services.PaymentService;
import com.gambeat.site.payment.payant.DefaultPayantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;

@Service
public class DefaultPaymentService implements PaymentService {

    @Autowired
    DefaultPayantService defaultPayantService;

    @Override
    public Response<WithdrawResponse> makeTransfer(TransactionModel transactionModel) throws IOException {

         return defaultPayantService.makeWalletWithdrawal(
                transactionModel.getWithdrawModel().getBankCode(),
                transactionModel.getWithdrawModel().getAccountNumber(),
                String.valueOf(transactionModel.getTransaction().getAmount()));


    }


}
