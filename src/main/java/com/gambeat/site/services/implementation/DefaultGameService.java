package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.Game;
import com.gambeat.site.entities.Genre;
import com.gambeat.site.entities.Platform;
import com.gambeat.site.repositories.GameRepository;
import com.gambeat.site.services.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DefaultGameService implements GameService {

    @Autowired
    GameRepository gameRepository;

    @Override
    public Game findGameByName(String name) {

        return gameRepository.findGameByName(name);

    }

    @Override
    public Game save(Game game) {

        return gameRepository.save(game);

    }

    @Override
    public Page<Game> getAll(Pageable pageable) {

        return gameRepository.findAll(pageable);

    }

    @Override
    public Page<Game> findGamesByNameContaining(String name, Pageable pageable) {

        return gameRepository.findGamesByNameContaining(name, pageable);

    }

    @Override
    public Page<Game> findGamesByPlatform(Platform platform, Pageable pageable) {

        return gameRepository.findGamesByPlatforms(platform, pageable);

    }

    @Override
    public Page<Game> findGamesByGenre(Genre genre, Pageable pageable) {

        return gameRepository.findGamesByGenres(genre, pageable);

    }

    @Override
    public Page<Game> findGamesByNameContainingAndGenre(String name, Genre genre, Pageable pageable) {

        return gameRepository.findGamesByNameContainingAndGenres(name, genre, pageable);

    }

    @Override
    public Page<Game> findGamesByNameContainingAndPlatform(String name, Platform platform, Pageable pageable) {

        return gameRepository.findGamesByNameContainingAndPlatforms(name, platform, pageable);

    }

    @Override
    public Page<Game> findGamesByPlatformAndGenre(Platform platform, Genre genre, Pageable pageable) {

        return gameRepository.findGamesByPlatformsAndGenres(platform, genre, pageable);

    }

    @Override
    public Page<Game> findGamesByNameContainingAndPlatformsAndGenres(String name, Platform platform, Genre genre, Pageable pageable) {

        return gameRepository.findGamesByNameContainingAndPlatformsAndGenres(name, platform, genre, pageable);

    }
}
