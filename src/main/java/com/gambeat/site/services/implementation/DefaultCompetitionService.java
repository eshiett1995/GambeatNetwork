package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.Competition;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Game;
import com.gambeat.site.entities.User;
import com.gambeat.site.repositories.CompetitionRepository;
import com.gambeat.site.services.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;


@Service
public class DefaultCompetitionService implements CompetitionService {

    @Autowired
    CompetitionRepository competitionRepository;

    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public Competition save(Competition competition) {

       return competitionRepository.save(competition);

    }

    @Override
    public Competition getCompetitionById(String id) {

        return competitionRepository.findOne(id);

    }

    @Override
    public Competition update(Competition competition) {

        return competitionRepository.save(competition);

    }

    @Override
    public Competition getCompetitionByName(String name) {

        return competitionRepository.findCompetitionByCompetitionName(name);

    }

    @Override
    public List<Competition> getMatches(User user) {

        return competitionRepository.getAllByPuntersPlayer(user);

    }

    @Override
    public Competition checkIfCompetitionExist(Game game, double entryFee) {

        return competitionRepository.findCompetitionByGameAndEntryFeeAndCompetitionStatus(game, entryFee, Enum.CompetitionStatus.OPEN);

    }

    @Override
    public Page<Competition> queryCompetitionTable(Competition competition, Pageable pageable) {

        ArrayList<AggregationOperation> aggregationOperationArrayList = new ArrayList<>();



        MatchOperation competitionNameMatchOperation = match(Criteria.where("competitionName").is(competition.getCompetitionName()));

        if(!competition.getCompetitionName().isEmpty()) aggregationOperationArrayList.add(competitionNameMatchOperation);



        MatchOperation gameMatchOperation = match(Criteria.where("game").is(competition.getGame()));

        if(!competition.getGame().getName().isEmpty()) aggregationOperationArrayList.add(gameMatchOperation);



        MatchOperation entryFeeMatchOperation = match(Criteria.where("entryFee").is(competition.getEntryFee()));

        if(competition.getEntryFee() > 0) aggregationOperationArrayList.add(entryFeeMatchOperation);


        MatchOperation playerMatchOperation = match(Criteria.where("punters.player").is(competition.getPunters().get(0).getPlayer()));

        if(!competition.getPunters().get(0).getPlayer().getUserName().isEmpty()) aggregationOperationArrayList.add(playerMatchOperation);



        SortOperation sortOperation = new SortOperation(new Sort(Sort.Direction.DESC,"dateCreated"));

        aggregationOperationArrayList.add(sortOperation);

        Aggregation aggregation = newAggregation(aggregationOperationArrayList);



        AggregationResults<Competition> result = mongoTemplate.aggregate(aggregation,"competition", Competition.class);


        return new PageImpl<>(result.getMappedResults(), pageable, result.getMappedResults().size());
    }
}
