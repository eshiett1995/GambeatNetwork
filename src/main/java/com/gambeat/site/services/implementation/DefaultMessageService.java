package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import com.gambeat.site.repositories.MessageRepository;
import com.gambeat.site.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

/**
 * Created by Oto-obong on 16/08/2017.
 */

@Service
public class DefaultMessageService implements MessageService {


    @Autowired
    MessageRepository messageRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Message save(Message message) {

        return messageRepository.save(message);

    }

    @Override
    public Message update(Message message) {

        return messageRepository.save(message);

    }


    @Override
    public Page<Message> getMessage(User user1, User user2, Pageable pageable) {

        return messageRepository.findMessageByReceiverAndSenderOrSenderAndReceiverOrderByDateCreatedDesc(user1, user2, user1, user2, pageable);

    }

    @Override
    public void addMessage(Message message) {

    }

    @Override
    public List<Message> getUnreadChats(User receiver) {

        MatchOperation matchOperation = match(
                Criteria.where("receiver.email").is(receiver.getEmail())
                        .andOperator(Criteria.where("isRead").is(false)));


        SortOperation sortOperation = new SortOperation(new Sort(Sort.Direction.DESC,"dateCreated"));

        GroupOperation groupOperation = group("sender","receiver").push("$$ROOT").as("children");

        AggregationOperation replaceRoot = Aggregation.replaceRoot().withValueOf(ArrayOperators.ArrayElemAt.arrayOf("children").elementAt(0));



        Aggregation aggregation = newAggregation(matchOperation, sortOperation, groupOperation,replaceRoot);


        AggregationResults<Message> result = mongoTemplate.aggregate(aggregation,"message", Message.class);


        return result.getMappedResults();

    }

    @Override
    public int countUnreadMessagesFromSender(User sender, User receiver, boolean isRead) {

        return messageRepository.countMessagesBySenderAndReceiverAndIsRead(sender, receiver, isRead);

    }

    @Override
    public int countTotalUnreadMessages(User receiver) {

        return messageRepository.countMessagesByReceiverAndIsRead(receiver,false);

    }

    @Override
    public Page<Message> findMostRecentMessageFromConversation(User user,Pageable pageable) {

        MatchOperation matchOperation = match(new Criteria().orOperator(
                Criteria.where("sender.email").is(user.getEmail()),
                Criteria.where("receiver.email").is(user.getEmail())
        ));

        ProjectionOperation projectToMatchModel = project("id","dateCreated","sender","receiver","message","messageStatus","isRead");



        ConditionalOperators.Cond condOperation = ConditionalOperators.when(Criteria.where("receiver.email").is(user.getEmail()))
                .thenValueOf(ConditionalOperators.when(Criteria.where("sender.email").is(user.getEmail())).thenValueOf("").otherwiseValueOf("sender.userName"))
                .otherwiseValueOf("receiver.userName");

        projectToMatchModel.and(condOperation);

        SortOperation sortOperation = new SortOperation(new Sort(Sort.Direction.DESC,"dateCreated"));

        GroupOperation groupOperation = group("friend").push("$$ROOT").as("messages");

        AggregationOperation replaceRoot = Aggregation.replaceRoot().withValueOf(ArrayOperators.ArrayElemAt.arrayOf("messages").elementAt(0));


        Aggregation aggregation = newAggregation(
                matchOperation,
                projectToMatchModel.and(condOperation).as("friend"),
                sortOperation,
                groupOperation,
                replaceRoot);

        AggregationResults<Message> result = mongoTemplate.aggregate(aggregation,"message", Message.class);

         return new PageImpl<>(result.getMappedResults(),pageable, result.getMappedResults().size());


    }

    @Override
    public Message findById(String id) {

        return messageRepository.findOne(id);

    }
}
