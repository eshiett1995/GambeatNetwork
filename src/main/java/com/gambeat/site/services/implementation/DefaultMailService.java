package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.MailService;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;


@Service
public class DefaultMailService implements MailService {


    @Override
    public Context ComposeUserActivationMail(User user, String token) {

        Context context = new Context();

        String msg = String.format("Hello %s %s welcome to Gambeat, to activate your account please kindly click the button below. We trust you would enjoy your stay, for request, inquiry please contact us on 08098516969. ", user.getFirstName(),user.getLastName(),token);

        context.setVariable("message", msg);

        context.setVariable("url","http://localhost:8080/activate/" + token );

        return  context;
    }



    @Override
    public Context ComposeWithdrawalOtpMail(User user, String token) {

        Context context = new Context();

        String msg = String.format("Hello %s %s welcome to Gambeat, to activate your account please kindly click the button below. We trust you would enjoy your stay, for request, inquiry please contact us on 08098516969. ", user.getFirstName(),user.getLastName(),token);

        context.setVariable("message", msg);

        context.setVariable("url","http://localhost:8080/activate/" + token );

        return  context;

    }

    @Override
    public Context ComposeUserProfileUpdateMail(User user, String token, Enum.RequestedUpdate requestedUpdate) {

        Context context = new Context();

        String msg = String.format("Good day %s, we got your request to " +

                "update your %s , we have processed your request, given " +

                "below is security token needed to make your update permanent. " +

                "Please be advised to ignore if you wish to terminate the update " +

                "of your account.", user.getUserName(),requestedUpdate.toString());


        context.setVariable("salutation", String.format("Hello %s,", user.getUserName()));

        context.setVariable("message", msg);

        context.setVariable("token",token );

        return  context;
    }


    /** @Override
    public Context ComposeEmployeeActivationMail(Employee employee, String token) {

        Context context = new Context();

        String msg = String.format("Hello %s %s welcome to Branded, to activate your account please kindly click the button below. We trust you would enjoy your stay, for request, inquiry please contact us on 08098516969. ", employee.getFirstName(),employee.getLastName(),token);

        context.setVariable("message", msg);

        context.setVariable("url","http://localhost:8080/activate/" + token );

        return  context;
    }  **/
}
