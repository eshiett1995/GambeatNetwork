package com.gambeat.site.services.implementation.sms;

import com.gambeat.site.services.TwilioService;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.stereotype.Service;

@Service
public class DefaultTwilioService implements TwilioService {

    @Override
    public Message sendMessage(String recipientNumber, String senderNumber, String message) {

       return Message.creator(new PhoneNumber(recipientNumber), new PhoneNumber(senderNumber), message).create();

    }
}
