package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.User;
import com.gambeat.site.services.MailNotificationService;
import com.gambeat.site.utility.email.EmailHtmlSender;
import com.gambeat.site.utility.email.EmailStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

/**
 * Created by Oto-obong on 30/10/2017.
 */

@Service
public class DefaultEmailMailNotificationService implements MailNotificationService {

    private JavaMailSender javaMailSender;

    @Autowired
    private EmailHtmlSender emailHtmlSender;

    @Autowired
    public DefaultEmailMailNotificationService(JavaMailSender javaMailSender, EmailHtmlSender emailHtmlSender){

        this.javaMailSender = javaMailSender;

        this.emailHtmlSender = emailHtmlSender;

    }

    @Override
    public EmailStatus SignUpConfirmation(User user, String url) {

        Context context = new Context();

        context.setVariable("salutation", String.format("Hello s%", user.getUserName()));

        context.setVariable("title", "Welcome to Gambeat");

        context.setVariable("message", "Lorem Lorem Lorem");

        context.setVariable("link", url);

        EmailStatus emailStatus = emailHtmlSender.send(user.getEmail(), "Welcome to Gambeat", "email/welcome", context);

        return emailStatus;

    }

    @Override
    public EmailStatus BroadCastViaEmail(String message) {

        return null;

    }

    @Override
    public EmailStatus BroadCastViaEmail(User user, String message) {

        return null;

    }

    @Override
    public EmailStatus SendTokenViaEmail(User user, String token) {

        Context context = new Context();

        context.setVariable("salutation", String.format("Hello %s", user.getUserName()));

        context.setVariable("title", "Welcome to Gambeat");

        context.setVariable("description", "Lorem Lorem Lorem");

        EmailStatus emailStatus = emailHtmlSender.send(user.getEmail(), "Welcome to Gambeat", "email/welcome", context);

        return emailStatus;

    }
}
