package com.gambeat.site.services.implementation;

import com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel.NewCompetitionModel;
import com.gambeat.site.entities.*;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.repositories.NotificationRepository;
import com.gambeat.site.services.NotificationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


@Service
public class DefaultNotificationService implements NotificationsService {

    @Autowired
    NotificationRepository notificationRepository;

    @Override
    public Notifications save(Notifications notification) {

        return notificationRepository.save(notification);

    }

    @Async("processExecutor")
    @Override
    public Notifications competitionStarted(User user, Competition competition) {

        Notifications notifications = new Notifications();

        notifications.setNotificationHeader("Competition Started");

        notifications.setNotificationType(Enum.NotificationType.TOURNAMENTSTARTED);

        notifications.setUser(user).setCompetition(competition)
                .setNotificationMessage(String.format("The s% competition has started, your match would be scheduled shortly", competition.getCompetitionName()));


        return save(notifications);
    }


    @Async("processExecutor")
    @Override
    public Notifications friendKnockedOutOfCompetition(User user, User looser, Competition competition, NewCompetitionModel newCompetitionModel) {

        Notifications notifications = new Notifications();

        notifications.setUser(user);

        notifications.setNotificationType(Enum.NotificationType.FRIENDELIMINATED);

        notifications.setNotificationHeader("The Black Sheep");

        notifications.setUser(user).setCompetition(competition)

                .setNotificationMessage(String.format("s% has been eliminated from the s% competition", looser.getUserName(), competition.getCompetitionName()));


        return save(notifications);
    }

    @Async("processExecutor")
    @Override
    public Notifications playerWinsMatch(User user, User looser, Competition competition) {

        Notifications notifications = new Notifications();

        notifications.setReference(looser);

        notifications.setNotificationType(Enum.NotificationType.PLAYERWINSMATCH);

        notifications.setNotificationHeader("A Winner");

        notifications.setUser(user).setCompetition(competition)

                .setNotificationMessage(String.format("You have progressed to the next stage after defeating s% in the s% competition", looser.getUserName(), competition.getCompetitionName() ));


        return save(notifications);
    }


    @Async("processExecutor")
    @Override
    public Notifications playerLoosesMatch(User user, User winner, Competition competition) {

        Notifications notifications = new Notifications();

        notifications.setReference(winner);

        notifications.setNotificationType(Enum.NotificationType.PLAYERWINSMATCH);

        notifications.setNotificationHeader("A Winner");

        notifications.setUser(user).setCompetition(competition)

                .setNotificationMessage(String.format("You have been eliminated from the s% competition, after being defeated by s%", competition.getCompetitionName(), winner.getUserName() ));


        return save(notifications);
    }


    @Async("processExecutor")
    @Override
    public Notifications friendWonCompetition(User user, User winner, Competition competition, NewCompetitionModel newCompetitionModel) {

        Notifications notifications = new Notifications();

        notifications.setUser(user);

        notifications.setReference(winner);

        notifications.setNotificationType(Enum.NotificationType.FRIENDELIMINATED);

        notifications.setNotificationHeader("Friend Of A Winner");

        notifications.setUser(user).setCompetition(competition)

                .setNotificationMessage(String.format("s% has won the s% competition, congrats, you are a friend of a winner", winner.getUserName(), competition.getCompetitionName()));


        return save(notifications);
    }


    @Async("processExecutor")
    @Override
    public Notifications PlayerWinsCompetition(User user,Competition competition) {

        Notifications notifications = new Notifications();

        notifications.setNotificationType(Enum.NotificationType.TOURNAMENTWINNER);

        notifications.setNotificationHeader("The Champion");

        notifications.setUser(user).setCompetition(competition)

                .setNotificationMessage(String.format("You have won the s% competition, congrats, your cash price has been disbursed to your account", competition.getCompetitionName() ));


        return save(notifications);
    }

    @Async("processExecutor")
    @Override
    public Notifications competitionEnded(User user, User winner, Competition competition) {

        Notifications notifications = new Notifications();

        notifications.setReference(winner);

        notifications.setNotificationType(Enum.NotificationType.TOURNAMENTENDED);

        notifications.setNotificationHeader("Finally Over");

        notifications.setUser(user).setCompetition(competition)

                .setNotificationMessage(String.format("The s% competition is finally over with s% emerging as the winner. Thanks for participating guys", competition.getCompetitionName(), winner.getUserName()));


        return save(notifications);
    }

    @Override
    public Notifications friendRequestAccepted(User user, FriendRequest friendRequest) {

        Notifications notifications = new Notifications();

        notifications.setReference(friendRequest.getRecipient())

                .setUser(friendRequest.getSender())

                .setNotificationType(Enum.NotificationType.FRIENDREQUESTACCEPTED)

                .setNotificationHeader("Friend Request Accepted")

                .setNotificationMessage(String.format("s% has accepted your friend request", friendRequest.getRecipient()));

        return save(notifications);
    }

    @Override
    public Notifications Transaction(User user, double amount, Transactions transaction) {

        String notificationHeader = "";

        switch (transaction.getTransactiontype()){

            case DEPOSIT:

                notificationHeader = String.format("Wallet (s%)", "Deposit");

                break;

            case WITHDRAW:

                notificationHeader = String.format("Wallet (s%)", "Withdrawal");

                break;

            case LOSS:

                notificationHeader = String.format("Wallet (s%)", "Loss");

                break;

            case WIN:

                notificationHeader = String.format("Wallet (s%)", "Win");

                break;

            case DEBIT:

                notificationHeader = String.format("Wallet (s%)", "Debit");

                break;

            case REVERSAL:

                notificationHeader = String.format("Wallet (s%)", "Reversal");

                break;

            default :
                notificationHeader = "Wallet";
        }

        Notifications notification = new Notifications();

        notification.setUser(user);

        notification.setNotificationHeader(notificationHeader);

        notification.setNotificationType(Enum.NotificationType.TRANSACTION);

        notification.setNotificationMessage(transaction.getDetail());

        notification.setDateCreated();

        return save(notification);

    }

    @Override
    public Notifications competitionEntered(User user, Competition competition) {

        Notifications notification = new Notifications();

        notification.setUser(user);

        notification.setNotificationHeader("Competition Entry");

        notification.setNotificationMessage(String.format("You have successfully entered into the s% competition. Best of luck", competition.getCompetitionName()));

        notification.setCompetition(competition);

        notification.setDateCreated();

        return save(notification);
    }

    @Override
    public Notifications competitionRegistrationDenied(User user, String reason) {

        Notifications notification = new Notifications();

        notification.setUser(user);

        notification.setNotificationHeader("Competition Registration Denied");

        notification.setNotificationMessage(reason);

        notification.setDateCreated();

        return save(notification);
    }

    @Override
    public Notifications competitionCouldNotStart(User user, Competition competition) {

        Notifications notification = new Notifications();

        notification.setUser(user);

        notification.setNotificationHeader("Competition Could Not Start");

        notification.setNotificationMessage("Competition could not start as it does'nt have the minimum number of contestants required");

        notification.setCompetition(competition);

        notification.setDateCreated();

        return save(notification);

    }

    @Override
    public Notifications friendRequestGotten(User user, User sender){

        Notifications notification = new Notifications();

        notification.setUser(user);

        notification.setNotificationHeader("Friend Request");

        notification.setNotificationMessage(String.format("You have a friend request from s%", sender.getUserName()));

        notification.setNotificationType(Enum.NotificationType.FRIENDREQUESTGOTTEN);

        notification.setReference(sender);

        notification.setDateCreated();

        return save(notification);
    }

}
