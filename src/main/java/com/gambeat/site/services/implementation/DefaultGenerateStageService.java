package com.gambeat.site.services.implementation;

import com.gambeat.site.models.GenerateStageStrategy;
import com.gambeat.site.services.GenerateStageService;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;


@Service
public class DefaultGenerateStageService implements GenerateStageService {

    @Override
    public String generateStage(GenerateStageStrategy ... generateStageStrategies) {

        if(Array.getLength(generateStageStrategies) == 1){

            GenerateStageStrategy firstGenerateStageStrategy = (GenerateStageStrategy) Array.get(generateStageStrategies, 0);

            double[]singleColumnArray = new double[Math.toIntExact(firstGenerateStageStrategy.getColumnSize())];

            for (int index = 0; index < firstGenerateStageStrategy.getColumnSize(); index++){

                singleColumnArray[index] = (Math.random() * ((firstGenerateStageStrategy.getEndNumber() - firstGenerateStageStrategy.getStartNumber()) + 1)) + firstGenerateStageStrategy.getStartNumber();
            }

            return singleColumnArray.toString();

        }else{

            int numberOfColumns = generateStageStrategies.length;

            int numberOfRows = Math.toIntExact(generateStageStrategies[0].getRowSize());

            double[][] multiDimensionArray = new double[numberOfRows][numberOfColumns];

            for (int indexOfGenerateStageStrategy = 0; indexOfGenerateStageStrategy < generateStageStrategies.length; indexOfGenerateStageStrategy++){

                for (int index = 0; index < generateStageStrategies[indexOfGenerateStageStrategy].getColumnSize(); index++){

                    multiDimensionArray[index][indexOfGenerateStageStrategy] = (Math.random() * ((generateStageStrategies[indexOfGenerateStageStrategy].getEndNumber() - generateStageStrategies[indexOfGenerateStageStrategy].getStartNumber()) + 1)) + generateStageStrategies[indexOfGenerateStageStrategy].getStartNumber();
                }
            }

            return multiDimensionArray.toString();
        }
    }
}
