package com.gambeat.site.services.implementation;

import com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.third_party.jsonmodel.SocketIOFriendRequestModel;
import com.gambeat.site.controllers.third_party.jsonmodel.SocketIOMessageModel;
import com.gambeat.site.entities.Message;
import com.gambeat.site.network.retrofit.apiHelper.socketIO.SocketIOApi;
import com.gambeat.site.network.retrofit.apiHelper.socketIO.SocketIOServiceGenerator;
import com.gambeat.site.services.SocketIOService;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;


@Service
public class DefaultSocketIOService implements SocketIOService {



    private SocketIOApi socketIOApi = SocketIOServiceGenerator.createService(SocketIOApi.class, "yello");

    @Override
    public Response<ResponseModel> sendMessageToSocketIOServer(SocketIOMessageModel message) throws IOException {

        Call<ResponseModel> callAsync = socketIOApi.SendMessageToSocketIOServer(message);

        Response<ResponseModel> response =  callAsync.execute();

        return response;

    }

    @Override
    public Response<ResponseModel> sendFriendRequestToSocketIOServer(SocketIOFriendRequestModel friendRequest) throws IOException {

        Call<ResponseModel> callAsync = socketIOApi.SendFriendRequestToSocketIOServer(friendRequest);

        Response<ResponseModel> response =  callAsync.execute();

        return response;

    }
}
