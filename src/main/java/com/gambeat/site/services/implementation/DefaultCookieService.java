package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.User;
import com.gambeat.site.services.CookieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Oto-obong on 18/10/2017.
 */

@Service
public class DefaultCookieService implements CookieService {

    @Autowired
    DefaultUserDAOService defaultUserDAOService;


    @Override
    public boolean hasValidCookie(HttpServletRequest request) {


        Cookie[] cookieArray = request.getCookies();

        boolean hasValidCookie = false;

        if(cookieArray != null) {   //checks if the cookie array is null

            if (cookieArray.length >= 2) {  // checks if there are two cookies, SESSION and gambeat else return false (there most be only two cookies)

                for (Cookie cookie : cookieArray) { // checks if gambeat cookie is amongst the cookies

                     if(cookie.getName().equalsIgnoreCase("gambeat")){

                         HttpSession session = request.getSession(true);

                         String name = (String) session.getAttribute(cookie.getValue());

                         return name != null;


                     }
                }

            } else {

                return false;
            }

        }else{

            return false;
        }

        return hasValidCookie;
    }

    @Override
    public Cookie generateCookie(String sessionId) {

        // generates cookie for gambeat.

        Cookie cookie = new Cookie("gambeat", sessionId);
        cookie.setSecure(false);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(6000);
        cookie.setDomain("localhost");
        cookie.setPath("/");

        return cookie;

    }

    @Override
    public Cookie expireCookie() {
        Cookie cookie = new Cookie("gambeat", "");
        cookie.setSecure(false);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(0);
        cookie.setDomain("localhost");
        cookie.setPath("/");

        return cookie;
    }


    @Override
    public User getUser(String cookieName, HttpServletRequest request) {

        Cookie[] cookies = request.getCookies();

        for (Cookie cookie: cookies) {

            if(cookie.getName().equalsIgnoreCase(cookieName)){

                // Gets the session ID from the request
                HttpSession session = request.getSession(true);

               // System.out.println("cookie name: " + cookie.getValue());

                //System.out.println(session.getAttribute(cookie.getValue()));

                return defaultUserDAOService.getByUsername((String) session.getAttribute(cookie.getValue()));

            }

        }
        return null;

    }


}
