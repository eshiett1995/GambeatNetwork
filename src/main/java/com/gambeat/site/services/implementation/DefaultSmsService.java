package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.User;
import com.gambeat.site.services.SmsService;
import com.gambeat.site.services.implementation.sms.DefaultTwilioService;
import com.twilio.rest.api.v2010.account.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultSmsService implements SmsService {

    @Autowired
    DefaultTwilioService defaultTwilioService;

    @Autowired
    DefaultTokenService defaultTokenService;

    private static String senderNumber = "+17737419429";

    private static int changePasswordTokenLength = 6;

    @Override
    public Message SendSms(String recipientNumber, String senderNumber, String message) {

        return defaultTwilioService.sendMessage(recipientNumber,senderNumber,message);

    }

    @Override
    public Message ComposeWithdrawalTokenSms(User user, String senderNumber, String token) {

        return this.SendSms(user.getPhoneNumber(), senderNumber, token);

    }

    @Override
    public Message ComposeChangePasswordTokenSms(User user, String token) {

        return this.SendSms(user.getPhoneNumber(), senderNumber, token);
    }

    @Override
    public Message ComposeChangeEmailTokenSms(User user, String token) {

        return this.SendSms(user.getPhoneNumber(), senderNumber, token);
    }
}
