package com.gambeat.site.services.implementation;

import com.gambeat.site.services.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

import java.io.File;


/**
 * Created by Oto-obong on 18/09/2017.
 */

@Service
public class DefaultFileService implements FileService{

    // path for the local machine
    private static final String ABS_PATH = "C:\\Users\\Oto-obong\\IdeaProjects\\GambeatNetwork\\src\\main\\resources\\static\\profile-pictures";

    // path for the server
    private static String REAL_PATH = "";

    private Logger logger = LoggerFactory.getLogger(DefaultFileService.class);


    @Override
    public String saveImage(HttpServletRequest request, MultipartFile file, String userName) {




        // get the Real_Path
        REAL_PATH = request.getServletContext().getRealPath("/static/profile-pictures/");

        logger.info(REAL_PATH);

        //to make sure all the directory exists
        //creates the directory if it doesnt exists

        if(!new File(ABS_PATH).exists()){
            //create directory
            new File(ABS_PATH).mkdirs();
        }

        if(!new File(REAL_PATH).exists()){
            //create directory
            new File(REAL_PATH).mkdirs();
        }


        try{

            // transfer to the server path
            file.transferTo(new File(REAL_PATH + userName + ".jpg"));

            // transfer to the development path
            file.transferTo(new File(ABS_PATH + userName + ".jpg"));


        }catch (Exception ex){

            // logs IO exception or illegal state exception
            logger.error(ex.getCause().toString());
        }

        return null;
    }
}
