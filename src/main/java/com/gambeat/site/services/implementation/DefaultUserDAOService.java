package com.gambeat.site.services.implementation;

import com.gambeat.site.controllers.third_party.jsonmodel.Chess;
import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.User;
import com.gambeat.site.repositories.UserRepository;
import com.gambeat.site.services.UserDAOService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

/**
 * Created by Oto-obong on 02/09/2017.
 */


@Service
public class DefaultUserDAOService implements UserDAOService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    UserRepository userRepository;

    @Override
    public Page<User> findUser(String key, Pageable pageable) {

        return userRepository.findUsersByUserNameContainingOrFirstNameContainingOrLastNameContaining(key,key, key, pageable);
    }

    @Override
    public User saveUser(User user) {

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        return this.userRepository.save(user);

    }

    @Override
    public List<User> getAllUser(User user) {
        return userRepository.findAll();
    }

    @Override
    public User getUser(String userID) {

        return userRepository.getUserById(userID);
    }

    @Override
    public User updateUser(User user) {

        return this.userRepository.save(user);

    }

    @Override
    public void deleteUser(int userID) {


    }

    @Override
    public void deleteUser(User user) {

    }

    @Override
    public void verifyUser(User user) {

    }

    @Override
    public void verifyUser(String userName) {

    }


    @Override
    public User getByUsername(String userName) {

        if(userRepository.getUserByUserName(userName) == null) {return null;}
        else {

            return userRepository.getUserByUserName(userName);
        }
    }

    @Override
    public User getByEmail(String email) {

        return this.userRepository.getUserByEmail(email);
    }

    @Override
    public User getUserByPhoneNumber(String phoneNumber) {

        return this.userRepository.getUserByPhoneNumber(phoneNumber);
    }

    @Override
    public boolean isLocked(User user) {

        return user.getLocked();
    }

    @Override
    public Page<User> getFriends(User user, Pageable pageable) {


        MatchOperation matchOperation = match(new Criteria().orOperator(
                Criteria.where("sender.email").is(user.getEmail()),
                Criteria.where("recipient.email").is(user.getEmail())
        ));

        ProjectionOperation projectToMatchModel = project("sender","recipient");

        ConditionalOperators.Cond condOperation = ConditionalOperators.when(Criteria.where("recipient.email").is(user.getEmail()))
                .thenValueOf(ConditionalOperators.when(Criteria.where("sender.email").is(user.getEmail())).thenValueOf("").otherwiseValueOf("sender"))
                .otherwiseValueOf("recipient");


        SortOperation sortOperation = new SortOperation(new Sort(Sort.Direction.ASC,"friend.userName"));


        Aggregation aggregation = newAggregation( matchOperation, projectToMatchModel.and(condOperation).as("friend"), project("friend").andExclude("_id"), sortOperation);


        AggregationResults<Chess> result = mongoTemplate.aggregate(aggregation,"friendRequest", Chess.class);

        List<User> friends = new ArrayList<>();

        for (Chess chess: result.getMappedResults()) {

            friends.add(chess.getFriend());

        }


        return new PageImpl<>(friends, pageable, friends.size());
    }

    @Override
    public List<User> getFriends(User user) {
        MatchOperation matchOperation = match(new Criteria().orOperator(
                Criteria.where("sender.email").is(user.getEmail()),
                Criteria.where("recipient.email").is(user.getEmail())
        ));

        ProjectionOperation projectToMatchModel = project("sender","recipient");

        ConditionalOperators.Cond condOperation = ConditionalOperators.when(Criteria.where("recipient.email").is(user.getEmail()))
                .thenValueOf(ConditionalOperators.when(Criteria.where("sender.email").is(user.getEmail())).thenValueOf("").otherwiseValueOf("sender"))
                .otherwiseValueOf("recipient");


        SortOperation sortOperation = new SortOperation(new Sort(Sort.Direction.ASC,"friend.userName"));


        Aggregation aggregation = newAggregation( matchOperation, projectToMatchModel.and(condOperation).as("friend"), project("friend").andExclude("_id"), sortOperation);


        AggregationResults<Chess> result = mongoTemplate.aggregate(aggregation,"friendRequest", Chess.class);

        List<User> friends = new ArrayList<>();

        for (Chess chess: result.getMappedResults()) {

            friends.add(chess.getFriend());

        }


        return friends;
    }


    // checks if the record of the user with the same username and email exists before any actions (saving it to the database.)
    public String exists(User user){

        User tempUser = getByUsername(user.getUserName()); // checks for the same username

        if(tempUser == null){

            tempUser = getByEmail(user.getEmail()); // check for the same email

               if(tempUser == null){

                    tempUser = getUserByPhoneNumber(user.getPhoneNumber());// checks for the same number

                        if(tempUser == null){

                           return "No records found";

                        }else{

                           return "Phone number already exists";
                        }

               }else{

                  return "Email already exists";

               }

        }else{

           return "Username already exists";
        }

    }


  // method before login checks if the user exists and if it does, checks if the password provided correlates with the user
    public String verification(User user){

        User tempUser = getByUsername(user.getUserName()); // checks for the existing username in the database

        if(tempUser != null){ // if a user with that username exists

            if(bCryptPasswordEncoder.matches(user.getPassword(), tempUser.getPassword())){ // checks if the username provided tallies with the user in the database

                return "Verified"; //response to the client if both the username and password is correct.

            }else{

                return "Incorrect password"; // response to the client if the password is incorrect
            }

        }else{

            return "Incorrect credential"; // response to the client if user with the username does not exist in the database.
        }


    }
}


