package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.Transactions;
import com.gambeat.site.entities.User;
import com.gambeat.site.repositories.TransactionRepository;
import com.gambeat.site.services.TransactionService;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultTransactionService implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public Transactions save(Transactions transaction) {

        return transactionRepository.save(transaction);

    }

    @Override
    public Transactions update(Transactions transaction) {

        return transactionRepository.save(transaction);

    }

    @Override
    public Transactions getAccurateTransaction(Transactions transactions) {

        return transactionRepository.getByIdAndAmountAndTransactiontype(transactions.getId(),transactions.getAmount(), transactions.getTransactiontype());
    }

    @Override
    public Page<Transactions> getUsersTransactions(User user, Pageable pageable) {

        return transactionRepository.getTransactionsByPlayer(user, pageable);

    }

    public List<Transactions> getAll() {

        return (List<Transactions>) transactionRepository.findAll();
    }
}
