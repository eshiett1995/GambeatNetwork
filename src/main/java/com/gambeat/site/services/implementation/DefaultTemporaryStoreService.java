package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.TemporaryStore;
import com.gambeat.site.repositories.TemporaryStoreRepository;
import com.gambeat.site.services.TemporaryStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Oto-obong on 31/10/2017.
 */

@Service
public class DefaultTemporaryStoreService implements TemporaryStoreService {

    @Autowired
    TemporaryStoreRepository temporaryStoreRepository;


    @Override
    public TemporaryStore saveTemporaryStore(TemporaryStore temporaryStore) {

        return temporaryStoreRepository.save(temporaryStore);
    }

    @Override
    public TemporaryStore getTemporaryStoreByIdAndToken(long id, String Token) {

        return temporaryStoreRepository.getTemporaryStoreByIdAndToken(id,Token);
    }

    @Override
    public void delete(TemporaryStore temporaryStore) {

        temporaryStoreRepository.delete(temporaryStore);

    }
}
