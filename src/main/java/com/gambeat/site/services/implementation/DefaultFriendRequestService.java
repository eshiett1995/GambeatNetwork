package com.gambeat.site.services.implementation;

import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.User;
import com.gambeat.site.repositories.FriendRequestRepository;
import com.gambeat.site.services.FriendRequestService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Oto-obong on 16/08/2017.
 */


@Service
public class DefaultFriendRequestService implements FriendRequestService{


    @Autowired
    FriendRequestRepository friendRequestRepository;


    @Override
    public FriendRequest save(FriendRequest friendRequest) {

        return friendRequestRepository.save(friendRequest);

    }

    @Override
    public void addFriendRequest(FriendRequest friendRequest) {


    }

    @Override
    public List<FriendRequest> getAllFriendRequest(User user) {

        return friendRequestRepository.getFriendRequestsByRecipientAndFriendRequestStatus(user, Enum.FriendRequestStatus.PENDING);
    }

    @Override
    public List<FriendRequest> getAllPendingFriendRequests(User user) {

        return friendRequestRepository.getFriendRequestsByRecipientAndFriendRequestStatus(user, Enum.FriendRequestStatus.PENDING);

    }

    @Override
    public List<FriendRequest> getAllFriendRequest(Long userID) {
        return null;
    }



    @Override
    public FriendRequest findFriendRequestById(int id) {
        return null;
    }

    @Override
    public FriendRequest findFriendRequestByRecipientAndSender(User recipient, User sender) {

        return friendRequestRepository.getFriendRequestByRecipientAndSender(recipient, sender);

    }


    @Override
    public FriendRequest findFriendRequestByUsers(User userOne, User userTwo) {

        if(friendRequestRepository.getFriendRequestByRecipientAndSender(userOne, userTwo) != null) {

            return friendRequestRepository.getFriendRequestByRecipientAndSender(userOne, userTwo);

        }else{

            return friendRequestRepository.getFriendRequestByRecipientAndSender(userTwo, userOne);

        }
    }


    @Override
    public void deleteFriendRequestById(String id) {

        friendRequestRepository.delete(id);
    }


    @Override
    public void deleteFriendRequest(FriendRequest friendRequest) {

        friendRequestRepository.delete(friendRequest);

    }


    @Override
    public void updateFriendRequest(FriendRequest friendRequest) {

        // save works as update(merge) if the entity T is not new to the database
        friendRequestRepository.save(friendRequest);
    }


    @Override
    public List<User> getFriends(User user) {

        // sets up the users list
        List<User> friends = new ArrayList<User>();

        // gets the friend requests that are for the user and has been accepted (his friends)
        /** List<FriendRequest> receivedFriendRequests =  friendRequestRepository.getFriendRequestsByRecipientAndFriendRequestStatus(user, Enum.FriendRequestStatus.ACCEPTED );

        for (FriendRequest friendRequest:receivedFriendRequests) {

            //adds the senders to the users list
            friends.add(friendRequest.getSender());

        }


        // gets the friends requests the user has sent and has been accepted
        List<FriendRequest> sentFriendRequests = friendRequestRepository.getFriendRequestsBySenderAndFriendRequestStatus(user, Enum.FriendRequestStatus.ACCEPTED);

        for (FriendRequest friendRequest:sentFriendRequests) {

            //adds the recipient to the users list
            friends.add(friendRequest.getRecipient());

        } **/

        List<FriendRequest> receivedFriendRequests =  friendRequestRepository.getByRecipientOrSenderAndFriendRequestStatus(user,user, Enum.FriendRequestStatus.ACCEPTED );

        for (FriendRequest friendRequest:receivedFriendRequests) {

            if(friendRequest.getRecipient().getUserName().equals(user.getUserName())) {
                //adds the senders to the users list
                friends.add(friendRequest.getSender());

            }else{

                friends.add(friendRequest.getRecipient());
            }

        }

        return friends;
    }


    @Override
    public List<User> getFriends(User user, List<FriendRequest> friendRequests) {

        List<User> friends = new ArrayList<User>();

        for (FriendRequest friendRequest:friendRequests) {

            if(friendRequest.getRecipient().getUserName().equals(user.getUserName())) {

                friends.add(friendRequest.getSender());

            }else{

                friends.add(friendRequest.getRecipient());
            }

        }

        return friends;
    }


    @Override
    public List<User> getFriendsOnline(User user) {

        // sets up the users list
        List<User> friendsOnline = new ArrayList<User>();

        // gets the friend requests that are for the user and has been accepted (his friends)
        List<FriendRequest> friendRequests =  friendRequestRepository.getByRecipientOrSenderAndFriendRequestStatus(user,user, Enum.FriendRequestStatus.ACCEPTED );

        // for each friend request that has been accepted it gets the sender of the request
        for (FriendRequest friendRequest:friendRequests) {

            // checks if the sender of the friend request(friend) is online

                if(user.getId().equalsIgnoreCase(friendRequest.getRecipient().getId()) && friendRequest.getSender().getOnlineStatus() == Enum.OnlineStatus.ONLINE ) {
                    //if friend is online, he is added to the friendsOnline list
                    friendsOnline.add(friendRequest.getSender());

                }else if(user.getId().equalsIgnoreCase(friendRequest.getSender().getId()) && friendRequest.getRecipient().getOnlineStatus() == Enum.OnlineStatus.ONLINE ) {

                    friendsOnline.add(friendRequest.getRecipient());

                }else{ }



        }

        // the list is returned
        return friendsOnline;
    }


    @Override
    public List<User> getMutualFriends(User firstUser, User secondUser) {

        List<User> mutualFriends = new ArrayList<>();

        List<User> firstUsersFriends = this.getFriends(firstUser);

        List<User> secondUsersFriends = this.getFriends(secondUser);

        for (User forUserOne: firstUsersFriends) {

            for (User forUserTwo: secondUsersFriends) {

                if(forUserOne.getUserName().equals(forUserTwo.getUserName())){

                    mutualFriends.add(forUserOne);
                }
            }
        }

        return mutualFriends;
    }

    @Override
    public Page<FriendRequest> getAcceptedFriendRequestPages(User user,Enum.FriendRequestStatus friendRequestStatus, PageRequest pageRequest) {

        return friendRequestRepository.getByRecipientOrSenderAndFriendRequestStatus(user,user,friendRequestStatus,pageRequest);

    }

    @Override
    public int countAllFriends(User user) {

        return friendRequestRepository.countAllByRecipientOrSenderAndFriendRequestStatus(user, user, Enum.FriendRequestStatus.ACCEPTED);

    }


}
