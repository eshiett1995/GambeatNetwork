package com.gambeat.site.services;

import com.gambeat.site.entities.TemporaryStore;

/**
 * Created by Oto-obong on 31/10/2017.
 */
public interface TemporaryStoreService {

    TemporaryStore saveTemporaryStore(TemporaryStore temporaryStore);

    TemporaryStore getTemporaryStoreByIdAndToken(long id, String Token);

    void delete(TemporaryStore temporaryStore);

}
