package com.gambeat.site.services;

import com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.third_party.jsonmodel.SocketIOFriendRequestModel;
import com.gambeat.site.controllers.third_party.jsonmodel.SocketIOMessageModel;
import com.gambeat.site.entities.Message;
import retrofit2.Response;

import java.io.IOException;

public interface SocketIOService {

    Response<ResponseModel> sendMessageToSocketIOServer(SocketIOMessageModel message) throws IOException;

    Response<ResponseModel> sendFriendRequestToSocketIOServer(SocketIOFriendRequestModel friendRequest) throws IOException;

}
