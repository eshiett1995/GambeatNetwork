package com.gambeat.site.services;

import com.gambeat.site.models.GenerateStageStrategy;

public interface GenerateStageService {

    String generateStage(GenerateStageStrategy ... generateStageStrategies);

}
