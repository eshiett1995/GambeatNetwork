package com.gambeat.site.services;

import com.gambeat.site.entities.Gamification;

public interface GamificationService {

    Gamification save(Gamification gamification);
}
