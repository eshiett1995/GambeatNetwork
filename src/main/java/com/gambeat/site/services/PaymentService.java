package com.gambeat.site.services;


import com.gambeat.site.controllers.restcontroller.jsonmodel.TransactionModel;
import com.gambeat.site.payment.payant.models.response.WithdrawResponse;
import retrofit2.Response;

import java.io.IOException;

public interface PaymentService {

    Response<WithdrawResponse> makeTransfer(TransactionModel transactionModel) throws IOException;

}
