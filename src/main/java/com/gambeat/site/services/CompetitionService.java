package com.gambeat.site.services;

import com.gambeat.site.entities.Competition;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Game;
import com.gambeat.site.entities.User;
import com.sun.javafx.binding.StringFormatter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CompetitionService {

    Competition save(Competition competition);

    Competition getCompetitionById(String id);

    Competition update(Competition competition);

    Competition getCompetitionByName(String name);

    List<Competition> getMatches(User user);

    Competition checkIfCompetitionExist(Game game, double entryFee);

    Page<Competition> queryCompetitionTable(Competition competition, Pageable pageable);
}
