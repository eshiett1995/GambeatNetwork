package com.gambeat.site.services;

import com.gambeat.site.entities.Game;
import com.gambeat.site.entities.Genre;
import com.gambeat.site.entities.Platform;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GameService {

    Game findGameByName(String name);

    Game save(Game game);

    Page<Game> getAll(Pageable pageable);

    Page<Game> findGamesByNameContaining(String name, Pageable pageable);

    Page<Game> findGamesByPlatform(Platform platform, Pageable pageable);

    Page<Game> findGamesByGenre(Genre genre, Pageable pageable);

    Page<Game> findGamesByNameContainingAndGenre(String name, Genre genre, Pageable pageable);

    Page<Game> findGamesByNameContainingAndPlatform(String name, Platform platform, Pageable pageable);

    Page<Game> findGamesByPlatformAndGenre(Platform platform, Genre genre, Pageable pageable);

    Page<Game> findGamesByNameContainingAndPlatformsAndGenres(String name, Platform platform, Genre genre, Pageable pageable);
}
