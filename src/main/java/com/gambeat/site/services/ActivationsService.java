package com.gambeat.site.services;

import com.gambeat.site.entities.Activations;

public interface ActivationsService {

    Activations save(Activations activations);

    void delete(Activations activations);

    boolean TokenExists(String token);

    Activations GetActivationByToken(String string);
}
