package com.gambeat.site.services;


import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.User;
import org.thymeleaf.context.Context;

public interface MailService {

    Context ComposeUserActivationMail(User user, String token);

    Context ComposeWithdrawalOtpMail(User user, String token);

    Context ComposeUserProfileUpdateMail(User user, String token, Enum.RequestedUpdate requestedUpdate);
}
