package com.gambeat.site.services;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Oto-obong on 18/09/2017.
 */
public interface FileService {

    String saveImage(HttpServletRequest request, MultipartFile file, String userName);
}
