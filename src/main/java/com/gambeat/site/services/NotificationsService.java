package com.gambeat.site.services;

import com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel.NewCompetitionModel;
import com.gambeat.site.entities.*;

public interface NotificationsService {

    Notifications save(Notifications notification);

    Notifications competitionStarted(User user, Competition competition);

    Notifications friendKnockedOutOfCompetition(User user, User looser, Competition competition, NewCompetitionModel newCompetitionModel);

    Notifications friendWonCompetition(User user, User looser, Competition competition, NewCompetitionModel newCompetitionModel);

    Notifications PlayerWinsCompetition(User user,Competition competition);

    Notifications playerWinsMatch(User user, User looser, Competition competition);

    Notifications playerLoosesMatch(User user, User winner, Competition competition);

    Notifications competitionEnded(User user, User winner, Competition competition);

    Notifications friendRequestAccepted(User user, FriendRequest friendRequest);

    Notifications Transaction(User user, double amount, Transactions transactions);

    Notifications competitionEntered(User user, Competition competition);

    Notifications competitionRegistrationDenied(User user, String reason);

    Notifications competitionCouldNotStart(User user, Competition competition);

    Notifications friendRequestGotten(User user, User sender);


}
