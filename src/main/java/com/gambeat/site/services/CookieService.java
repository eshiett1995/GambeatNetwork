package com.gambeat.site.services;

import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.User;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Oto-obong on 18/10/2017.
 */


public interface CookieService {

    boolean hasValidCookie(HttpServletRequest request);

    Cookie generateCookie(String sessionID);

    Cookie expireCookie();

    User getUser(String cookieName, HttpServletRequest request);

}
