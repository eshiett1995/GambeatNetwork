package com.gambeat.site.services;

import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by Oto-obong on 16/08/2017.
 */
public interface MessageService {

    Message save(Message message);

    Message update(Message message);

    Page<Message> getMessage(User user1, User user2, Pageable pageable);

    void addMessage (Message message);

    List<Message> getUnreadChats(User receiver);

    int countUnreadMessagesFromSender(User sender, User receiver, boolean isRead);

    int countTotalUnreadMessages(User receiver);

    Page<Message> findMostRecentMessageFromConversation(User user,Pageable pageable);

    Message findById(String id);

}
