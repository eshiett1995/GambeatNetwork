package com.gambeat.site.services;

import com.gambeat.site.entities.Platform;

public interface PlatformService {

    Platform findPlatformByName(String name);

    Platform save(Platform platform);

    Platform update(Platform platform);
}
