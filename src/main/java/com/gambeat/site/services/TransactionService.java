package com.gambeat.site.services;

import com.gambeat.site.entities.Transactions;
import com.gambeat.site.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TransactionService {

    Transactions save(Transactions transaction);

    Transactions update(Transactions transaction);

    Transactions getAccurateTransaction(Transactions transactions);

    Page<Transactions> getUsersTransactions(User user, Pageable pageable);

}
