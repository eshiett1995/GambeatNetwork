package com.gambeat.site.services;

import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.Punter;
import com.gambeat.site.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Oto-obong on 16/08/2017.
 */


public interface UserDAOService {

    Page<User> findUser(String key, Pageable pageable);

    User saveUser(User user);

    List<User> getAllUser (User user);

    User getUser(String userID);

    User updateUser(User user);

    void deleteUser(int userID);

    void deleteUser(User user);

    void verifyUser(User user);

    void verifyUser(String userName);

    User getByUsername(String userName);

    User getByEmail(String email);

    User getUserByPhoneNumber(String phoneNumber);

    boolean isLocked(User user);

    Page<User> getFriends(User user, Pageable pageable);

    List<User> getFriends(User user);



}
