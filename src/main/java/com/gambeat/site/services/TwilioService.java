package com.gambeat.site.services;

import com.twilio.rest.api.v2010.account.Message;

public interface TwilioService {

    Message sendMessage(String recipientNumber, String senderNumber, String message);

}
