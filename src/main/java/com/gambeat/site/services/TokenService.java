package com.gambeat.site.services;

/**
 * Created by Oto-obong on 29/10/2017.
 */
public interface TokenService {

    String generateToken(int length);

    String generateActivationToken(int length);

    String generateWithdrawalToken();
}
