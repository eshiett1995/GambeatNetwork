package com.gambeat.site.controllers;

import com.gambeat.site.controllers.restcontroller.jsonmodel.DashboardModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.FriendRequestsModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.FriendsOnlineModel;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultFriendRequestService;
import com.gambeat.site.services.implementation.DefaultMessageService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;

/**
 * Created by Oto-obong on 15/08/2017.
 */


// Note: Controller made to switch view when needed.

@Controller
public class SwitchViewController {

    @Autowired
    DefaultCookieService defaultCookieService;

    @Autowired
    DefaultMessageService defaultMessageService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Autowired
    FriendsOnlineModel friendsOnlineModel;

    @Autowired
    DashboardModel dashboardModel;

    @Autowired
    FriendRequestsModel friendRequestsModel;

    @Autowired
    DefaultFriendRequestService defaultFriendRequestService;


    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchViewController.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String GambeatNetworkPage(Model model, HttpServletRequest request) throws IOException {

        User user = this.defaultCookieService.getUser("gambeat",request);

        friendsOnlineModel.setFriendsOnline(defaultFriendRequestService.getFriendsOnline(user) == null ? Collections.EMPTY_LIST : defaultFriendRequestService.getFriendsOnline(user));


        for (User friend: friendsOnlineModel.getFriendsOnline()) {

            friend.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(friend,user,false));

        }


        dashboardModel.setChats(defaultMessageService.getUnreadChats(user) == null ? Collections.EMPTY_LIST : defaultMessageService.getUnreadChats(user));

        dashboardModel.setFriendRequests(defaultFriendRequestService.getAllPendingFriendRequests(user) == null ? Collections.EMPTY_LIST : defaultFriendRequestService.getAllPendingFriendRequests(user));

        dashboardModel.setUser(user);

        dashboardModel.setUnreadMessagesCount(defaultMessageService.countTotalUnreadMessages(user));

        model.addAttribute("dashboard_model", dashboardModel);

        model.addAttribute("friends_online", friendsOnlineModel);


       // model.addAttribute("friend_request", new FriendRequest());

        return "dashboard";

    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String WelcomePage(){





      /**  Chat chat = new Chat();

        chat.setDateCreated();
        chat.setParticipantOne(defaultUserDAOService.getUser(Long.valueOf(1)));
        chat.setParticipantTwo(defaultUserDAOService.getUser(Long.valueOf(2)));

        defaultChatService.save(chat);

        Chat c = defaultChatService.findChatById(Long.valueOf(123));

        Message m = new Message().setMessage("hello mfon")
                .setRead(false).setSender(defaultUserDAOService.getUser(Long.valueOf(1)))
                .setReceiver(defaultUserDAOService.getUser(Long.valueOf(2)))
                .setChat(c);
        m.setDateCreated();

        Message m2 = new Message().setMessage("how have u been")
                .setRead(false).setSender(defaultUserDAOService.getUser(Long.valueOf(1)))
                .setReceiver(defaultUserDAOService.getUser(Long.valueOf(2)))
                .setChat(c);
        m2.setDateCreated();

        List<Message> ml = new ArrayList<Message>();

        ml = c.getMessages();

        if(ml == null){

            List<Message> b = new ArrayList<>();

            b.add(m);
            b.add(m2);
            c.setMessages(b);


        }else{

            ml.add(m);
            ml.add(m2);
            c.setMessages(ml);

        }

        defaultChatService.update(c); **/

        return "welcomepage";
    }

    @RequestMapping(value = "/locked", method = RequestMethod.GET)
    public String Locked(HttpServletRequest request){

        User user = this.defaultCookieService.getUser("gambeat", request);

        user.setLocked(true);

        defaultUserDAOService.updateUser(user);

        return "locked-screen";
    }


}
