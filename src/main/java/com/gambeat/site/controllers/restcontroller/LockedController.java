package com.gambeat.site.controllers.restcontroller;

import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.UserModel;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Oto-obong on 06/11/2017.
 */

@RestController
@RequestMapping(value = "/locked")
public class LockedController {

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private DefaultCookieService defaultCookieService;

    @Autowired
    private DefaultUserDAOService defaultUserDAOService;

    @Autowired
    private ResponseModel responseModel;


    @ResponseBody
    @RequestMapping(value = "/unlock", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> UnlockAccount(@RequestBody String password, HttpServletRequest request, HttpServletResponse response) {

        try {


            User user = this.defaultCookieService.getUser("gambeat", request);

            if (bCryptPasswordEncoder.matches(password, user.getPassword())){

                user.setLocked(false);

                defaultUserDAOService.updateUser(user);

                responseModel.setResponseMessage("Successful").setIsSuccessful(true);

                return new ResponseEntity<>(responseModel, HttpStatus.OK);

            } else {

                responseModel.setResponseMessage("Incorrect password!").setIsSuccessful(false);

                return new ResponseEntity<>(responseModel, HttpStatus.OK);

            }

        }catch (Exception ex){

            responseModel.setResponseMessage("Oops! Connection error.").setIsSuccessful(false);

            return new ResponseEntity<>(responseModel, HttpStatus.OK);

        }

    }

 }
