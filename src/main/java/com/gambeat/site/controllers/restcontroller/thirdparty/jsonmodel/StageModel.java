package com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel;

import java.util.ArrayList;

import java.util.List;

public class StageModel {



  private int round;

  private List<PairModel> pairs = new ArrayList<>();

  private String immunity;

    public int getRound() {

        return round;

    }

    public StageModel setRound(int round) {

        this.round = round;

        return this;

    }

    public List<PairModel> getPairs() {

        return pairs;

    }

    public StageModel setPairs(List<PairModel> pairs) {

        this.pairs = pairs;

        return this;

    }

    public String getImmunity() {

        return immunity;

    }

    public StageModel setImmunity(String immunity) {

        this.immunity = immunity;

        return this;
    }
}
