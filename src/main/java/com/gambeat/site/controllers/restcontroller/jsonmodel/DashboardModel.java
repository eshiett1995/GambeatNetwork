package com.gambeat.site.controllers.restcontroller.jsonmodel;


import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DashboardModel {


    private List<FriendRequest> friendRequests;

    private List<User> friendsOnline;

    private int unreadMessagesCount;

    private List<Message> chats;

    private User user;

    Boolean isSuccessful;


    public int getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public DashboardModel setUnreadMessagesCount(int unreadMessagesCount) {

        this.unreadMessagesCount = unreadMessagesCount;

        return this;
    }

    public List<FriendRequest> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(List<FriendRequest> friendRequests) {
        this.friendRequests = friendRequests;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(Boolean successful) {
        isSuccessful = successful;
    }

    public List<User> getFriendsOnline() {

        return friendsOnline;

    }

    public List<Message> getChats() {
        return chats;
    }

    public void setChats(List<Message> chats) {
        this.chats = chats;
    }

    public void setFriendsOnline(List<User> friendsOnline) {
        this.friendsOnline = friendsOnline;
    }
}
