package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.Competition;

public class CompetitionResponseModel {

    public CompetitionResponseModel(Competition competition, ResponseModel responseModel){

        this.competition = competition;

        this.responseModel = responseModel;

    }

    public CompetitionResponseModel(ResponseModel responseModel){


        this.responseModel = responseModel;

    }

    private Competition competition;

    private ResponseModel responseModel;

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public void setResponseModel(ResponseModel responseModel) {
        this.responseModel = responseModel;
    }
}
