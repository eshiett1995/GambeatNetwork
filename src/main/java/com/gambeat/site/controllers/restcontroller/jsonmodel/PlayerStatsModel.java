package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.PlayerStats;
import com.gambeat.site.entities.User;
import org.springframework.stereotype.Component;

/**
 * Created by Oto-obong on 02/09/2017.
 */


@Component
public class PlayerStatsModel {


    private User user;

    private PlayerStats playerStats;

    private Boolean isSuccessful;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PlayerStats getPlayerStats() {
        return playerStats;
    }

    public void setPlayerStats(PlayerStats playerStats) {
        this.playerStats = playerStats;
    }

    public Boolean getSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(Boolean successful) {
        isSuccessful = successful;
    }
}
