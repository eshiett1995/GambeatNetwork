package com.gambeat.site.controllers.restcontroller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Oto-obong on 26/09/2017.
 */

@RestController()
@RequestMapping(value = "/switch/to")
public class SwitchUrlController {

    @ResponseBody
    @RequestMapping("/index")
    public void SwitchToIndexPage(HttpServletResponse response) throws IOException {

        response.sendRedirect("/");
    }
}
