package com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel;

public class NewCompetitionModel {

    private String competitionName;

    private  String gameName;

    private double entryFee;

    private StageModel stage;

    public String getCompetitionName() {
        return competitionName;
    }

    public NewCompetitionModel setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
        return this;
    }

    public String getGameName() {
        return gameName;
    }

    public NewCompetitionModel setGameName(String gameName) {
        this.gameName = gameName;
        return this;
    }

    public double getEntryFee() {
        return entryFee;
    }

    public NewCompetitionModel setEntryFee(double entryFee) {
        this.entryFee = entryFee;
        return this;
    }

    public StageModel getStage() {
        return stage;
    }

    public NewCompetitionModel setStage(StageModel stage) {
        this.stage = stage;
        return this;
    }
}
