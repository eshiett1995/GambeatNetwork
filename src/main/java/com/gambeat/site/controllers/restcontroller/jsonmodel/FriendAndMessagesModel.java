package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;

import java.util.ArrayList;
import java.util.List;

public class FriendAndMessagesModel {

        private User friend;

        private List<Message> messages = new ArrayList<>();

        public User getFriend() {
                return friend;
        }

        public FriendAndMessagesModel setFriend(User friend) {
                this.friend = friend;
                return this;
        }

        public List<Message> getMessages() {
                return messages;
        }

        public FriendAndMessagesModel setMessages(List<Message> messages) {
                this.messages = messages;
                return this;
        }
}


