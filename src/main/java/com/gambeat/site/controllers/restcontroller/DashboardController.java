package com.gambeat.site.controllers.restcontroller;

import com.corundumstudio.socketio.SocketIOClient;
import com.gambeat.site.controllers.restcontroller.jsonmodel.*;
import com.gambeat.site.entities.*;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.pushnotification.firebase.service.DefaultFirebaseService;
import com.gambeat.site.repositories.UserRepository;
import com.gambeat.site.services.implementation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.gambeat.GambeatNetworkApplication.server;

/**
 * Created by Oto-obong on 02/09/2017.
 */

@RestController
@RequestMapping(value = "/dashboard")
public class DashboardController {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;


    @Autowired
    UserRepository userRepository;

    @Autowired
    private DefaultCookieService defaultCookieService;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardController.class);


    @Autowired
    private DefaultUserDAOService defaultUserDAOService;


    @Autowired
    private DefaultFriendRequestService defaultFriendRequestService;

    @Autowired
    private FriendRequestsModel friendRequestsModel;

    @Autowired
    private FriendsOnlineModel friendsOnlineModel;

    @Autowired
    private FriendsModel friendsModel;

    @Autowired
    private DefaultNotificationService defaultNotificationService;


    @Autowired
    ResponseModel responseModel;

    @Autowired
    private DefaultMessageService defaultMessageService;

    @Autowired
    DefaultFirebaseService defaultFirebaseService;


    @Autowired
    private PaginationModel paginationModel;

    @ResponseBody
    @RequestMapping(value = "/friendrequests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FriendRequestsModel> FetchFriendRequests(HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // gets all the friend quests the user has and returns an empty list if the user has no friends
            friendRequestsModel.setFriendRequests(this.defaultFriendRequestService.getAllFriendRequest(user) == null ? Collections.EMPTY_LIST : this.defaultFriendRequestService.getAllFriendRequest(user));

            // sets isSuccessful to true to show that the request was successful
            friendRequestsModel.setSuccessful(true);

            return new ResponseEntity<>(friendRequestsModel, HttpStatus.OK);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            friendRequestsModel.setFriendRequests(Collections.EMPTY_LIST);

            friendRequestsModel.setSuccessful(false);

            return new ResponseEntity<>(friendRequestsModel, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    /**@ResponseBody
    @RequestMapping(value = "/chats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ChatsModel> FetchChats(HttpServletRequest request, HttpServletResponse response) {

        try {

            user = this.defaultCookieService.getUser("gambeat", request);

            // gets all chats by the user but returns an empty list if the chat is empty
            chatsModel.setChats(this.defaultChatService.getChatsByUser(user) == null ? Collections.EMPTY_LIST : this.defaultChatService.getChatsByUser(user));

            // sets the isSuccessful to true, to indicate the request was successful
            chatsModel.setSuccessful(true);

            return new ResponseEntity<>(chatsModel, HttpStatus.OK);


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            // returns an empty list
            chatsModel.setChats(null);

            // sets the isSuccessful to false, to indicate the request was not successful
            chatsModel.setSuccessful(false);

            return new ResponseEntity<>(chatsModel, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    } **/

    @ResponseBody
    @RequestMapping(value = "/friendsonline", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FriendsOnlineModel> FetchFriendsOnline(HttpServletRequest request) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // gets all users that are online and sets them in the friendsOnlineModel
            friendsOnlineModel.setFriendsOnline(defaultFriendRequestService.getFriendsOnline(user) == null ? Collections.EMPTY_LIST : defaultFriendRequestService.getFriendsOnline(user));

            friendsOnlineModel.setSuccessful(true);

            return new ResponseEntity<>(friendsOnlineModel, HttpStatus.OK);


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            friendsOnlineModel.setFriendsOnline(null);

            friendsOnlineModel.setSuccessful(false);

            return new ResponseEntity<>(friendsOnlineModel, HttpStatus.INTERNAL_SERVER_ERROR);

        }


    }

    @ResponseBody
    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public ResponseEntity<FriendsModel> FetchFriends(HttpServletRequest request) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            friendsModel.setFriends(defaultFriendRequestService.getFriends(user) == null ? Collections.EMPTY_LIST : defaultFriendRequestService.getFriends(user));

            friendsModel.setSuccessful(true);

            return new ResponseEntity<>(friendsModel, HttpStatus.OK);


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            friendsModel.setFriends(null);

            friendsModel.setSuccessful(false);

            return new ResponseEntity<>(friendsModel, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @ResponseBody
    @RequestMapping(value = "/friendrequest/accept/{username}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> AcceptRequests(@PathVariable("username") String friendUsername, HttpServletRequest request) {


        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getByUsername(friendUsername);

            // Gets the specific friend requests of both users
            FriendRequest friendRequest = defaultFriendRequestService.findFriendRequestByUsers(user, user2);


            // checks if the request is pending and if it is sets it to accepted
            if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.PENDING) {

                friendRequest.setFriendRequestStatus(Enum.FriendRequestStatus.ACCEPTED);

                defaultFriendRequestService.updateFriendRequest(friendRequest);

                if(user.getOnlineStatus() == Enum.OnlineStatus.ONLINE && user2.getOnlineStatus() == Enum.OnlineStatus.ONLINE ){

                    messagingTemplate.convertAndSendToUser(user2.getUserName(),"/topic/friend/online", user.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(user, user2, false)));

                    messagingTemplate.convertAndSendToUser(user.getUserName(),"/topic/friend/online", user2.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(user2, user, false)));

                }

                defaultNotificationService.friendRequestAccepted(user, friendRequest);

                defaultFirebaseService.composeFriendRequestAcceptedNotification(user, user2, friendRequest).forEach(

                        firebaseNotificationModel -> {

                            defaultFirebaseService.sendNotification(firebaseNotificationModel);

                        }
                );


                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request has been successfully accepted");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);


                // checks if the requests has been accepted  and if it has leaves it as accepted
            } else if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.ACCEPTED) {


                // logs an warning to the database - accepted requests are not supposed to show up here
                LOGGER.warn("Friend request had already been accepted, but is still shown to the user");


                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request has already been accepted");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);


                // if the request has already been declined it deletes it from the database
            } else {

                // logs an warning to the database - declined requests are supposed to be deleted and not show up here
                LOGGER.warn("Friend request had already been declined, but is still in the database");

                defaultFriendRequestService.deleteFriendRequest(friendRequest);

                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request had already been declined");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);

            }


        } catch (Exception ex) { // in case of error the server responds with this

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage("Connection error, please try again!");

            return new ResponseEntity<>(responseModel, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }




    @ResponseBody
    @RequestMapping(value = "/friendrequest/decline/{username}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> DeclineRequests(@PathVariable("username") String friendUsername, HttpServletRequest request) {


        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getByUsername(friendUsername);

            // Gets the specific friend requests of both users
            FriendRequest friendRequest = defaultFriendRequestService.findFriendRequestByUsers(user, user2);


            // checks if the request is pending and if it is sets it to accepted
            if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.PENDING) {

                defaultFriendRequestService.deleteFriendRequest(friendRequest);

                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request has been successfully rejected");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);


                // checks if the requests has been accepted  and if it has leaves it as accepted
            } else if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.ACCEPTED) {


                defaultFriendRequestService.deleteFriendRequest(friendRequest);

                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request has been removed");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);


                // if the request has already been declined it deletes it from the database
            } else {

                // logs an warning to the database - declined requests are supposed to be deleted and not show up here
                LOGGER.warn("Friend request had already been declined, but is still in the database");

                defaultFriendRequestService.deleteFriendRequest(friendRequest);

                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request had already been declined");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);

            }


        } catch (Exception ex) { // in case of error the server responds with this

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage("Connection error, please try again!");

            return new ResponseEntity<>(responseModel, HttpStatus.OK);

        }

    }




    @ResponseBody
    @RequestMapping(value = "/friendrequest/send/{username}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> SendRequests(@PathVariable("username") String username, HttpServletRequest request) {


        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getByUsername(username);

            // Gets the specific friend requests of both users
            FriendRequest friendRequest = new FriendRequest();

            friendRequest.setFriendRequestStatus(Enum.FriendRequestStatus.PENDING)
                    .setSender(user)
                    .setRecipient(user2)
                    .setDateCreated();

            FriendRequest savedFriendRequest = defaultFriendRequestService.save(friendRequest);

            messagingTemplate.convertAndSendToUser(user2.getUserName(),"/topic/friend-request/receive/request", savedFriendRequest);

            if(!Objects.isNull(server)){

                SocketIOClient socketIOClient = server.getAllClients().stream().filter(

                        client -> client.get("name").equals(username)

                ).findFirst().get();

                socketIOClient.sendEvent("friend request", savedFriendRequest);
            }

            responseModel.setIsSuccessful(true)

                  .setResponseMessage("Success");

            return new ResponseEntity<>(responseModel, HttpStatus.OK);


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage("Connection error, please try again!");

            return new ResponseEntity<>(responseModel, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @ResponseBody
    @RequestMapping(value = "/friendrequest/remove/{username}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> RemoveFriendship(@PathVariable("username") String username, HttpServletRequest request) {


        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getByUsername(username);

            // Gets the specific friend requests of both users
            FriendRequest friendRequest = defaultFriendRequestService.findFriendRequestByUsers(user, user2);

            if(friendRequest != null)

            defaultFriendRequestService.deleteFriendRequest(friendRequest);

            responseModel.setIsSuccessful(true)

                    .setResponseMessage("Success");

            return new ResponseEntity<>(responseModel, HttpStatus.OK);


        } catch (Exception ex) { // in case of error the server responds with this

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage("Connection error, please try again!");

            return new ResponseEntity<>(responseModel, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }



    @ResponseBody
    @RequestMapping(value = "/users/query?{username}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> GetUsers(@PathVariable("username") String friendUsername, HttpServletRequest request) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getByUsername(friendUsername);

            // Gets the specific friend requests of both users
            FriendRequest friendRequest = defaultFriendRequestService.findFriendRequestByUsers(user, user2);

            // if the request gotten for the database is pending, delete it (shows the friend request has been rejected);
            if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.PENDING) {

                defaultFriendRequestService.deleteFriendRequest(friendRequest);

                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request has already been successfully declined");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);


            } else if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.ACCEPTED) {


                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request has already been accepted");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);


            } else {

                LOGGER.warn("Friend request was already declined, but it was seen in the database");

                defaultFriendRequestService.deleteFriendRequest(friendRequest);

                responseModel.setIsSuccessful(true);

                responseModel.setResponseMessage("Friend request has already been deleted");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);

            }


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage("Connection error, please try again!");

            return new ResponseEntity<>(responseModel, HttpStatus.INTERNAL_SERVER_ERROR);


        }
    }

    @ResponseBody
    @RequestMapping(value = "/search/{username}/{page}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<User> FindUsers(@PathVariable String username,@PathVariable int page, Pageable pageable, HttpServletRequest request) {

        return defaultUserDAOService.findUser(username, new PageRequest(page,6));

    }


    @ResponseBody
    @RequestMapping(value = "/message/{userID}/{page}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginationModel> GetPaginatedMessage(@PathVariable Long userID, @PathVariable int page, Pageable pageable, HttpServletRequest request) {


        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getUser(String.valueOf(userID));

            if(page == 0) {

                Page<Message> firstPage = defaultMessageService.getMessage(user, user2, new PageRequest(0, 6));

                paginationModel.setMessagesPage(firstPage)
                        .setResponseMessage("Successful")
                        .setSuccessful(true);

            }else {

                paginationModel.setMessagesPage(defaultMessageService.getMessage(user, user2, new PageRequest(page, 6)))
                        .setResponseMessage("Successful")
                        .setSuccessful(true);

            }

            return new ResponseEntity<>(paginationModel, HttpStatus.OK);

        }catch (Exception ex){

            paginationModel.setResponseMessage("False")
                                  .setSuccessful(false);

            return new ResponseEntity<>(paginationModel, HttpStatus.OK);

        }

    }

}
