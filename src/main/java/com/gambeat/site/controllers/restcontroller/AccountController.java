package com.gambeat.site.controllers.restcontroller;


import com.gambeat.site.controllers.restcontroller.jsonmodel.PlayerStatsModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.TransactionModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.WithdrawModel;
import com.gambeat.site.entities.PlayerStats;
import com.gambeat.site.entities.TemporaryStore;
import com.gambeat.site.entities.Transactions;
import com.gambeat.site.entities.User;
import com.gambeat.site.network.retrofit.RetrofitCallback;
import com.gambeat.site.payment.payant.models.response.WithdrawResponse;
import com.gambeat.site.services.TemporaryStoreService;
import com.gambeat.site.services.TransactionService;
import com.gambeat.site.services.implementation.*;
import com.gambeat.site.utility.AppUtil;
import com.gambeat.site.utility.MyExclusionStrategy;
import com.gambeat.site.utility.datatable.DataTableRequest;
import com.gambeat.site.utility.datatable.DataTableResults;
import com.gambeat.site.utility.datatable.PaginationCriteria;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import flexjson.JSONSerializer;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import retrofit2.Response;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/account")
public class AccountController {

    @Autowired
    DefaultPaymentService defaultPaymentService;

    @Autowired
    DefaultTokenService defaultTokenService;

    @Autowired
    private DefaultCookieService defaultCookieService;

    @Autowired
    private DefaultTransactionService defaultTransactionService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Autowired
    DefaultSmsService defaultSmsService;

    @Autowired
    DefaultMailService defaultMailService;

    @PersistenceContext
    private EntityManager entityManager;

    protected final transient Logger LOGGER = LoggerFactory.getLogger(AccountController.class);





    @ResponseBody
    @RequestMapping(value = "/deposit/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionModel> SaveTransaction(@RequestBody TransactionModel transactionModel, HttpServletRequest request, HttpServletResponse response){

        try {


            User user = this.defaultCookieService.getUser("gambeat", request);

            transactionModel.getTransaction().setPlayer(user)
                    .setTransactiontype(Transactions.TRANSACTIONTYPE.DEPOSIT)
                    .setDetail("Deposit into Gambeat account")
                    .setDateCreated();

            transactionModel.setTransaction(defaultTransactionService.save(transactionModel.getTransaction()));


            /*******************************UPDATE USERS WALLET & SAVE LOGIC************************************************/

            transactionModel.getTransaction().getPlayer().setMoney(transactionModel.getTransaction().getPlayer().getMoney() + transactionModel.getTransaction().getAmount());

            transactionModel.getTransaction().getPlayer().getPlayerStats().setWallet(transactionModel.getTransaction().getPlayer().getPlayerStats().getWallet() + transactionModel.getTransaction().getAmount());

            defaultUserDAOService.updateUser(transactionModel.getTransaction().getPlayer());

            /***************************************************************************************************************/



            transactionModel.setResponseModel(new ResponseModel().setIsSuccessful(true).setResponseMessage("User found"));

            return new ResponseEntity<>(transactionModel, HttpStatus.OK); // return response to client.


        }catch (Exception ex){

              LOGGER.error(ex.getMessage());

              TransactionModel responseModel = new TransactionModel();

              responseModel.setResponseModel(new ResponseModel().setIsSuccessful(false).setResponseMessage(ex.getMessage()));

              return new ResponseEntity<>(responseModel, HttpStatus.OK); // return response to client.


        }

    }


    @ResponseBody
    @RequestMapping(value = "/deposit/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionModel> UpdateTransaction(@RequestBody TransactionModel transactionModel, HttpServletRequest request, HttpServletResponse response){

        try {


            transactionModel.setTransaction(defaultTransactionService.update(transactionModel.getTransaction()));

            transactionModel.setResponseModel(new ResponseModel().setIsSuccessful(true).setResponseMessage("User found"));

            return new ResponseEntity<>(transactionModel, HttpStatus.OK); // return response to client.


        }catch (Exception ex){

            LOGGER.error(ex.getMessage());

            TransactionModel responseModel = new TransactionModel();

            responseModel.setResponseModel(new ResponseModel().setIsSuccessful(false).setResponseMessage(ex.getMessage()));

            return new ResponseEntity<>(responseModel, HttpStatus.OK); // return response to client.


        }

    }


    @ResponseBody
    @RequestMapping(value = "/withdraw", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionModel> WithdrawalPage(HttpServletRequest request, HttpServletResponse response){

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            TransactionModel transactionModel = new TransactionModel();

            transactionModel.setWithdrawModel(new WithdrawModel().setAvailableBalance(user.getMoney())

                    .setViaEmail(true));

            return new ResponseEntity<>(transactionModel, HttpStatus.OK); // return response to client.


        }catch (Exception ex){

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new TransactionModel(), HttpStatus.OK); // return response to client.

        }

    }

    @ResponseBody
    @RequestMapping(value = "/initiate/withdrawal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionModel> InitiateWithdrawal(@RequestBody TransactionModel transactionModel,HttpServletRequest request, HttpServletResponse response){

        if((transactionModel.getWithdrawModel().getAvailableBalance() - transactionModel.getWithdrawModel().getWithdrawalAmount()) < 100){

            String errorMsg = "Please note, according to Gambeat's policy, you cannot withdraw everything";

            return new ResponseEntity<>(new TransactionModel().setResponseModel(new ResponseModel(false, errorMsg)), HttpStatus.OK); // return response to client.

        }

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            if(transactionModel.getWithdrawModel().isViaEmail()){

                defaultMailService.ComposeWithdrawalOtpMail(user, defaultTokenService.generateWithdrawalToken());

            }else{

                System.out.println("it got here");

                defaultSmsService.ComposeWithdrawalTokenSms(user, "08098516969",defaultTokenService.generateWithdrawalToken());

            }

            transactionModel.setTransaction(new Transactions()
                    .setPlayer(user)
                    .setAmount(transactionModel.getWithdrawModel().getWithdrawalAmount())
                    .setTransactiontype(Transactions.TRANSACTIONTYPE.WITHDRAW)
                    .setPlayer(user)
                    .setDetail("Withdrawal from user's Gambeat account"))

            .setResponseModel(new ResponseModel(true, "Transaction saved"));

            transactionModel.getTransaction().setDateCreated();

            transactionModel.setTransaction(defaultTransactionService.save(transactionModel.getTransaction()));

            return new ResponseEntity<>(transactionModel, HttpStatus.OK); // return response to client.


        }catch (Exception ex){

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new TransactionModel().setResponseModel(new ResponseModel(false, "Ooops! an error occurred")), HttpStatus.OK); // return response to client.

        }

    }


    @ResponseBody
    @RequestMapping(value = "/finalize/withdrawal", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionModel> FinalizeWithdrawal(@RequestBody TransactionModel transactionModel,HttpServletRequest request, HttpServletResponse response){

        System.out.println("it got ere");

        try {

            Transactions foundTransaction =  defaultTransactionService.getAccurateTransaction(transactionModel.getTransaction());

            transactionModel.setTransaction(foundTransaction);

            Response<WithdrawResponse> withdrawResponse = defaultPaymentService.makeTransfer(transactionModel);

            if (!withdrawResponse.isSuccessful()){

                return new ResponseEntity<>(transactionModel.setResponseModel(new ResponseModel(false,withdrawResponse.message())), HttpStatus.OK); // return response to client.


            }else {




                if (!withdrawResponse.body().getStatus().equalsIgnoreCase("success")) {

                    return new ResponseEntity<>(transactionModel.setResponseModel(new ResponseModel(false, withdrawResponse.message())), HttpStatus.OK); // return response to client.

                }else {


                    transactionModel.getTransaction().setReferenceID(withdrawResponse.body().getData().getReference_code());

                    Transactions updatedTransaction = defaultTransactionService.save(transactionModel.getTransaction());

                    transactionModel.setTransaction(updatedTransaction);

                    transactionModel.getTransaction()

                            .getPlayer().setMoney(transactionModel.getTransaction().getPlayer().getMoney() - transactionModel.getTransaction().getAmount())
                            .setPlayerStats(transactionModel.getTransaction().getPlayer()
                                    .getPlayerStats().setWallet(transactionModel.getTransaction().getPlayer().getPlayerStats().getWallet() - transactionModel.getTransaction().getAmount()));

                    User updatedUser = defaultUserDAOService.updateUser(transactionModel.getTransaction().getPlayer());

                    transactionModel.getTransaction().setPlayer(updatedUser);

                    return new ResponseEntity<>(transactionModel.setResponseModel(new ResponseModel(true, "Successful")), HttpStatus.OK); // return response to client.

                }
            }

        } catch (IOException e) {

            return new ResponseEntity<>(transactionModel.setResponseModel(new ResponseModel(false,"Please check your input and try again")), HttpStatus.OK); // return response to client.


        }

    }

    @ResponseBody
    @RequestMapping(value = "/query/data-table/transaction",method = RequestMethod.GET)
    public String QueryTransactionsTable(HttpServletRequest request){

        DataTableRequest<Transactions> dataTableInRQ = new DataTableRequest<Transactions>(request);

        PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();

        String baseQuery = "SELECT id as id, time as time, player as player, amount as amount, transactiontype as transactiontype, referenceid as referenceid, detail as detail, token as token, (SELECT COUNT(1) FROM TRANS) AS totalrecords  FROM TRANS";
        String paginatedQuery = AppUtil.buildPaginatedQuery(baseQuery, pagination);

        System.out.println(paginatedQuery);

        Query query = entityManager.createNativeQuery(paginatedQuery, Transactions.class);

        @SuppressWarnings("unchecked")

        List<Transactions> transactionsList = query.getResultList();



        DataTableResults<Transactions> dataTableResult = new DataTableResults<Transactions>();
        dataTableResult.setDraw(dataTableInRQ.getDraw());
        dataTableResult.setListOfDataObjects(transactionsList);
        if (!AppUtil.isObjectEmpty(transactionsList)) {
            dataTableResult.setRecordsTotal(String.valueOf(transactionsList.size())
            );
            if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
                dataTableResult.setRecordsFiltered(String.valueOf(transactionsList.size()));
            } else {
                dataTableResult.setRecordsFiltered(String.valueOf(transactionsList.size()));
            }
        }

        Gson g = new GsonBuilder()
                .setExclusionStrategies(new MyExclusionStrategy())
                .create();

        System.out.println(dataTableResult.getListOfDataObjects().size());




        return g.toJson(dataTableResult);

    }
}
