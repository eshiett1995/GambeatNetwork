package com.gambeat.site.controllers.restcontroller.thirdparty;

import com.corundumstudio.socketio.SocketIOClient;
import com.gambeat.site.controllers.restcontroller.WelcomeController;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.UserModel;
import com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel.GameServerPlayerModel;
import com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel.NewCompetitionModel;
import com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel.PairModel;
import com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel.StageModel;
import com.gambeat.site.entities.*;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.pushnotification.firebase.models.request.FirebaseNotificationModel;
import com.gambeat.site.pushnotification.firebase.service.DefaultFirebaseService;
import com.gambeat.site.services.CookieService;
import com.gambeat.site.services.implementation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.gambeat.GambeatNetworkApplication.server;

/**
 * Created by Oto-obong on 16/03/2018.
 */

@RestController
@RequestMapping(value = "/gameserver")
public class UnityController {


    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    CookieService cookieService;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(WelcomeController.class);

    @Autowired
    DefaultGamificationService defaultGamificationService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Autowired
    DefaultGameService defaultGameService;

    @Autowired
    DefaultNotificationService defaultNotificationService;

    @Autowired
    DefaultCompetitionService defaultCompetitionService;

    @Autowired
    DefaultFirebaseService defaultFirebaseService;

    @Autowired
    DefaultTransactionService defaultTransactionService;

    @Autowired
    DefaultFriendRequestService defaultFriendRequestService;




    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameServerPlayerModel> Login(@RequestBody User user){

        // check if the username and password provided matches a user in the database.
        String verifiedResponse = this.defaultUserDAOService.verification(user);

        GameServerPlayerModel gameServerPlayerModel = new GameServerPlayerModel();

        if(verifiedResponse.equalsIgnoreCase("Verified")){ // if the user was verified successfully (Found in the database)

            gameServerPlayerModel.setUser(defaultUserDAOService.getByUsername(user.getUserName()))
                    .setResponseModel(new ResponseModel(true, "Success"));


            return new ResponseEntity<>(gameServerPlayerModel, HttpStatus.OK); // return response to client.


        }else{

            gameServerPlayerModel.setUser(new User())
                    .setResponseModel(new ResponseModel(false, "Password/Username incorrect"));

            return new ResponseEntity<>(gameServerPlayerModel, HttpStatus.OK); // return response to client.

        }


    }


    @ResponseBody
    @RequestMapping(value = "/player/{id}", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameServerPlayerModel> GetPlayer(@PathVariable("id") String id){



        GameServerPlayerModel gameServerPlayerModel = new GameServerPlayerModel();

            gameServerPlayerModel.setUser(defaultUserDAOService.getUser(id))

                    .setResponseModel(new ResponseModel(true, "Success"));


            return new ResponseEntity<>(gameServerPlayerModel, HttpStatus.OK); // return response to client.

    }


    @ResponseBody
    @RequestMapping(value = "/has-money/{id}/{amount}", method = RequestMethod.GET)
    public ResponseEntity<ResponseModel> CheckIfPlayerHasMoney(@PathVariable("id") String id,@PathVariable("amount") double amount){

        User user = defaultUserDAOService.getUser(id);

        if(user.getMoney() == amount){

            return new ResponseEntity<>(new ResponseModel(false, "You cannot use all the money in your wallet"),HttpStatus.OK); // return response to client.

        }else if(user.getMoney() < amount){

            return new ResponseEntity<>(new ResponseModel(false, "You do not have enough money"),HttpStatus.OK); // return response to client.

        }else{

            return new ResponseEntity<>(new ResponseModel(true, "Success"),HttpStatus.OK); // return response to client.

        }
    }


    @ResponseBody
    @RequestMapping(value = "/competition/create", method = RequestMethod.POST)
    public ResponseEntity<ResponseModel> CreateCompetition(@RequestBody GameServerPlayerModel gameServerPlayerModel){

        User user = defaultUserDAOService.getUser(gameServerPlayerModel.getUser().getId());

        if(user.getMoney() < gameServerPlayerModel.getEntryFee()){

            String reason = "Your competition registration has been denied because of lack of money";

            defaultNotificationService.competitionRegistrationDenied(user, reason);

            defaultFirebaseService.composeCompetitionRegistrationDeniedNotification(user, reason).forEach(defaultFirebaseService::sendNotification);

            return new ResponseEntity<>(new ResponseModel(false, "You do not have enough money"),HttpStatus.OK); // return response to client.

        }else if(user.getMoney() <= (gameServerPlayerModel.getEntryFee() + 100)){

            String reason = "Your competition registration has been denied because of lack of money";

            defaultNotificationService.competitionRegistrationDenied(user, reason);

            defaultFirebaseService.composeCompetitionRegistrationDeniedNotification(user, reason).forEach(defaultFirebaseService::sendNotification);

            return new ResponseEntity<>(new ResponseModel(false, "You cannot withdraw all you have"),HttpStatus.OK); // return response to client.

        }else{


            user.setMoney(user.getMoney() - gameServerPlayerModel.getEntryFee());

            User updatedUser = defaultUserDAOService.updateUser(user);

            Competition newCompetition = new Competition();

            newCompetition.setCompetitionName(gameServerPlayerModel.getCompetition().getCompetitionName());

            newCompetition.setGame(defaultGameService.findGameByName(gameServerPlayerModel.getCompetition().getGame().getName()));

            newCompetition.setEntryFee(gameServerPlayerModel.getEntryFee());

            newCompetition.getPunters().add(new Punter().setPlayer(updatedUser)
                    .setEliminated(false).setRisk(gameServerPlayerModel.getEntryFee()));

            Competition saveCompetition = defaultCompetitionService.update(newCompetition);

            defaultNotificationService.competitionEntered(user, saveCompetition);

            defaultFirebaseService.composeCompetitionEnteredNotification(user, saveCompetition).forEach(defaultFirebaseService::sendNotification);



            Transactions transactions = new Transactions();

            transactions.setPlayer(updatedUser);

            transactions.setAmount(gameServerPlayerModel.getEntryFee());

            transactions.setTransactiontype(Transactions.TRANSACTIONTYPE.DEBIT);

            transactions.setDetail(String.format("Entry fee paid for the s% competition", "comerson"));

            transactions.setDateCreated();

            defaultTransactionService.update(transactions);

            defaultNotificationService.Transaction(user, transactions.getAmount(), transactions);

            defaultFirebaseService.composeTransactionNotification(user, transactions.getAmount(), transactions).forEach(defaultFirebaseService::sendNotification);


            return new ResponseEntity<>(new ResponseModel(true, "Success"),HttpStatus.OK); // return response to client.

        }
    }



    @ResponseBody
    @RequestMapping(value = "/competition/join", method = RequestMethod.POST)
    public ResponseEntity<ResponseModel> DebitCompetitionMoneyFromPlayer(@RequestBody GameServerPlayerModel gameServerPlayerModel){

        User user = defaultUserDAOService.getUser(gameServerPlayerModel.getUser().getId());

        if(user.getMoney() < gameServerPlayerModel.getEntryFee()){

            String reason = "Your competition registration has been denied because of lack of money";

            defaultNotificationService.competitionRegistrationDenied(user, reason);

            defaultFirebaseService.composeCompetitionRegistrationDeniedNotification(user, reason).forEach(defaultFirebaseService::sendNotification);

            return new ResponseEntity<>(new ResponseModel(false, "You do not have enough money"),HttpStatus.OK); // return response to client.

        }else if(user.getMoney() <= (gameServerPlayerModel.getEntryFee() + 100)){

            String reason = "Your competition registration has been denied because of lack of money";

            defaultNotificationService.competitionRegistrationDenied(user, reason);

            defaultFirebaseService.composeCompetitionRegistrationDeniedNotification(user, reason).forEach(defaultFirebaseService::sendNotification);

            return new ResponseEntity<>(new ResponseModel(false, "You do not have enough money"),HttpStatus.OK); // return response to client.


        }else{


            user.setMoney(user.getMoney() - gameServerPlayerModel.getEntryFee());

            User updatedUser = defaultUserDAOService.updateUser(user);

            Competition foundCompetition = defaultCompetitionService.getCompetitionById(gameServerPlayerModel.getCompetition().getId());

            foundCompetition.getPunters().add(new Punter().setPlayer(updatedUser)
                    .setEliminated(false).setRisk(gameServerPlayerModel.getEntryFee()));

            Competition updatedCompetition = defaultCompetitionService.update(foundCompetition);

            defaultNotificationService.competitionEntered(user, updatedCompetition);

            defaultFirebaseService.composeCompetitionEnteredNotification(user, updatedCompetition).forEach(defaultFirebaseService::sendNotification);



            Transactions transactions = new Transactions();

            transactions.setPlayer(updatedUser);

            transactions.setAmount(gameServerPlayerModel.getEntryFee());

            transactions.setTransactiontype(Transactions.TRANSACTIONTYPE.DEBIT);

            transactions.setDetail(String.format("Entry fee paid for the s% competition", "comerson"));

            transactions.setDateCreated();

            defaultTransactionService.update(transactions);

            defaultNotificationService.Transaction(user, transactions.getAmount(), transactions);

            defaultFirebaseService.composeTransactionNotification(user, transactions.getAmount(), transactions).forEach(defaultFirebaseService::sendNotification);


            return new ResponseEntity<>(new ResponseModel(true, "Success"),HttpStatus.OK); // return response to client.

        }
    }



    @ResponseBody
    @RequestMapping(value = "/new-competition", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> NewCompetition(@RequestBody NewCompetitionModel newCompetitionModel){

        try {

            Competition competition = new Competition();

            competition.setCompetitionName(newCompetitionModel.getCompetitionName())
                    .setGame(defaultGameService.findGameByName(newCompetitionModel.getGameName()))
                    .setEntryFee(newCompetitionModel.getEntryFee())
                    .setCompetitionStatus(Enum.CompetitionStatus.OPEN);

            Competition savedCompetition = defaultCompetitionService.save(competition);

            return new ResponseEntity<>(new ResponseModel(true, "Successful"), HttpStatus.OK); // return response to client.

        }catch (Exception ex){


            return new ResponseEntity<>(new ResponseModel(false, "Oops! an error occurred"), HttpStatus.OK); // return response to client.

        }



    }



    @ResponseBody
    @RequestMapping(value = "/competition/started", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> InitCompetitionHasStarted(@RequestBody NewCompetitionModel competitionModel){

        try {

            Competition competition = defaultCompetitionService.getCompetitionByName(competitionModel.getCompetitionName());

            competition.setCompetitionStatus(Enum.CompetitionStatus.STARTED);

            Competition updatedCompetition = defaultCompetitionService.update(competition);

            for (Punter punter: competition.getPunters()) {

                defaultNotificationService.competitionStarted(punter.getPlayer(), updatedCompetition);

                /***********************************************************************************/
                /****************************Create Firebase Broadcast Message**********************/
                /***********************************************************************************/

                List<FirebaseNotificationModel> firebaseNotificationModelList = defaultFirebaseService.composeCompetitionStartedNotification(punter.getPlayer(), competition);

                for (FirebaseNotificationModel firebaseNotificationModel: firebaseNotificationModelList) {

                    defaultFirebaseService.sendNotification(firebaseNotificationModel);

                }

            }

            return new ResponseEntity<>(new ResponseModel(true, "Successful"), HttpStatus.OK); // return response to client.

        }catch (Exception ex){


            return new ResponseEntity<>(new ResponseModel(false, "Oops! an error occurred"), HttpStatus.INTERNAL_SERVER_ERROR); // return response to client.

        }



    }


    @ResponseBody
    @RequestMapping(value = "/competition/could-be-started", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> CompetitionCouldNotBeStarted(@RequestBody GameServerPlayerModel gameServerPlayerModel){

        try {

           gameServerPlayerModel.getCompetition().getPunters().forEach(punter -> {

               User player = defaultUserDAOService.getUser(punter.getPlayer().getId());

               Competition foundCompetition = defaultCompetitionService.getCompetitionByName(gameServerPlayerModel.getCompetition().getCompetitionName());

               player.setMoney(player.getMoney() + foundCompetition.getEntryFee());

               User updatedPlayer = defaultUserDAOService.updateUser(player);

               Transactions transactions = new Transactions();

               transactions.setPlayer(updatedPlayer)
                       .setTransactiontype(Transactions.TRANSACTIONTYPE.REVERSAL)
                       .setAmount(foundCompetition.getEntryFee())
                       .setDetail(String.format("A sum of s% Naira has been reversed back into your account as the s% competition you registered for did not have enough contestants to start", foundCompetition.getEntryFee(), foundCompetition.getCompetitionName()))
                       .setDateCreated();

               defaultNotificationService.Transaction(player, foundCompetition.getEntryFee(), transactions);

               defaultFirebaseService.composeTransactionNotification(player, foundCompetition.getEntryFee(), transactions);

               defaultNotificationService.competitionCouldNotStart(player, foundCompetition);

               defaultFirebaseService.composeCompetitionCouldNotStartNotification(player, foundCompetition).forEach(defaultFirebaseService::sendNotification);


           });
            return new ResponseEntity<>(new ResponseModel(true, "Successful"), HttpStatus.OK); // return response to client.

        }catch (Exception ex){


            return new ResponseEntity<>(new ResponseModel(false, "Oops! an error occurred"), HttpStatus.INTERNAL_SERVER_ERROR); // return response to client.

        }



    }




    @ResponseBody
    @RequestMapping(value = "/stage-ended", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> StageEnded(@RequestBody NewCompetitionModel newCompetitionModel){

        List<User> friends = new ArrayList<>();

        try {

            Competition competition = defaultCompetitionService.getCompetitionByName(newCompetitionModel.getCompetitionName());


            for (PairModel pair: newCompetitionModel.getStage().getPairs()) {

                Punter winningPlayer = competition.getPunters().stream().
                        filter(punter -> punter.getPlayer().getUserName().equalsIgnoreCase(pair.getWinner())).
                        findFirst().orElse(null);

                Punter loosingPlayer = competition.getPunters().stream().
                        filter(punter -> punter.getPlayer().getUserName().equalsIgnoreCase(
                                pair.getWinner().equalsIgnoreCase(pair.getPlayerOne()) ? pair.getPlayerTwo() : pair.getPlayerTwo()
                        )).
                        findFirst().orElse(null);

                int indexOfLoosingPlayer = competition.getPunters().indexOf(loosingPlayer);

                loosingPlayer.setEliminated(true);

                defaultNotificationService.playerLoosesMatch(loosingPlayer.getPlayer(),winningPlayer.getPlayer(),competition);

                List<FirebaseNotificationModel> losersFirebaseNotificationModelList = defaultFirebaseService.composeMatchLostNotification(loosingPlayer.getPlayer(),winningPlayer.getPlayer(),competition);

                losersFirebaseNotificationModelList.forEach(firebaseNotificationModel-> defaultFirebaseService.sendNotification(firebaseNotificationModel));

                if(newCompetitionModel.getStage().getPairs().size() > 1) {

                    defaultNotificationService.playerWinsMatch(winningPlayer.getPlayer(), loosingPlayer.getPlayer(), competition);

                    List<FirebaseNotificationModel> winnerFirebaseNotificationModelList = defaultFirebaseService.composeMatchWonNotification(winningPlayer.getPlayer(),loosingPlayer.getPlayer(),competition);

                    winnerFirebaseNotificationModelList.forEach(firebaseNotificationModel-> defaultFirebaseService.sendNotification(firebaseNotificationModel));

                }


                competition.getPunters().set(indexOfLoosingPlayer, loosingPlayer);

                friends = defaultUserDAOService.getFriends(loosingPlayer.getPlayer());

                for (User friend: friends) {

                    defaultNotificationService.friendKnockedOutOfCompetition(friend,loosingPlayer.getPlayer(), competition, newCompetitionModel);

                    defaultFirebaseService.composeFriendKnockedOutOfCompetitionNotification(friend,loosingPlayer.getPlayer(),competition).forEach(firebaseNotificationModel-> defaultFirebaseService.sendNotification(firebaseNotificationModel));
                }


                if(newCompetitionModel.getStage().getPairs().size() == 1){

                    winningPlayer.getPlayer().setMoney(winningPlayer.getPlayer().getMoney() + (competition.getPunters().size() * competition.getEntryFee()))
                            .getPlayerStats().setWallet(winningPlayer.getPlayer().getPlayerStats().getWallet() + (competition.getPunters().size() * competition.getEntryFee()));

                    winningPlayer.setWinner(true);

                    winningPlayer.setWinAmount(competition.getPunters().size() * competition.getEntryFee());

                    competition.setWinners().add(winningPlayer);

                    competition.setWinningAmount(competition.getPunters().size() * competition.getEntryFee());

                    competition.setCompetitionStatus(Enum.CompetitionStatus.ENDED);

                    defaultUserDAOService.updateUser(winningPlayer.getPlayer());

                    friends = defaultUserDAOService.getFriends(loosingPlayer.getPlayer());

                    for (User friend: friends) {

                        defaultNotificationService.friendWonCompetition(friend, winningPlayer.getPlayer(), competition, newCompetitionModel);

                        defaultFirebaseService.composeFriendWonCompetitionNotification(friend, winningPlayer.getPlayer(),competition).forEach(firebaseNotificationModel -> defaultFirebaseService.sendNotification(firebaseNotificationModel));
                    }

                    defaultNotificationService.PlayerWinsCompetition(winningPlayer.getPlayer(), competition);

                }

                defaultCompetitionService.update(competition);

            }

            return new ResponseEntity<>(new ResponseModel(true, "Successful"), HttpStatus.OK); // return response to client.

        }catch (Exception ex){

            return new ResponseEntity<>(new ResponseModel(false, "Oops! an error occurred"), HttpStatus.INTERNAL_SERVER_ERROR); // return response to client.

        }
    }

    @ResponseBody
    @RequestMapping(value = "/stage-started", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> StageStarted(@RequestBody StageModel stage){

        try {

            for (PairModel pair : stage.getPairs()) {

                User playerOne = defaultUserDAOService.getByUsername(pair.getPlayerOne());

                User playerTwo = defaultUserDAOService.getByUsername(pair.getPlayerOne());

                Gamification gamification = new Gamification()
                        .setContestantOne(playerOne)
                        .setContestantTwo(playerTwo)
                        .setRead(false)
                        .setCompetition(new Competition())
                        .setGamificationMessage("");
                gamification.setDateCreated();

                defaultGamificationService.save(gamification);

                /***********************************************************************************/
                /****************************Create Firebase Broadcast Message**********************/
                /***********************************************************************************/

                defaultFirebaseService.composeStageStartedNotification(playerOne, playerTwo, gamification).forEach(defaultFirebaseService::sendNotification);

                defaultFirebaseService.composeStageStartedNotification(playerTwo, playerOne, gamification).forEach(defaultFirebaseService::sendNotification);


            }

            return new ResponseEntity<>(new ResponseModel(true, "Successful"), HttpStatus.OK); // return response to client.

        }catch (Exception ex){

            return new ResponseEntity<>(new ResponseModel(false, "Oops! server error"), HttpStatus.INTERNAL_SERVER_ERROR); // return response to client.
        }

    }

    @ResponseBody
    @RequestMapping(value = "/friend-request/send", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> FriendRequest(@RequestBody FriendRequest friendRequest){

        try {

            friendRequest.setDateCreated();

            friendRequest.setFriendRequestStatus(Enum.FriendRequestStatus.PENDING);

            FriendRequest updatedFriendRequest = defaultFriendRequestService.save(friendRequest);

            defaultNotificationService.friendRequestGotten(friendRequest.getRecipient(),friendRequest.getSender());

            defaultFirebaseService.composeFriendRequestGottenNotification(updatedFriendRequest.getRecipient(), updatedFriendRequest.getSender(), updatedFriendRequest).forEach(defaultFirebaseService::sendNotification);

            messagingTemplate.convertAndSendToUser(updatedFriendRequest.getRecipient().getUserName(),"/topic/friend-request/receive/request", updatedFriendRequest);

            if(!Objects.isNull(server)){

                SocketIOClient socketIOClient = server.getAllClients().stream().filter(

                        client -> client.get("name").equals(updatedFriendRequest.getRecipient().getUserName())

                ).findFirst().get();

                socketIOClient.sendEvent("friend request", updatedFriendRequest);
            }

            return new ResponseEntity<>(new ResponseModel(true, "Successful"), HttpStatus.OK); // return response to client.

        }catch (Exception ex){

            return new ResponseEntity<>(new ResponseModel(false, "Oops! server error"), HttpStatus.INTERNAL_SERVER_ERROR); // return response to client.
        }

    }
}
