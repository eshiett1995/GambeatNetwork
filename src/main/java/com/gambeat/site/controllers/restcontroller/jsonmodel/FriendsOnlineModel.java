package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Oto-obong on 02/10/2017.
 */

@Component
public class FriendsOnlineModel {

    List<User> friendsOnline;

    Boolean isSuccessful;

    public List<User> getFriendsOnline() {
        return friendsOnline;
    }

    public void setFriendsOnline(List<User> friendsOnline) {
        this.friendsOnline = friendsOnline;
    }

    public Boolean getSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(Boolean successful) {
        isSuccessful = successful;
    }
}
