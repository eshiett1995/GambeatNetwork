package com.gambeat.site.controllers.restcontroller;

import com.gambeat.site.controllers.restcontroller.jsonmodel.PlayerStatsModel;
import com.gambeat.site.entities.PlayerStats;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Oto-obong on 30/09/2017.
 */

@RestController
@RequestMapping(value = "/home")
public class HomeController {

    @Autowired
    private DefaultCookieService defaultCookieService;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);


    @Autowired
    private DefaultUserDAOService defaultUserDAOService;

    @Autowired
    PlayerStatsModel playerStatsModel;



    @ResponseBody
    @RequestMapping(value = "/stats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlayerStatsModel> FetchStats(HttpServletRequest request, HttpServletResponse response){

        try {

                 User user = this.defaultCookieService.getUser("gambeat", request);

                 playerStatsModel.setUser(user);

                 playerStatsModel.setPlayerStats(user.getPlayerStats().setMatchPlayed(120).setWins(60));

                 playerStatsModel.setSuccessful(true);

                 return new ResponseEntity<>(playerStatsModel, HttpStatus.OK); // return response to client.


        }catch (Exception ex){

            LOGGER.error(ex.getMessage());

            playerStatsModel.setPlayerStats(new PlayerStats());

            playerStatsModel.setSuccessful(false);

            return new ResponseEntity<>(playerStatsModel, HttpStatus.OK); // return response to client.


        }

    }

}
