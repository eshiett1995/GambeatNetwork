package com.gambeat.site.controllers.restcontroller;


import com.gambeat.site.controllers.restcontroller.jsonmodel.UserModel;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultMessageService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class Test {

    @Autowired
    DefaultMessageService defaultMessageService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;


    @ResponseBody
    @RequestMapping("/tester")
    public Page<User> SwitchToIndexPage(HttpServletResponse response) throws IOException {

        User currentUser = defaultUserDAOService.getUser(String.valueOf(1));

        Page<Message> foundMessagePage = defaultMessageService.findMostRecentMessageFromConversation(currentUser,new PageRequest(0,20));

        List<User> users = new ArrayList<>();

        for (Message message:foundMessagePage.getContent()) {


            if(currentUser == message.getSender()){

                users.add(message.getReceiver());

            }else{

                users.add(message.getSender());
            }
        }

        Page<User> page = new PageImpl<User>(users);

        return page;
    }

}
