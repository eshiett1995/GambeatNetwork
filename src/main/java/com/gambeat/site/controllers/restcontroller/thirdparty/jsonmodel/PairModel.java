package com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel;

public class PairModel {

    String playerOne;

    String playerTwo;

    String winner;

    public String getPlayerOne() {

        return playerOne;

    }

    public PairModel setPlayerOne(String playerOne) {

        this.playerOne = playerOne;

        return this;

    }

    public String getPlayerTwo() {

        return playerTwo;

    }

    public PairModel setPlayerTwo(String playerTwo) {

        this.playerTwo = playerTwo;

        return this;

    }

    public String getWinner() {

        return winner;

    }

    public PairModel setWinner(String winner) {

        this.winner = winner;

        return this;
    }
}
