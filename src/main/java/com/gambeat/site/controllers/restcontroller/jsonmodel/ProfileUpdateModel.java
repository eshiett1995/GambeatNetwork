package com.gambeat.site.controllers.restcontroller.jsonmodel;

import org.bson.types.ObjectId;

import java.math.BigInteger;

public class ProfileUpdateModel {

    public ProfileUpdateModel(String temporaryStoreId, ResponseModel responseModel) {

        this.temporaryStoreId = temporaryStoreId;

        this.responseModel = responseModel;

    }

    private String temporaryStoreId;

    private ResponseModel responseModel;

    public String getTemporaryStoreId() {

        return temporaryStoreId;

    }

    public ProfileUpdateModel setTemporaryStoreId(String temporaryStoreId) {

        this.temporaryStoreId = temporaryStoreId;

        return  this;
    }

    public ResponseModel getResponseModel() {

        return responseModel;
    }

    public ProfileUpdateModel setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

        return this;
    }
}
