package com.gambeat.site.controllers.restcontroller;


import com.gambeat.site.controllers.restcontroller.jsonmodel.FriendAndMessagesModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.MessageModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.PaginationModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultFriendRequestService;
import com.gambeat.site.services.implementation.DefaultMessageService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/message")
public class MessageController {

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);


    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Autowired
    DefaultCookieService defaultCookieService;

    @Autowired
    DefaultMessageService defaultMessageService;

    @Autowired
    DefaultFriendRequestService defaultFriendRequestService;


    @ResponseBody
    @RequestMapping(value = "/all/{page}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageModel> GetPaginatedMessage(@PathVariable int page, Pageable pageable, HttpServletRequest request) {

        User user = this.defaultCookieService.getUser("gambeat", request);

        Page<Message> foundMessagePage = defaultMessageService.findMostRecentMessageFromConversation(user,new PageRequest(0,20));

        List<FriendAndMessagesModel> friendAndMessagesModelList = new ArrayList<>();

        for (Message message:foundMessagePage.getContent()) {

            FriendAndMessagesModel friendAndMessagesModel = new FriendAndMessagesModel();

            if(user == message.getSender()){

                friendAndMessagesModel.setFriend(message.getReceiver()
                        .setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(message.getReceiver(), user, false)));

            }else{

                friendAndMessagesModel.setFriend(message.getSender()
                        .setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(message.getSender(),user, false)));
            }

            friendAndMessagesModel.getMessages().add(message);

            friendAndMessagesModelList.add(friendAndMessagesModel);
        }

        Page<FriendAndMessagesModel> friendAndMessageModel = new PageImpl<FriendAndMessagesModel>(friendAndMessagesModelList);


        MessageModel messageModel = new MessageModel();

        messageModel.setTotalNumberOfUnreadMessages(defaultMessageService.countTotalUnreadMessages(user));

        messageModel.setFriendsAndMessagesModelPage(friendAndMessageModel);

        messageModel.setResponseModel(new ResponseModel(true, "Successful"));


        return new ResponseEntity<>(messageModel, HttpStatus.OK);

    }


    @ResponseBody
    @RequestMapping(value = "/{userID}/{page}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginationModel> GetPaginatedMessage(@PathVariable Long userID, @PathVariable int page, Pageable pageable, HttpServletRequest request) {

        PaginationModel paginationModel = new PaginationModel();

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            // Calls the userservice to find user by username (senders user)
            User friend = this.defaultUserDAOService.getUser(String.valueOf(userID));

            if(page == -1) {

                Page<Message> firstPage = defaultMessageService.getMessage(user, friend, new PageRequest(0, 20));

                paginationModel.setMessagesPage(defaultMessageService.getMessage(user, friend, new PageRequest(firstPage.getTotalPages() -1, 20)))
                        .setResponseMessage("Successful")
                        .setSuccessful(true);

            }else {

                paginationModel.setMessagesPage(defaultMessageService.getMessage(user, friend, new PageRequest(page, 20)))
                        .setResponseMessage("Successful")
                        .setSuccessful(true);

            }

            return new ResponseEntity<>(paginationModel, HttpStatus.OK);

        }catch (Exception ex){

            paginationModel.setResponseMessage("False")
                    .setSuccessful(false);

            return new ResponseEntity<>(paginationModel, HttpStatus.OK);

        }

    }




    @RequestMapping(value = "/friends/{pageNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginationModel> GetFriendsPage(@PathVariable int pageNumber, HttpServletRequest request, HttpServletResponse response) {

        PaginationModel paginationModel = new PaginationModel();

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            Page<FriendRequest> pages = defaultFriendRequestService.getAcceptedFriendRequestPages(user, Enum.FriendRequestStatus.ACCEPTED, new PageRequest(pageNumber, 20, Sort.Direction.ASC, "id"));

            paginationModel.setNumberOfPages(pages.getTotalPages())
                    .setFriendsPage(pages)
                    .setFriends(defaultFriendRequestService.getFriends(user, pages.getContent()))
                    .setNumberOfUsers( defaultFriendRequestService.countAllFriends(user))
                    .setResponseMessage("Successful")
                    .setSuccessful(true);

            for (User friend: paginationModel.getFriends()) {

                friend.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(friend, user, false));

            }

            return new ResponseEntity<>(paginationModel, HttpStatus.OK);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            paginationModel.setNumberOfPages(0)
                    //pages.getContent() is a method in pages that gets the content of the Pages request
                    .setFriends(Collections.EMPTY_LIST)
                    .setNumberOfUsers(0)
                    .setResponseMessage("Failed")
                    .setSuccessful(false);

            return new ResponseEntity<>(paginationModel, HttpStatus.OK);
        }
    }

}
