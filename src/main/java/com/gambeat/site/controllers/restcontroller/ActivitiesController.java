package com.gambeat.site.controllers.restcontroller;


import com.gambeat.site.controllers.restcontroller.jsonmodel.CompetitionResponseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.EnterCompetitionModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.Competition;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultCompetitionService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/activities")
public class ActivitiesController {

    @Autowired
    DefaultUserDAOService defaultUserDAOService;


    @Autowired
    DefaultCompetitionService defaultCompetitionService;


    @Autowired
    DefaultCookieService defaultCookieService;



    @ResponseBody
    @RequestMapping(value = "/match-stats", method = RequestMethod.GET)
    public void GetPaginatedMessage(HttpServletRequest request, HttpServletResponse res) throws IOException {

        User user = this.defaultCookieService.getUser("gambeat", request);
        Enumeration<String> parameterNames = request.getParameterNames();
        PrintWriter out = res.getWriter();


        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            System.out.println(paramName);

            System.out.println("n");

            String[] paramValues = request.getParameterValues(paramName);

            for (int i = 0; i < paramValues.length; i++) {

                String paramValue = paramValues[i];

                System.out.println("t" + paramValue);

                System.out.println("n");

            }


        }




        /** if(page == -1) {

             Page<Message> firstPage = defaultMessageService.getLastMessageToUsers(user,new PageRequest(0, 20));

             Page<Message> lastPage = defaultMessageService.getLastMessageToUsers(user, new PageRequest(firstPage.getTotalPages() - 1, 20));


             if(lastPage.getContent().size() != 20 && lastPage.getNumber() > 0){

                 Page<Message> newPage = defaultMessageService.getLastMessageToUsers(user, new PageRequest(firstPage.getTotalPages() - 2, 20));

                 for (Message message: lastPage.getContent()) {

                     newPage.getContent().add(message);

                 }

                 paginationModel.setMessagesPage(newPage).setResponseMessage("Successful").setSuccessful(true);


             }else{

                 paginationModel.setMessagesPage(lastPage).setResponseMessage("Successful").setSuccessful(true);

             }



         }else {

             paginationModel.setMessagesPage(defaultMessageService.getLastMessageToUsers(user,new PageRequest(page, 20))).setResponseMessage("Successful").setSuccessful(true);

         }

         return new ResponseEntity<>(paginationModel, HttpStatus.OK);


 **/
    }


    @ResponseBody
    @RequestMapping(value = "/create/competition", method = RequestMethod.POST)
    public ResponseEntity<CompetitionResponseModel> CreateCompetition(@RequestBody EnterCompetitionModel enterCompetitionModel, HttpServletRequest request, HttpServletResponse res) throws IOException {

        Competition foundCompetition = defaultCompetitionService.checkIfCompetitionExist(enterCompetitionModel.getCompetition().getGame(), enterCompetitionModel.getCompetition().getEntryFee());

        if(Objects.isNull(foundCompetition)){

            RestTemplate restTemplate = new RestTemplate();

            HttpEntity<EnterCompetitionModel> body = new HttpEntity<>(enterCompetitionModel);

            ResponseEntity<ResponseModel> responseEntity = restTemplate.exchange(enterCompetitionModel.getCompetition().getGame().getCreateCompetitionUrl(), HttpMethod.POST, body, ResponseModel.class);

            if(responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.ACCEPTED){

                return new ResponseEntity<>(new CompetitionResponseModel(responseEntity.getBody()), HttpStatus.OK);

            }else{

                return new ResponseEntity<>(new CompetitionResponseModel(new ResponseModel(false, "Oops! an error occurred on the server, so we couldn't create a competition for you.")), responseEntity.getStatusCode());

            }

        }else{

            return new ResponseEntity<>(new CompetitionResponseModel(foundCompetition,new ResponseModel(true,String.format("A competition titled s% is already up, would you love to join it ?"))), HttpStatus.OK);
        }
    }


    @ResponseBody
    @RequestMapping(value = "/join/competition", method = RequestMethod.POST)
    public ResponseEntity<CompetitionResponseModel> JoinCompetition(@RequestBody EnterCompetitionModel enterCompetitionModel, HttpServletRequest request, HttpServletResponse res) throws IOException {

        Competition foundCompetition = defaultCompetitionService.checkIfCompetitionExist(enterCompetitionModel.getCompetition().getGame(), enterCompetitionModel.getCompetition().getEntryFee());

        if(Objects.isNull(foundCompetition)){

            return new ResponseEntity<>(new CompetitionResponseModel(new ResponseModel(true,String.format("No competition titled s% was found", enterCompetitionModel.getCompetition().getCompetitionName()))), HttpStatus.OK);


        }else if(enterCompetitionModel.getCompetition().getPunters().size() == 100 || enterCompetitionModel.getCompetition().getCompetitionStatus() != Enum.CompetitionStatus.OPEN){

            return new ResponseEntity<>(new CompetitionResponseModel(enterCompetitionModel.getCompetition(), new ResponseModel(true,String.format("The s% competition is closed to new contestants, please try entering another or starting one", enterCompetitionModel.getCompetition().getCompetitionName()))), HttpStatus.OK);

        }else{

            RestTemplate restTemplate = new RestTemplate();

            HttpEntity<EnterCompetitionModel> body = new HttpEntity<>(enterCompetitionModel);

            ResponseEntity<ResponseModel> responseEntity = restTemplate.exchange(enterCompetitionModel.getCompetition().getGame().getJoinCompetitionUrl(), HttpMethod.POST, body, ResponseModel.class);

            if(responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.ACCEPTED){

                return new ResponseEntity<>(new CompetitionResponseModel(responseEntity.getBody()), HttpStatus.OK);

            }else{

                return new ResponseEntity<>(new CompetitionResponseModel(new ResponseModel(false, "Oops! an error occurred on the server, so we couldn't create a competition for you.")), responseEntity.getStatusCode());

            }

        }
    }


 }
