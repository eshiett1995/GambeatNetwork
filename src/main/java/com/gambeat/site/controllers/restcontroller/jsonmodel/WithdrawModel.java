package com.gambeat.site.controllers.restcontroller.jsonmodel;

public class WithdrawModel {

    private double availableBalance;

    private double withdrawalAmount;

    private boolean viaEmail;

    private String accountNumber;

    private String bankCode;

    public double getAvailableBalance() {

        return availableBalance;

    }

    public WithdrawModel setAvailableBalance(double availableBalance) {

        this.availableBalance = availableBalance;

        return this;
    }

    public double getWithdrawalAmount() {

        return withdrawalAmount;

    }

    public WithdrawModel setWithdrawalAmount(double withdrawalAmount) {

        this.withdrawalAmount = withdrawalAmount;

        return this;

    }

    public boolean isViaEmail() {

        return viaEmail;

    }

    public WithdrawModel setViaEmail(boolean viaEmail) {

        this.viaEmail = viaEmail;

        return this;

    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public WithdrawModel setAccountNumber(String accountNumber) {

        this.accountNumber = accountNumber;

        return this;
    }

    public String getBankCode() {

        return bankCode;

    }

    public WithdrawModel setBankCode(String bankCode) {

        this.bankCode = bankCode;

        return this;
    }
}
