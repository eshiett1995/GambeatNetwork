package com.gambeat.site.controllers.restcontroller.jsonmodel;

public class FirebaseModel {

    public FirebaseModel(){};

    public FirebaseModel(String token){

        this.token = token;
    };

    public FirebaseModel(String token, ResponseModel responseModel){

        this.token = token;

        this.responseModel = responseModel;
    };

    private String token;

    private ResponseModel responseModel;

    public String getToken() {
        return token;
    }

    public FirebaseModel setToken(String token) {
        this.token = token;
        return this;
    }

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public FirebaseModel setResponseModel(ResponseModel responseModel) {
        this.responseModel = responseModel;
        return this;
    }
}
