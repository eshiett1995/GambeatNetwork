package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.Competition;
import com.gambeat.site.entities.User;

public class EnterCompetitionModel {

    private Competition competition;

    private User user;

    private boolean continueIfCompetitionEntryClosed;

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isContinueIfCompetitionEntryClosed() {
        return continueIfCompetitionEntryClosed;
    }

    public void setContinueIfCompetitionEntryClosed(boolean continueIfCompetitionEntryClosed) {
        this.continueIfCompetitionEntryClosed = continueIfCompetitionEntryClosed;
    }
}
