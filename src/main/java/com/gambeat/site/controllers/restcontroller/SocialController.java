package com.gambeat.site.controllers.restcontroller;

import com.gambeat.site.controllers.restcontroller.jsonmodel.PaginationModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ProfileUpdateModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.UserModel;
import com.gambeat.site.entities.*;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.services.implementation.*;
import com.gambeat.site.utility.email.EmailHtmlSender;
import com.gambeat.site.utility.email.EmailStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Objects;

/**
 * Created by Oto-obong on 02/09/2017.
 */

@RestController
@RequestMapping(value = "/social")
public class SocialController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    DefaultMailService defaultMailService;

    @Autowired
    private DefaultSmsService defaultSmsService;

    @Autowired
    private DefaultCookieService defaultCookieService;

    @Autowired
    private DefaultEmailMailNotificationService defaultEmailNotificationService;

    @Autowired
    private DefaultTemporaryStoreService defaultTemporaryStoreService;

    @Autowired
    EmailHtmlSender emailHtmlSender;

    @Autowired
    private DefaultFriendRequestService defaultFriendRequestService;


    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(SocialController.class);

    @Autowired
    DefaultTokenService defaultTokenService;

    @Autowired
    PaginationModel paginationModel;

    @Autowired
    ResponseModel responseModel;

    @Autowired
    UserModel userModel;


    @Autowired
    private DefaultUserDAOService defaultUserDAOService;



    @ResponseBody
    @RequestMapping(value = "/profile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> AccountOwnersProfile(HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);


            userModel.setUser(user).setResponseMessage("Successful").setSuccessful(true);

            return new ResponseEntity<>(userModel, HttpStatus.OK); // return response to client.


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            userModel.setUser(null).setResponseMessage("Opps! connection lost, please try again").setSuccessful(false);

            return new ResponseEntity<>(userModel, HttpStatus.UNAUTHORIZED);
        }

    }


    @RequestMapping(value = "/profile/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> UpdateUserProfile(@RequestBody User usersNewDetails, HttpServletRequest request, HttpServletResponse response) {

        try {

            // Calls the userservice to find user by username
            User user = this.defaultCookieService.getUser("gambeat", request);

            user.setFirstName(usersNewDetails.getFirstName())
                    .setLastName(usersNewDetails.getLastName())
                    .setGender(usersNewDetails.getGender())
                    .setCountry(usersNewDetails.getCountry())
                    .setBio(usersNewDetails.getBio());

            defaultUserDAOService.updateUser(user);

            responseModel.setIsSuccessful(true).setResponseMessage("Successfully updated");

            return new ResponseEntity<>(responseModel, HttpStatus.OK);


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false).setResponseMessage("Oops! connection lost, please try again");

            return new ResponseEntity<>(responseModel, HttpStatus.UNAUTHORIZED);
        }

    }


    @RequestMapping(value = "/profile/update/username", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileUpdateModel> UpdateUsername(@RequestBody TemporaryStore temporaryStore, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            if(user.getUserName().equalsIgnoreCase(temporaryStore.getValue())){

                return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "set username is the user's current user name")), HttpStatus.OK);

            }


            String token = defaultTokenService.generateToken(6);

            if (temporaryStore.getSendVia().equals(Enum.MessageSender.EMAIL)) {

                EmailStatus emailStatus = emailHtmlSender.send(user.getEmail(), "User name update token", "email/update-token", defaultMailService.ComposeUserProfileUpdateMail(user, token, Enum.RequestedUpdate.USERNAME));

            } else {

                defaultSmsService.ComposeChangePasswordTokenSms(user, token);

            }

            temporaryStore.setToken(token)
                    .setUserName(user.getUserName())
                    .setValue(temporaryStore.getValue())
                    .setRequestedUpdate(Enum.RequestedUpdate.USERNAME)
                    .setAttempts(0);


            TemporaryStore savedTemporaryStore = defaultTemporaryStoreService.saveTemporaryStore(temporaryStore);

            responseModel.setIsSuccessful(true).setResponseMessage("Successful, Token has been generated successfully");

            return new ResponseEntity<>(new ProfileUpdateModel(savedTemporaryStore.getId(), new ResponseModel(true, "Successful, Token has been generated successfully")), HttpStatus.OK);


        } catch (Exception ex) {

            System.out.println(ex.getMessage());

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false).setResponseMessage("Oops! connection lost, please try again");

            return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), responseModel), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/profile/update/password", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileUpdateModel> UpdatePassword(@RequestBody TemporaryStore temporaryStore, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            //temporaryStore.setPresentValue(bCryptPasswordEncoder.encode(temporaryStore.getPresentValue()));

            if(!bCryptPasswordEncoder.matches(temporaryStore.getPresentValue(), user.getPassword())){

                return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "Password mismatch, current password does'nt match what the users own")), HttpStatus.OK);

            }else if(bCryptPasswordEncoder.matches(temporaryStore.getValue(), user.getPassword())){

                return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "Current and new Password are the same")), HttpStatus.OK);

            }


            String token = defaultTokenService.generateToken(6);

            if (temporaryStore.getSendVia().equals(Enum.MessageSender.EMAIL)) {

                EmailStatus emailStatus = emailHtmlSender.send(user.getEmail(), "Password update token", "email/update-token", defaultMailService.ComposeUserProfileUpdateMail(user, token, Enum.RequestedUpdate.PASSWORD));

            } else {

                defaultSmsService.ComposeChangePasswordTokenSms(user, token);

            }

            temporaryStore.setUserName(user.getUserName())
                    .setValue(bCryptPasswordEncoder.encode(temporaryStore.getValue()))
                    .setToken(token)
                    .setRequestedUpdate(Enum.RequestedUpdate.PASSWORD)
                    .setAttempts(0);

            TemporaryStore savedTemporaryStore = defaultTemporaryStoreService.saveTemporaryStore(temporaryStore);

            responseModel.setIsSuccessful(true).setResponseMessage("Successful, Token has been generated successfully");

            return new ResponseEntity<>(new ProfileUpdateModel(savedTemporaryStore.getId(), responseModel), HttpStatus.OK);



        } catch (Exception ex) {

            System.out.println(ex.getMessage());

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false).setResponseMessage("Oops! connection lost, please try again");

            return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "Oops! an error has occurred")), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/profile/update/email", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileUpdateModel> UpdateEmail(@RequestBody TemporaryStore temporaryStore, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            //check if the new mail is the same as the old mail
            if(user.getEmail().equalsIgnoreCase(temporaryStore.getValue())){

                return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "You are already using this mail")), HttpStatus.OK);
            }

            String token = defaultTokenService.generateToken(6);

            TemporaryStore savedTemporaryStore = temporaryStore.setUserName(user.getUserName())
                    .setToken(token)
                    .setRequestedUpdate(Enum.RequestedUpdate.EMAIL)
                    .setSendVia(Enum.MessageSender.SMS)
                    .setAttempts(0);


            //Send SMS token to the client
            defaultSmsService.ComposeChangeEmailTokenSms(user,token);

            defaultTemporaryStoreService.saveTemporaryStore(temporaryStore);

            responseModel.setIsSuccessful(true).setResponseMessage("Successful, Token has been generated successfully");

            return new ResponseEntity<>(new ProfileUpdateModel(savedTemporaryStore.getId(), responseModel), HttpStatus.OK);


        } catch (Exception ex) {

            System.out.println(ex.getMessage());

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new ProfileUpdateModel(null, new ResponseModel(false, "Oops! an error occurred")), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/profile/update/phonenumber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileUpdateModel> UpdatePhoneNumber(@RequestBody TemporaryStore temporaryStore, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            String token = defaultTokenService.generateToken(6);

             temporaryStore.setUserName(user.getUserName())
                    .setToken(token)
                    .setRequestedUpdate(Enum.RequestedUpdate.PHONENUMBER)
                    .setSendVia(Enum.MessageSender.EMAIL)
                    .setAttempts(0);

            EmailStatus emailStatus = emailHtmlSender.send(user.getEmail(), "Phone number update token", "email/update-token", defaultMailService.ComposeUserProfileUpdateMail(user, token, Enum.RequestedUpdate.PHONENUMBER));


            TemporaryStore savedTemporaryStore = defaultTemporaryStoreService.saveTemporaryStore(temporaryStore);

            responseModel.setIsSuccessful(true).setResponseMessage("Successful, Token has been generated successfully");

            return new ResponseEntity<>(new ProfileUpdateModel(savedTemporaryStore.getId(), responseModel), HttpStatus.OK);


        } catch (Exception ex) {

            System.out.println(ex.getMessage());

            LOGGER.error(ex.getMessage());

            responseModel.setIsSuccessful(false).setResponseMessage("Oops! connection lost, please try again");

            return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), responseModel), HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/profile/update/token/{tempStoreId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> SwallowToken(@PathVariable("tempStoreId") long tempStoreId, @RequestBody String token, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            TemporaryStore temporaryStore = defaultTemporaryStoreService.getTemporaryStoreByIdAndToken(tempStoreId,token);

            if (Objects.isNull(temporaryStore)) {

                responseModel.setIsSuccessful(false).setResponseMessage("You have entered an invalid token!");

                return new ResponseEntity<>(responseModel, HttpStatus.OK);

            } else {

                String requestedUpdate;

                if (user.getUserName().equals(temporaryStore.getUserName())) {

                    switch (temporaryStore.getRequestedUpdate()) {

                        case EMAIL:
                            user.setEmail(temporaryStore.getValue());
                            requestedUpdate = "email";
                            break;

                        case PASSWORD:
                            user.setPassword(temporaryStore.getValue());
                            requestedUpdate = "password";
                            break;

                        case USERNAME:
                            user.setUserName(temporaryStore.getValue());
                            requestedUpdate = "username";
                            break;

                        case PHONENUMBER:
                            user.setPhoneNumber(temporaryStore.getValue());
                            requestedUpdate = "phone number";
                            break;

                        default:

                            requestedUpdate = "nothing";
                    }

                    defaultTemporaryStoreService.delete(temporaryStore);

                    defaultUserDAOService.updateUser(user);

                    responseModel.setIsSuccessful(true).setResponseMessage("Your " + requestedUpdate + " has been successfully updated!");

                    return new ResponseEntity<>(responseModel, HttpStatus.OK);

                } else {

                    responseModel.setIsSuccessful(false).setResponseMessage("You have entered an invalid token!");

                    return new ResponseEntity<>(responseModel, HttpStatus.OK);

                }

            }

        } catch (Exception ex) {

            responseModel.setIsSuccessful(false).setResponseMessage("Oops!, a connection error has occurred!");

            return new ResponseEntity<>(responseModel, HttpStatus.OK);

        }

    }


    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> UsersDetail(@PathVariable("username") String userName, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            User profiledUser = defaultUserDAOService.getByUsername(userName);

            FriendRequest friendRequest = defaultFriendRequestService.findFriendRequestByUsers(user, profiledUser);

            int numberOfFriends = defaultFriendRequestService.getFriends(profiledUser).size();

            int numberOfMutualFriends = defaultFriendRequestService.getMutualFriends(user, profiledUser).size();

            userModel.setSuccessful(true)
                    .setResponseMessage("Successful")
                    .setUser(profiledUser)
                    .setFriendsCount(numberOfFriends)
                    .setMutualFriendsCount(numberOfMutualFriends)
                    .setFriendRequestStatus(friendRequest == null ? Enum.FriendRequestStatus.DECLINED : friendRequest.getFriendRequestStatus());

            return new ResponseEntity<>(userModel, HttpStatus.OK);

        } catch (Exception ex) {

            userModel.setSuccessful(false).setResponseMessage("Oops!, a connection error has occurred!").setUser(null);

            return new ResponseEntity<>(userModel, HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/friends/{pageNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginationModel> GetFriendsPage(@PathVariable int pageNumber, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            Page<FriendRequest> pages = defaultFriendRequestService.getAcceptedFriendRequestPages(user, Enum.FriendRequestStatus.ACCEPTED, new PageRequest(pageNumber, 6, Sort.Direction.ASC, "id"));

            paginationModel.setNumberOfPages(pages.getTotalPages())
                    //pages.getContent() is a method in pages that gets the content of the Pages request
                    .setFriends(defaultFriendRequestService.getFriends(user, pages.getContent()))
                    .setNumberOfUsers((int) pages.getTotalElements())
                    .setResponseMessage("Successful")
                    .setSuccessful(true);

            return new ResponseEntity<>(paginationModel, HttpStatus.OK);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            paginationModel.setNumberOfPages(0)
                    //pages.getContent() is a method in pages that gets the content of the Pages request
                    .setFriends(Collections.EMPTY_LIST)
                    .setNumberOfUsers(0)
                    .setResponseMessage("Failed")
                    .setSuccessful(false);

            return new ResponseEntity<>(paginationModel, HttpStatus.OK);

        }
    }

    }
