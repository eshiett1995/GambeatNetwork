package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Oto-obong on 21/10/2017.
 */

@Component
public class UserModel {

    private User user;

    private Enum.FriendRequestStatus friendRequestStatus;

    private List<User> friends;

    private  int friendsCount;

    private List<User> mutualFriends;

    private int mutualFriendsCount;

    private String responseMessage;

    private boolean isSuccessful;



    public User getUser() {
        return user;
    }

    public UserModel setUser(User user) {
        this.user = user;
        return this;
    }

    public Enum.FriendRequestStatus getFriendRequestStatus() {
        return friendRequestStatus;
    }

    public UserModel setFriendRequestStatus(Enum.FriendRequestStatus friendRequestStatus) {
        this.friendRequestStatus = friendRequestStatus;
        return this;
    }

    public List<User> getFriends() {
        return friends;
    }

    public UserModel setFriends(List<User> friends) {
        this.friends = friends;
        return this;
    }

    public List<User> getMutualFriends() {
        return mutualFriends;
    }

    public UserModel setMutualFriends(List<User> mutualFriends) {
        this.mutualFriends = mutualFriends;
        return this;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public UserModel setFriendsCount(int friendsCount) {
        this.friendsCount = friendsCount;
        return this;
    }

    public int getMutualFriendsCount() {
        return mutualFriendsCount;
    }

    public UserModel setMutualFriendsCount(int mutualFriendsCount) {
        this.mutualFriendsCount = mutualFriendsCount;
        return this;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public UserModel setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
        return this;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public UserModel setSuccessful(boolean successful) {
        isSuccessful = successful;
        return this;
    }
}
