package com.gambeat.site.controllers.restcontroller.thirdparty.jsonmodel;

import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.Competition;
import com.gambeat.site.entities.User;

public class GameServerPlayerModel {

    private User user;

    private Competition competition;

    private double entryFee;

    private ResponseModel responseModel;

    public double getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(double entryFee) {
        this.entryFee = entryFee;
    }

    public User getUser() {
        return user;
    }

    public GameServerPlayerModel setUser(User user) {
        this.user = user;
        return this;
    }

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public GameServerPlayerModel setResponseModel(ResponseModel responseModel) {
        this.responseModel = responseModel;
        return this;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }
}
