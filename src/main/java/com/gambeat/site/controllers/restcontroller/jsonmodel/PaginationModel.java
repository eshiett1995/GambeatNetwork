package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oto-obong on 09/11/2017.
 */

@Component
public class PaginationModel {

    private int numberOfPages;

    private int numberOfUsers;

    private List<User> users;

    private int totalNumberOfUnreadMessages;

    private Page<FriendRequest> friendsPage;

    private Page<Message> messagesPage;

    private List<MessageModel> messageModels = new ArrayList<>();

    private List<User> friends;

    private List<User> referrals;

    private boolean isSuccessful;

    private String responseMessage;

    public Page<FriendRequest> getFriendsPage() {
        return friendsPage;
    }

    public PaginationModel setFriendsPage(Page<FriendRequest> friendsPage) {
        this.friendsPage = friendsPage;
        return this;
    }

    public int getTotalNumberOfUnreadMessages() {

        return totalNumberOfUnreadMessages;

    }

    public void setTotalNumberOfUnreadMessages(int totalNumberOfUnreadMessages) {

        this.totalNumberOfUnreadMessages = totalNumberOfUnreadMessages;

    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public PaginationModel setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
        return this;
    }

    public int getNumberOfUsers() {
        return numberOfUsers;
    }

    public PaginationModel setNumberOfUsers(int numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
        return this;
    }

    public List<User> getUsers() {
        return users;
    }

    public PaginationModel setUsers(List<User> users) {
        this.users = users;
        return this;
    }

    public List<User> getFriends() {
        return friends;
    }

    public PaginationModel setFriends(List<User> friends) {
        this.friends = friends;
        return this;
    }

    public List<User> getReferrals() {
        return referrals;
    }

    public PaginationModel setReferrals(List<User> referrals) {
        this.referrals = referrals;
        return this;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public PaginationModel setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
        return this;
    }

    public Page<Message> getMessagesPage() {
        return messagesPage;
    }

    public PaginationModel setMessagesPage(Page<Message> messagesPage) {

        this.messagesPage = messagesPage;

        return this;
    }

    public List<MessageModel> getMessageModels() {

        return messageModels;

    }

    public PaginationModel setMessageModels(List<MessageModel> messageModels) {

        this.messageModels = messageModels;

        return this;

    }
}
