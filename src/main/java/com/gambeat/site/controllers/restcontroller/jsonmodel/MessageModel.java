package com.gambeat.site.controllers.restcontroller.jsonmodel;


import com.gambeat.site.entities.Message;
import org.springframework.data.domain.Page;


public class MessageModel {

    private int totalNumberOfUnreadMessages;

    private Page<FriendAndMessagesModel> friendsAndMessagesModelPage;

    private ResponseModel responseModel;

    public int getTotalNumberOfUnreadMessages() {
        return totalNumberOfUnreadMessages;
    }

    public MessageModel setTotalNumberOfUnreadMessages(int totalNumberOfUnreadMessages) {
        this.totalNumberOfUnreadMessages = totalNumberOfUnreadMessages;
        return this;
    }

    public Page<FriendAndMessagesModel> getFriendsAndMessagesModelPage() {
        return friendsAndMessagesModelPage;
    }

    public MessageModel setFriendsAndMessagesModelPage(Page<FriendAndMessagesModel> friendsAndMessagesModelPage) {
        this.friendsAndMessagesModelPage = friendsAndMessagesModelPage;
        return this;
    }

    public ResponseModel getResponseModel() {

        return responseModel;

    }

    public void setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

    }
}
