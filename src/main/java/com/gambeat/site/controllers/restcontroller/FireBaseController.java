package com.gambeat.site.controllers.restcontroller;


import com.gambeat.site.controllers.restcontroller.jsonmodel.FirebaseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.TransactionModel;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/firebase")
public class FireBaseController {

    @Autowired
    DefaultCookieService defaultCookieService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @ResponseBody
    @RequestMapping(value = "/register/token", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FirebaseModel> registerFirebaseTokenToUser(@RequestBody FirebaseModel firebaseModel, HttpServletRequest request) {

        User user = this.defaultCookieService.getUser("gambeat", request);

        user.getFcmToken().add(firebaseModel.getToken());

        this.defaultUserDAOService.updateUser(user);

        return new ResponseEntity<>(firebaseModel.setResponseModel(new ResponseModel(true, "Successful")), HttpStatus.OK); // return response to client.

    }
}