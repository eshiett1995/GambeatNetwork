package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Oto-obong on 02/10/2017.
 */

@Component
public class FriendsModel {

    List<User> friends;

    Boolean isSuccessful;

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public Boolean getSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(Boolean successful) {
        isSuccessful = successful;
    }
}
