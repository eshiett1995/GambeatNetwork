package com.gambeat.site.controllers.restcontroller;

import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.Activations;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.PlayerStats;
import com.gambeat.site.entities.User;
import com.gambeat.site.security.fore.JwtTokenUtil;
import com.gambeat.site.services.MailService;
import com.gambeat.site.services.implementation.*;
import com.gambeat.site.utility.email.EmailHtmlSender;
import com.gambeat.site.utility.email.EmailStatus;
import com.google.gson.Gson;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.*;
import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Set;


/**
 * Created by Oto-obong on 02/09/2017.
 */

@RestController
@RequestMapping(value = "/welcome")
public class WelcomeController {

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(WelcomeController.class);

    @Autowired
    private AuthenticationManager authenticationManager;


    @Autowired
    DefaultCookieService defaultCookieService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Autowired
    DefaultFileService defaultFileService;

    @Autowired
    ResponseModel responseModel;

    @Autowired
    DefaultMailService defaultMailService;

    @Autowired
    DefaultTokenService defaultTokenService;

    @Autowired
    EmailHtmlSender emailHtmlSender;

    @Autowired
    DefaultActivationsService defaultActivationsService;

    @Autowired
    Validator validator;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @Autowired
    @Qualifier("customerDetailsService")
    private UserDetailsService userDetailsService;

    private static final String UPLOADED_FOLDER = "C:\\Users\\Java\\IdeaProjects\\GambeatNetwork (gitlab)\\src\\main\\resources\\static\\profile-pictures\\" ;




    public WelcomeController() {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

        this.validator = factory.getValidator();

    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials!", e);
        }
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> Login(@RequestBody User user, HttpServletRequest request, HttpServletResponse response){

        authenticate(user.getUserName(),user.getPassword());


        // Reload password post-security so we can generate the token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUserName());

        final String token = jwtTokenUtil.generateToken(userDetails);


        // check if the username and password provided matches a user in the database.
        String verifiedResponse = this.defaultUserDAOService.verification(user.setPassword(userDetails.getPassword()));

        if(verifiedResponse.equalsIgnoreCase("Verified")){ // if the user was verified successfully (Found in the database)

            responseModel.setIsSuccessful(true);

            // gets the session ID from the request
            HttpSession session = request.getSession(true);

            // Saves the user's userName by sessionID in the Session Database
            session.setAttribute(session.getId(),user.getUserName());


            responseModel.setJwtToken(token);
            // Puts the session id with in the response model.
            responseModel.setSessionId(session.getId());

            responseModel.setResponseMessage("Successfully Verified");


            // generates the cookie, puts the session id in the cookie and sends it in the response body
            response.addCookie(defaultCookieService.generateCookie(session.getId()));

            return new ResponseEntity<>(responseModel, HttpStatus.OK); // return response to client.


        }else{

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage(verifiedResponse);

            return new ResponseEntity<>(responseModel, HttpStatus.OK); // return response to client.

        }


    }



    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<ResponseModel> SignUp(@RequestParam("file") MultipartFile file, @RequestParam("model") String userJson, RedirectAttributes redirectAttributes){


        User tempSavedUser = new User();

        try {

                if (file.isEmpty()) {
                    redirectAttributes.addFlashAttribute("message", "Please select a file to upload");

                    responseModel.setIsSuccessful(false).setResponseMessage("Please select an image to upload");

                    return new ResponseEntity<>(responseModel, HttpStatus.CREATED); // return response to client.
                }

                User user = new Gson().fromJson(userJson, User.class);

                user.setUserType(Enum.UserType.PLAYER);


                //check to see if the responded user meets requirement
                Set<ConstraintViolation<User>> violations = validator.validate(user);

                if(!violations.isEmpty()){

                    for (ConstraintViolation<User> violation : violations) {

                        responseModel.setIsSuccessful(false).setResponseMessage(violation.getMessage());

                        return new ResponseEntity<>(responseModel, HttpStatus.OK); // return response to client.

                    }

                }



            //checks Database to know if a user already exists with the same username or email
                String existsResponse = this.defaultUserDAOService.exists(user);

                // if non exists then persists user to the Database
                if (existsResponse.equalsIgnoreCase("No records found")) {

                    if (false) { // checks if the data-binding (jpa validation errors) has errors

                        responseModel.setIsSuccessful(false);

                        responseModel.setResponseMessage("npthin");

                        return new ResponseEntity<ResponseModel>(responseModel, HttpStatus.OK);


                    } else {


                        String token = defaultTokenService.generateToken(10);

                        while (defaultActivationsService.TokenExists(token)) {

                            token = defaultTokenService.generateToken(10);
                        }


                        EmailStatus emailStatus = emailHtmlSender.send(user.getEmail(), "Welcome to Gambeat", "email/registration-token", defaultMailService.ComposeUserActivationMail(user, token));

                        if (emailStatus.isError()) {

                            return new ResponseEntity<>(new ResponseModel(false, "Unable to send email to the client, please check you mail and try again."), HttpStatus.NOT_FOUND);

                        }

                        user.setDateCreated();

                        user.setPicture(user.getUserName());

                        user.setPlayerStats(new PlayerStats())
                                .setBio("A man’s gotta make at least one bet a day, else he could be walking around lucky and never know it.")
                                .setLocked(false);

                        tempSavedUser = this.defaultUserDAOService.saveUser(user);

                        defaultActivationsService.save(new Activations(tempSavedUser,token));

                        byte[] bytes = file.getBytes();

                        Path path = Paths.get(UPLOADED_FOLDER + tempSavedUser.getUserName() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));

                        Files.write(path, bytes);

                        redirectAttributes.addFlashAttribute("message",
                                "You successfully uploaded '" + file.getOriginalFilename() + "'");


                        responseModel.setIsSuccessful(true);

                        responseModel.setResponseMessage("Registration was successful, Please check your mail for an account activation link");

                        return new ResponseEntity<ResponseModel>(responseModel, HttpStatus.CREATED); // return response to client.

                    }


                    // if record exists on the Database send a response to the user that his username and email has been used already.
                } else {

                    responseModel.setIsSuccessful(false);

                    responseModel.setResponseMessage(existsResponse);

                    return new ResponseEntity<ResponseModel>(responseModel, HttpStatus.OK); // return response to the client/
                }


        }catch (Exception ex) {

        if (!Objects.isNull(tempSavedUser.getUserName())) {

            defaultUserDAOService.deleteUser(tempSavedUser);

        }

        LOGGER.error(ex.getMessage()); // log the exception message;

        responseModel.setIsSuccessful(false);

        responseModel.setResponseMessage(ex.getMessage());

        return new ResponseEntity<>(responseModel, HttpStatus.OK);

    }

  }
}
