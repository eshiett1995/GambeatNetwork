package com.gambeat.site.controllers.restcontroller.jsonmodel;

import com.gambeat.site.entities.FriendRequest;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Oto-obong on 02/10/2017.
 */

@Component
public class FriendRequestsModel {

    List<FriendRequest> friendRequests;

    Boolean isSuccessful;

    public List<FriendRequest> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(List<FriendRequest> friendRequests) {
        this.friendRequests = friendRequests;
    }

    public Boolean getSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(Boolean successful) {
        isSuccessful = successful;
    }
}
