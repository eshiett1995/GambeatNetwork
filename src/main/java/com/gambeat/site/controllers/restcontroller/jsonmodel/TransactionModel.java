package com.gambeat.site.controllers.restcontroller.jsonmodel;


import com.gambeat.site.entities.Transactions;
import com.gambeat.site.entities.User;
import org.springframework.stereotype.Component;

@Component
public class TransactionModel {


    private Transactions transaction;

    public Transactions getTransaction() {
        return transaction;
    }

    public TransactionModel setTransaction(Transactions transaction) {
        this.transaction = transaction;
        return this;
    }

    private ResponseModel responseModel;

    private WithdrawModel withdrawModel;

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public TransactionModel setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

        return this;

    }

    public WithdrawModel getWithdrawModel() {

        return withdrawModel;

    }

    public TransactionModel setWithdrawModel(WithdrawModel withdrawModel) {

        this.withdrawModel = withdrawModel;

        return this;

    }
}
