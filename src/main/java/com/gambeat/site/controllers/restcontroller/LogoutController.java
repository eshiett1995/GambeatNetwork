package com.gambeat.site.controllers.restcontroller;

import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Oto-obong on 08/11/2017.
 */

@RestController
public class LogoutController {

    @Autowired
    private DefaultCookieService defaultCookieService;

    @Autowired
    private DefaultUserDAOService defaultUserDAOService;

    @Autowired
    private ResponseModel responseModel;

    @ResponseBody
    @RequestMapping(value = "/signout", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> Logout (HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultCookieService.getUser("gambeat", request);

            user.setLocked(false);

            response.addCookie(defaultCookieService.generateCookie("LoggedOut"));

            responseModel.setIsSuccessful(true).setResponseMessage("Successful");

            return new ResponseEntity<>(responseModel, HttpStatus.OK);

        }catch (Exception ex){

            responseModel.setIsSuccessful(false).setResponseMessage("Oops! Connection error.");

            return new ResponseEntity<>(responseModel, HttpStatus.OK);

        }
    }

  }
