package com.gambeat.site.controllers.third_party.jsonmodel;

import com.gambeat.site.entities.User;

public class Chess {

    private User friend;

    public User getFriend() {
        return friend;
    }

    public void setFriend(User friend) {
        this.friend = friend;
    }
}
