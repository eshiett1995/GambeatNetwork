package com.gambeat.site.controllers.third_party;


import com.gambeat.site.controllers.restcontroller.jsonmodel.CompetitionResponseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.EnterCompetitionModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.Competition;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Game;
import com.gambeat.site.services.implementation.DefaultCompetitionService;
import com.gambeat.site.services.implementation.DefaultGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@RestController
@RequestMapping(value = "api/third-party/activity")
public class MActivityController {

    @Autowired
    DefaultCompetitionService defaultCompetitionService;

    @Autowired
    DefaultGameService defaultGameService;


    @ResponseBody
    @RequestMapping(value = "/join/competition", method = RequestMethod.POST)
    public ResponseEntity<CompetitionResponseModel> JoinCompetition(@RequestBody EnterCompetitionModel enterCompetitionModel, HttpServletRequest request, HttpServletResponse res) throws IOException {

        Competition foundCompetition = defaultCompetitionService.checkIfCompetitionExist(enterCompetitionModel.getCompetition().getGame(), enterCompetitionModel.getCompetition().getEntryFee());

        if(Objects.isNull(foundCompetition)){

            return new ResponseEntity<>(new CompetitionResponseModel(new ResponseModel(true,String.format("No competition titled s% was found", enterCompetitionModel.getCompetition().getCompetitionName()))), HttpStatus.OK);


        }else if(enterCompetitionModel.getCompetition().getPunters().size() == 100 || enterCompetitionModel.getCompetition().getCompetitionStatus() != Enum.CompetitionStatus.OPEN){

            return new ResponseEntity<>(new CompetitionResponseModel(enterCompetitionModel.getCompetition(), new ResponseModel(true,String.format("The s% competition is closed to new contestants, please try entering another or starting one", enterCompetitionModel.getCompetition().getCompetitionName()))), HttpStatus.OK);

        }else{

            RestTemplate restTemplate = new RestTemplate();

            HttpEntity<EnterCompetitionModel> body = new HttpEntity<>(enterCompetitionModel);

            ResponseEntity<ResponseModel> responseEntity = restTemplate.exchange(enterCompetitionModel.getCompetition().getGame().getJoinCompetitionUrl(), HttpMethod.POST, body, ResponseModel.class);

            if(responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.ACCEPTED){

                return new ResponseEntity<>(new CompetitionResponseModel(responseEntity.getBody()), HttpStatus.OK);

            }else{

                return new ResponseEntity<>(new CompetitionResponseModel(new ResponseModel(false, "Oops! an error occurred on the server, so we couldn't create a competition for you.")), responseEntity.getStatusCode());

            }

        }
    }

    @ResponseBody
    @RequestMapping(value = "/create/competition", method = RequestMethod.POST)
    public ResponseEntity<CompetitionResponseModel> CreateCompetition(@RequestBody EnterCompetitionModel enterCompetitionModel, HttpServletRequest request, HttpServletResponse res) throws IOException {

        Competition foundCompetition = defaultCompetitionService.checkIfCompetitionExist(enterCompetitionModel.getCompetition().getGame(), enterCompetitionModel.getCompetition().getEntryFee());

        if(Objects.isNull(foundCompetition)){

            RestTemplate restTemplate = new RestTemplate();

            HttpEntity<EnterCompetitionModel> body = new HttpEntity<>(enterCompetitionModel);

            ResponseEntity<ResponseModel> responseEntity = restTemplate.exchange(enterCompetitionModel.getCompetition().getGame().getCreateCompetitionUrl(), HttpMethod.POST, body, ResponseModel.class);

            if(responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.ACCEPTED){

                return new ResponseEntity<>(new CompetitionResponseModel(responseEntity.getBody()), HttpStatus.OK);

            }else{

                return new ResponseEntity<>(new CompetitionResponseModel(new ResponseModel(false, "Oops! an error occurred on the server, so we couldn't create a competition for you.")), responseEntity.getStatusCode());

            }

        }else{

            return new ResponseEntity<>(new CompetitionResponseModel(foundCompetition,new ResponseModel(true,String.format("A competition titled s% is already up, would you love to join it ?"))), HttpStatus.OK);
        }

    }

    @ResponseBody
    @RequestMapping(value = "/games/page/{page}", method = RequestMethod.POST)
    public ResponseEntity<Page<Game>> ShowGames(@PathVariable("page") int page, @RequestBody Game game) throws IOException {

        Page<Game> gamePage;

        if(game.getName().isEmpty() && game.getPlatforms().isEmpty() && game.getGenres().isEmpty()){

            gamePage = defaultGameService.getAll(new PageRequest(page, 20));

        }else if(!game.getName().isEmpty() && game.getPlatforms().isEmpty() && game.getGenres().isEmpty()){

            gamePage = defaultGameService.findGamesByNameContaining(game.getName(), new PageRequest(page, 20));

        }else if(game.getName().isEmpty() && !game.getPlatforms().isEmpty() && game.getGenres().isEmpty()){

            gamePage = defaultGameService.findGamesByPlatform(game.getPlatforms().get(0), new PageRequest(page, 20));

        }else if(game.getName().isEmpty() && game.getPlatforms().isEmpty() && !game.getGenres().isEmpty()){

            gamePage = defaultGameService.findGamesByGenre(game.getGenres().get(0), new PageRequest(page, 20));

        }else if(!game.getName().isEmpty() && !game.getPlatforms().isEmpty() && game.getGenres().isEmpty()){

            gamePage = defaultGameService.findGamesByNameContainingAndPlatform(game.getName(), game.getPlatforms().get(0), new PageRequest(page, 20));

        }else if(!game.getName().isEmpty() && game.getPlatforms().isEmpty() && !game.getGenres().isEmpty()){

            gamePage = defaultGameService.findGamesByNameContainingAndGenre(game.getName(), game.getGenres().get(0), new PageRequest(page, 20));

        }else if(game.getName().isEmpty() && !game.getPlatforms().isEmpty() && !game.getGenres().isEmpty()){

            gamePage = defaultGameService.findGamesByPlatformAndGenre(game.getPlatforms().get(0), game.getGenres().get(0), new PageRequest(page, 20));

        }else{

            gamePage = defaultGameService.findGamesByNameContainingAndPlatformsAndGenres(game.getName(),game.getPlatforms().get(0), game.getGenres().get(0), new PageRequest(page, 20));

        }



        return new ResponseEntity<>(gamePage, HttpStatus.OK);

    }



    @ResponseBody
    @RequestMapping(value = "/competitions/{participant}/page/{page}", method = RequestMethod.POST)
    public ResponseEntity<Page<Competition>> ShowCompetitions(
            @PathVariable("page") int page, @PathVariable("participant") boolean isParticipant, @RequestBody Competition competition) throws IOException {

        Page<Competition> competitionPage = defaultCompetitionService.queryCompetitionTable(competition, new PageRequest(page, 20));

        return new ResponseEntity<>(competitionPage, HttpStatus.OK);

    }


}
