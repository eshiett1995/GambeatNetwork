package com.gambeat.site.controllers.third_party;


import com.gambeat.site.controllers.restcontroller.AuthenticationException;
import com.gambeat.site.controllers.third_party.jsonmodel.LoginModel;
import com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.PlayerStats;
import com.gambeat.site.entities.User;
import com.gambeat.site.security.fore.JwtTokenUtil;
import com.gambeat.site.services.implementation.DefaultActivationsService;
import com.gambeat.site.services.implementation.DefaultMailService;
import com.gambeat.site.services.implementation.DefaultTokenService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import com.gambeat.site.utility.email.EmailHtmlSender;
import com.gambeat.site.utility.email.EmailStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping(value = "api/third-party")
public class MWelcomeController {

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(MWelcomeController.class);


    @Autowired
    DefaultMailService defaultMailService;

    @Autowired
    Validator validator;

    @Autowired
    DefaultTokenService defaultTokenService;

    @Autowired
    EmailHtmlSender emailHtmlSender;

    @Autowired
    DefaultActivationsService defaultActivationsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;


    @Autowired
    @Qualifier("customerDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;




    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials!", e);
        }
    }


    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginModel> Login(@RequestBody User user, HttpServletRequest request, HttpServletResponse response){

        authenticate(user.getUserName(),user.getPassword());


        // Reload password post-security so we can generate the token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUserName());

        final String token = jwtTokenUtil.generateToken(userDetails);


        // check if the username and password provided matches a user in the database.
        String verifiedResponse = this.defaultUserDAOService.verification(user);

        ResponseModel responseModel = new ResponseModel();

        User foundUser = new User();


        if(verifiedResponse.equalsIgnoreCase("Verified")){ // if the user was verified successfully (Found in the database)

            foundUser = defaultUserDAOService.getByUsername(user.getUserName());

            foundUser.setFriend(false);

             responseModel.setIsSuccessful(true);

            // gets the session ID from the request
            HttpSession session = request.getSession(true);

            // Saves the user's userName by sessionID in the Session Database
            session.setAttribute(session.getId(),user.getUserName());


            responseModel.setJwtToken(token);
            // Puts the session id with in the response model.
            responseModel.setSessionId(session.getId());

            responseModel.setResponseMessage("Successfully Verified");

            return new ResponseEntity<>(new LoginModel(foundUser, responseModel), HttpStatus.OK); // return response to client.


        }else{

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage(verifiedResponse);

            return new ResponseEntity<>(new LoginModel(foundUser, responseModel), HttpStatus.OK); // return response to client.

        }


    }


    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<ResponseModel> SignUp(@RequestBody User user){


        User tempSavedUser = new User();

        ResponseModel responseModel = new ResponseModel();

        try {


            user.setUserType(Enum.UserType.PLAYER);


            //check to see if the responded user meets requirement
            Set<ConstraintViolation<User>> violations = validator.validate(user);

            if(!violations.isEmpty()){

                for (ConstraintViolation<User> violation : violations) {

                    responseModel.setIsSuccessful(false).setResponseMessage(violation.getMessage());

                    return new ResponseEntity<>(responseModel, HttpStatus.OK); // return response to client.

                }

            }



            //checks Database to know if a user already exists with the same username or email
            String existsResponse = this.defaultUserDAOService.exists(user);

            // if non exists then persists user to the Database
            if (existsResponse.equalsIgnoreCase("No records found")) {


                    String token = defaultTokenService.generateToken(10);

                    while (defaultActivationsService.TokenExists(token)) {

                        token = defaultTokenService.generateToken(10);
                    }


                    EmailStatus emailStatus = emailHtmlSender.send(user.getEmail(), "Welcome to Gambeat", "email/token", defaultMailService.ComposeUserActivationMail(user, token));

                    if (emailStatus.isError()) {

                        return new ResponseEntity<>(new ResponseModel(false, "Unable to send email to the client, please check you mail and try again."), HttpStatus.CREATED);

                    }

                    user.setDateCreated();

                    user.setPicture(user.getUserName());

                    user.setPlayerStats(new PlayerStats())
                            .setBio("A man’s gotta make at least one bet a day, else he could be walking around lucky and never know it.")
                            .setLocked(false);

                    tempSavedUser = this.defaultUserDAOService.saveUser(user);



                    responseModel.setIsSuccessful(true);

                    responseModel.setResponseMessage("Registration was successful, Please check your mail for an account activation link");

                    return new ResponseEntity<>(responseModel, HttpStatus.CREATED); // return response to client.



                // if record exists on the Database send a response to the user that his username and email has been used already.
            } else {

                responseModel.setIsSuccessful(false);

                responseModel.setResponseMessage(existsResponse);

                return new ResponseEntity<>(responseModel, HttpStatus.OK); // return response to the client/
            }


        }catch (Exception ex) {

            if (!Objects.isNull(tempSavedUser) ) {

                defaultUserDAOService.deleteUser(tempSavedUser);

            }

            LOGGER.error(ex.getMessage()); // log the exception message;

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage(ex.getMessage());

            return new ResponseEntity<>(responseModel, HttpStatus.OK);

        }

    }



}
