package com.gambeat.site.controllers.third_party.jsonmodel;

import org.springframework.stereotype.Component;

/**
 * Created by Oto-obong on 02/09/2017.
 */

public class ResponseModel {


    public ResponseModel(){}

    public ResponseModel(Boolean isSuccessful, String responseMessage){

        this.isSuccessful = isSuccessful;

        this.responseMessage = responseMessage;

    }


    private String jwtToken;

    private String sessionId;

    public Boolean isSuccessful;

    public String responseMessage;

    public String getJwtToken() {

        return jwtToken;

    }

    public ResponseModel setJwtToken(String jwtToken) {

        this.jwtToken = jwtToken;

        return this;
    }

    public String getSessionId() {
        return sessionId;
    }

    public ResponseModel setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public Boolean getIsSuccessful() {
        return isSuccessful;
    }

    public ResponseModel setIsSuccessful(Boolean isSuccessful) {
        this.isSuccessful = isSuccessful;
        return this;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public ResponseModel setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
        return this;
    }
}
