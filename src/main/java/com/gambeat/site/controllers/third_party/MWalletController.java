package com.gambeat.site.controllers.third_party;


import com.gambeat.site.entities.Transactions;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultFriendRequestService;
import com.gambeat.site.services.implementation.DefaultTransactionService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;

@RestController
@RequestMapping(value = "api/third-party/wallet")
public class MWalletController {

    @Autowired
    DefaultTransactionService defaultTransactionService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;


    @ResponseBody
    @RequestMapping(value = "/transactions/{page}", method = RequestMethod.GET)
    public ResponseEntity<Page<Transactions>> Login(@PathVariable int page, HttpServletRequest request, HttpServletResponse response) {

        User userz = defaultUserDAOService.getUser(String.valueOf(1));

        Page<Transactions> transactionsPage = defaultTransactionService.getUsersTransactions(userz, new PageRequest(page,20));

        return new ResponseEntity<>(transactionsPage, HttpStatus.OK);
    }

}
