package com.gambeat.site.controllers.third_party.jsonmodel;

import com.gambeat.site.entities.Message;

public class SocketIOMessageModel {

    public SocketIOMessageModel(){

    }

    public SocketIOMessageModel(Message message, ResponseModel responseModel) {

        this.message = message;

        this.responseModel = responseModel;
    }

    private Message message;

    private ResponseModel responseModel;

    public Message getMessage() {

        return message;

    }

    public SocketIOMessageModel setMessage(Message message) {

        this.message = message;

        return this;

    }

    public ResponseModel getResponseModel() {

        return responseModel;

    }

    public SocketIOMessageModel setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

        return this;

    }
}
