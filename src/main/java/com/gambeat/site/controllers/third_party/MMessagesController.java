package com.gambeat.site.controllers.third_party;


import com.gambeat.site.controllers.restcontroller.MessageController;
import com.gambeat.site.controllers.restcontroller.jsonmodel.FriendAndMessagesModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.MessageModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.PaginationModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultCookieService;
import com.gambeat.site.services.implementation.DefaultFriendRequestService;
import com.gambeat.site.services.implementation.DefaultMessageService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/third-party/message")
public class MMessagesController {

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);


    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Autowired
    DefaultCookieService defaultCookieService;

    @Autowired
    DefaultMessageService defaultMessageService;

    @Autowired
    DefaultFriendRequestService defaultFriendRequestService;

    @ResponseBody
    @RequestMapping(value = "/all/{page}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageModel> GetPaginatedMessage(@PathVariable int page, Pageable pageable, HttpServletRequest request) {

         User user = this.defaultUserDAOService.getUser(String.valueOf(1));

        Page<Message> foundMessagePage = defaultMessageService.findMostRecentMessageFromConversation(user,new PageRequest(0,20));


/**
        List<FriendAndMessagesModel> friendAndMessagesModelList = new ArrayList<>();

        for (Message message:foundMessagePage.getContent()) {

            FriendAndMessagesModel friendAndMessagesModel = new FriendAndMessagesModel();

            if(user == message.getSender()){

                friendAndMessagesModel.setFriend(message.getReceiver()
                        .setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(message.getReceiver(), user, false)));

            }else{

                friendAndMessagesModel.setFriend(message.getSender()
                        .setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(message.getSender(),user, false)));
            }

            friendAndMessagesModel.getMessages().add(message);

            friendAndMessagesModelList.add(friendAndMessagesModel);
        }

        Page<FriendAndMessagesModel> friendAndMessageModel = new PageImpl<FriendAndMessagesModel>(friendAndMessagesModelList);


        MessageModel messageModel = new MessageModel();

        messageModel.setTotalNumberOfUnreadMessages(defaultMessageService.countTotalUnreadMessages(user));

        messageModel.setFriendsAndMessagesModelPage(friendAndMessageModel);

        messageModel.setResponseModel(new ResponseModel(true, "Successful"));
        **/



        List<Message> messages = new ArrayList<>();

        for(int i = 0; i < 20; i++ ){

            for(int c = 0; c < foundMessagePage.getContent().size(); c++ ) {

                messages.add(foundMessagePage.getContent().get(0));

            }
        }




        Page<Message> foundMessagePage2 = new PageImpl<Message>(messages, new PageRequest(page,20),messages.size());

        List<FriendAndMessagesModel> friendAndMessagesModelList = new ArrayList<>();

        for (Message message:foundMessagePage2.getContent()) {

            FriendAndMessagesModel friendAndMessagesModel = new FriendAndMessagesModel();

            if(user == message.getSender()){

                friendAndMessagesModel.setFriend(message.getReceiver()
                        .setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(message.getReceiver(), user, false)));

            }else{

                friendAndMessagesModel.setFriend(message.getSender()
                        .setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(message.getSender(),user, false)));
            }

            friendAndMessagesModel.getMessages().add(message);

            friendAndMessagesModelList.add(friendAndMessagesModel);
        }

        Page<FriendAndMessagesModel> friendAndMessageModel = new PageImpl<FriendAndMessagesModel>(friendAndMessagesModelList);


        MessageModel messageModel = new MessageModel();

        messageModel.setTotalNumberOfUnreadMessages(defaultMessageService.countTotalUnreadMessages(user));

        messageModel.setFriendsAndMessagesModelPage(friendAndMessageModel);

        messageModel.setResponseModel(new ResponseModel(true, "Successful"));


        return new ResponseEntity<>(messageModel, HttpStatus.OK);

    }


    @ResponseBody
    @RequestMapping(value = "/chat/{userID}/{page}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Message>> GetPaginatedMessage(@PathVariable Long userID, @PathVariable int page, Pageable pageable, HttpServletRequest request) {

        //MessagePageModel paginationModel = new MessagePageModel();

        Page<Message> messagePage = null;

        try {

            User user = defaultUserDAOService.getUser(String.valueOf(1));

            // Calls the userservice to find user by username (senders user)
            User friend = this.defaultUserDAOService.getUser(String.valueOf(userID));


                messagePage = defaultMessageService.getMessage(user, friend, new PageRequest(page, 20));

                //paginationModel.setMessagesPage()
                  //      .setResponseModel(new ResponseModel(true, "Successful"));

            return new ResponseEntity<>(messagePage, HttpStatus.OK);

        }catch (Exception ex){

            //paginationModel.setResponseModel(new ResponseModel(true, "Ooops! an error occurred"));

            return new ResponseEntity<>(messagePage, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }


}
