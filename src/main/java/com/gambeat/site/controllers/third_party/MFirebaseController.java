package com.gambeat.site.controllers.third_party;

import com.gambeat.site.controllers.restcontroller.jsonmodel.FirebaseModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "api/third-party/firebase")
public class MFirebaseController {

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @ResponseBody
    @RequestMapping(value = "/register/token/{username}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FirebaseModel> registerFirebaseTokenToUser(@PathVariable String username, @RequestBody FirebaseModel firebaseModel, HttpServletRequest request) {

        User user = this.defaultUserDAOService.getByUsername(username);

        user.getFcmToken().add(firebaseModel.getToken());

        this.defaultUserDAOService.updateUser(user);

        return new ResponseEntity<>(firebaseModel.setResponseModel(new ResponseModel(true, "Successful")), HttpStatus.OK); // return response to client.

    }
}
