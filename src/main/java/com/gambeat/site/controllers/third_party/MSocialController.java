package com.gambeat.site.controllers.third_party;


import com.corundumstudio.socketio.SocketIOClient;
import com.gambeat.site.controllers.restcontroller.DashboardController;
import com.gambeat.site.controllers.third_party.jsonmodel.FriendRequestModel;
import com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.third_party.jsonmodel.LoginModel;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.FriendRequest;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultFriendRequestService;
import com.gambeat.site.services.implementation.DefaultUserDAOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.gambeat.GambeatNetworkApplication.server;

@RestController
@RequestMapping(value = "api/third-party/social")
public class MSocialController {

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(MSocialController.class);


    @Autowired
    DefaultFriendRequestService friendRequestService;

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private DefaultFriendRequestService defaultFriendRequestService;




    @ResponseBody
    @RequestMapping(value = "/friends/{page}", method = RequestMethod.GET)
    public ResponseEntity<Page<User>> Login(@PathVariable long page, HttpServletRequest request, HttpServletResponse response) {

        User userz = defaultUserDAOService.getUser(String.valueOf(1));

        Page<User> userPage = new PageImpl(friendRequestService.getFriends(userz));

        return new ResponseEntity<>(userPage, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/friends/2/{page}", method = RequestMethod.GET)
    public ResponseEntity<Page<User>> Login2(@PathVariable int page, HttpServletRequest request, HttpServletResponse response) {

        User userz = defaultUserDAOService.getUser(String.valueOf(1));

        Page<User> userPage = defaultUserDAOService.getFriends(userz, new PageRequest(page, 20));

        return new ResponseEntity<>(userPage, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/friendrequests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FriendRequestModel> FetchFriendRequests(HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = this.defaultUserDAOService.getUser(String.valueOf(1));

            // gets all the friend quests the user has and returns an empty list if the user has no friends
            FriendRequestModel friendRequestsModel = new FriendRequestModel().setFriendRequests(this.defaultFriendRequestService.getAllFriendRequest(user) == null ? Collections.EMPTY_LIST : this.defaultFriendRequestService.getAllFriendRequest(user))
                    .setResponseModel(new ResponseModel(true,"Successful"));


            return new ResponseEntity<>(friendRequestsModel, HttpStatus.OK);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            FriendRequestModel friendRequestsModel = new FriendRequestModel().setFriendRequests(Collections.EMPTY_LIST )
                    .setResponseModel(new ResponseModel(false,"Ooops! an error occurred"));


            return new ResponseEntity<>(friendRequestsModel, HttpStatus.OK);

        }
    }


    @ResponseBody
    @RequestMapping(value = "/friendrequest/send/{username}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> SendRequests(@PathVariable("username") String username, HttpServletRequest request) {


        try {

            User user = new User();

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getByUsername(username);

            // Gets the specific friend requests of both users
            FriendRequest friendRequest = new FriendRequest();

            friendRequest.setFriendRequestStatus(Enum.FriendRequestStatus.PENDING)
                    .setSender(user)
                    .setRecipient(user2)
                    .setDateCreated();

            FriendRequest savedFriendRequest = friendRequestService.save(friendRequest);

            messagingTemplate.convertAndSendToUser(user2.getUserName(),"/topic/friend-request/receive/request", savedFriendRequest);

            if(Objects.nonNull(server)){

                SocketIOClient socketIOClient = server.getAllClients().stream().filter(

                        client -> String.valueOf(client.get("name")).equals(username)

                ).findFirst().get();

                socketIOClient.sendEvent("friend request", savedFriendRequest);
            }

            return new ResponseEntity<>(new ResponseModel(true, "Success"), HttpStatus.OK);


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new ResponseModel(false,"Connection error, please try again!"), HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @ResponseBody
    @RequestMapping(value = "/friendrequest/accept/{username}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> AcceptRequests(@PathVariable("username") String friendUsername, HttpServletRequest request) {


        try {

            User user = defaultUserDAOService.getUser(String.valueOf(1));

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getByUsername(friendUsername);

            // Gets the specific friend requests of both users
            FriendRequest friendRequest = friendRequestService.findFriendRequestByRecipientAndSender(user, user2);

            if(Objects.isNull(friendRequest)){

                return new ResponseEntity<>(new ResponseModel(false, "No request was found for such user"), HttpStatus.NOT_FOUND);

            }


            // checks if the request is pending and if it is sets it to accepted
            if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.PENDING) {

                friendRequest.setFriendRequestStatus(Enum.FriendRequestStatus.ACCEPTED);

                friendRequestService.updateFriendRequest(friendRequest);

                if(user.getOnlineStatus() == Enum.OnlineStatus.ONLINE && user2.getOnlineStatus() == Enum.OnlineStatus.ONLINE ){

                    //messagingTemplate.convertAndSendToUser(user2.getUserName(),"/topic/friend/online", user.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(user, user2, false)));

                    //messagingTemplate.convertAndSendToUser(user.getUserName(),"/topic/friend/online", user2.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(user2, user, false)));

                }


                return new ResponseEntity<>(new ResponseModel(true,"Friend request has been successfully accepted"), HttpStatus.OK);


                // checks if the requests has been accepted  and if it has leaves it as accepted
            } else if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.ACCEPTED) {


                // logs an warning to the database - accepted requests are not supposed to show up here
                LOGGER.warn("Friend request had already been accepted, but is still shown to the user");



                return new ResponseEntity<>(new ResponseModel(true, "Friend request has already been accepted"), HttpStatus.OK);


                // if the request has already been declined it deletes it from the database
            } else {

                // logs an warning to the database - declined requests are supposed to be deleted and not show up here
                LOGGER.warn("Friend request had already been declined, but is still in the database");

                friendRequestService.deleteFriendRequest(friendRequest);

                //responseModel.setIsSuccessful(true);

                //responseModel.setResponseMessage("Friend request had already been declined");

                return new ResponseEntity<>(new ResponseModel(), HttpStatus.OK);

            }


        } catch (Exception ex) { // in case of error the server responds with this

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new ResponseModel(false,ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }



    @ResponseBody
    @RequestMapping(value = "/friendrequest/reject/{username}",
            method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> RejectRequest(@PathVariable("username") String friendUsername, HttpServletRequest request) {


        try {

            User user = defaultUserDAOService.getUser(String.valueOf(1));

            // Calls the userservice to find user by username (senders user)
            User user2 = this.defaultUserDAOService.getByUsername(friendUsername);

            // Gets the specific friend requests of both users
            FriendRequest friendRequest = friendRequestService.findFriendRequestByRecipientAndSender(user, user2);

            if(Objects.isNull(friendRequest)){

                return new ResponseEntity<>(new ResponseModel(false, "No request was found for such user"), HttpStatus.NOT_FOUND);

            }


            // checks if the request is pending and if it is sets it to accepted
            if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.PENDING) {

                friendRequest.setFriendRequestStatus(Enum.FriendRequestStatus.ACCEPTED);

                friendRequestService.updateFriendRequest(friendRequest);

                if(user.getOnlineStatus() == Enum.OnlineStatus.ONLINE && user2.getOnlineStatus() == Enum.OnlineStatus.ONLINE ){

                    //messagingTemplate.convertAndSendToUser(user2.getUserName(),"/topic/friend/online", user.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(user, user2, false)));

                    //messagingTemplate.convertAndSendToUser(user.getUserName(),"/topic/friend/online", user2.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(user2, user, false)));

                }


                return new ResponseEntity<>(new ResponseModel(true,"Friend request has been successfully accepted"), HttpStatus.OK);


                // checks if the requests has been accepted  and if it has leaves it as accepted
            } else if (friendRequest.getFriendRequestStatus() == Enum.FriendRequestStatus.ACCEPTED) {


                // logs an warning to the database - accepted requests are not supposed to show up here
                LOGGER.warn("Friend request had already been accepted, but is still shown to the user");



                return new ResponseEntity<>(new ResponseModel(true, "Friend request has already been accepted"), HttpStatus.OK);


                // if the request has already been declined it deletes it from the database
            } else {

                // logs an warning to the database - declined requests are supposed to be deleted and not show up here
                LOGGER.warn("Friend request had already been declined, but is still in the database");

                friendRequestService.deleteFriendRequest(friendRequest);

                //responseModel.setIsSuccessful(true);

                //responseModel.setResponseMessage("Friend request had already been declined");

                return new ResponseEntity<>(new ResponseModel(), HttpStatus.OK);

            }


        } catch (Exception ex) { // in case of error the server responds with this

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new ResponseModel(false,ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }



}
