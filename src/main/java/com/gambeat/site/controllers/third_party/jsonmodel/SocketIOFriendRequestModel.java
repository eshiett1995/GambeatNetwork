package com.gambeat.site.controllers.third_party.jsonmodel;

import com.gambeat.site.entities.FriendRequest;

public class SocketIOFriendRequestModel {

    public SocketIOFriendRequestModel(){

    }

    public SocketIOFriendRequestModel(FriendRequest friendRequest, ResponseModel responseModel) {

        this.friendRequest = friendRequest;

        this.responseModel = responseModel;
    }

    private FriendRequest friendRequest;

    private ResponseModel responseModel;


    public FriendRequest getFriendRequest() {
        return friendRequest;
    }

    public void setFriendRequest(FriendRequest friendRequest) {
        this.friendRequest = friendRequest;
    }

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public void setResponseModel(ResponseModel responseModel) {
        this.responseModel = responseModel;
    }
}
