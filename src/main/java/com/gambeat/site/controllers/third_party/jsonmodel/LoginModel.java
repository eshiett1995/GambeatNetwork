package com.gambeat.site.controllers.third_party.jsonmodel;

import com.gambeat.site.entities.User;

public class LoginModel {

    public LoginModel(User user, ResponseModel responseModel) {

        this.user = user;

        this.responseModel = responseModel;

    }

    private User user;

    private ResponseModel responseModel;

    public User getUser() {
        return user;
    }

    public LoginModel setUser(User user) {
        this.user = user;
        return this;
    }

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public LoginModel setResponseModel(ResponseModel responseModel) {
        this.responseModel = responseModel;
        return this;
    }
}
