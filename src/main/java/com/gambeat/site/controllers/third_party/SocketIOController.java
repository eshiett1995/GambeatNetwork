package com.gambeat.site.controllers.third_party;


import com.gambeat.site.controllers.restcontroller.jsonmodel.FriendAndMessagesModel;
import com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel;
import com.gambeat.site.controllers.third_party.jsonmodel.SocketIOMessageModel;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.Message;
import com.gambeat.site.entities.Transactions;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.DefaultMessageService;
import com.gambeat.site.services.implementation.DefaultSocketIOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "api/third-party/sockio")
public class SocketIOController {

    @Autowired
    DefaultMessageService defaultMessageService;

    @Autowired
    DefaultSocketIOService defaultSocketIOService;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;


    @ResponseBody
    @RequestMapping(value = "/message", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> sendMessageToSocketIOServer(@RequestBody Message message, HttpServletRequest request, HttpServletResponse response){

        try {

            message.setDateCreated();

            message.setMessageStatus(Enum.MessageStatus.DELIVERED);

           // Message savedMessage = defaultMessageService.save(message);

            //FriendAndMessagesModel friendAndMessagesModel = new FriendAndMessagesModel();

//            friendAndMessagesModel.setFriend(savedMessage.getSender()
  //                  .setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(savedMessage.getSender(), savedMessage.getReceiver(), false)))
    //                .getMessages().add(savedMessage);

//            messagingTemplate.convertAndSendToUser(savedMessage.getReceiver().getUserName(), "/topic/messaging/receive/message", friendAndMessagesModel);

//            messagingTemplate.convertAndSendToUser(savedMessage.getReceiver().getUserName(), "/topic/quick-chat/receive/message", savedMessage);

            defaultSocketIOService.sendMessageToSocketIOServer(new SocketIOMessageModel(message,new ResponseModel(true, "Successful")));

            return new ResponseEntity<>(new ResponseModel(true, "Successful"), HttpStatus.OK);

        }catch (Exception ex){

            return new ResponseEntity<>(new ResponseModel(false, "Unsuccessful"), HttpStatus.OK);

        }
    }


}
