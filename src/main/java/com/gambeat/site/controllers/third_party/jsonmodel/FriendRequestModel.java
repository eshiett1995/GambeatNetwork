package com.gambeat.site.controllers.third_party.jsonmodel;

import com.gambeat.site.entities.FriendRequest;

import java.util.ArrayList;
import java.util.List;

public class FriendRequestModel {

    public FriendRequestModel(){}

    public FriendRequestModel(List<FriendRequest> friendRequests, ResponseModel responseModel){

        this.friendRequests = friendRequests;

        this.responseModel = responseModel;

    }

        private List<FriendRequest> friendRequests = new ArrayList<>();

        private ResponseModel responseModel;

        public List<FriendRequest> getFriendRequests() {
            return friendRequests;
        }

        public FriendRequestModel setFriendRequests(List<FriendRequest> friendRequests) {
            this.friendRequests = friendRequests;
            return this;
        }

        public ResponseModel getResponseModel() {

            return responseModel;

        }

        public FriendRequestModel setResponseModel(ResponseModel responseModel) {

            this.responseModel = responseModel;

            return  this;

        }
}
