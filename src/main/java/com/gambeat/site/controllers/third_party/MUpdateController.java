package com.gambeat.site.controllers.third_party;


import com.gambeat.site.controllers.restcontroller.jsonmodel.ProfileUpdateModel;
import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.entities.TemporaryStore;
import com.gambeat.site.entities.User;
import com.gambeat.site.services.implementation.*;
import com.gambeat.site.utility.email.EmailStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.Objects;

@RestController
@RequestMapping(value = "api/third-party/update")
public class MUpdateController {

    @Autowired
    DefaultUserDAOService defaultUserDAOService;

    @Autowired
    DefaultTokenService defaultTokenService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    DefaultEmailMailNotificationService defaultEmailNotificationService;

    @Autowired
    DefaultTemporaryStoreService defaultTemporaryStoreService;

    @Autowired
    DefaultSmsService defaultSmsService;


    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(MUpdateController.class);

    @ResponseBody
    @RequestMapping(value = "/profile-picture/remove", method = RequestMethod.GET)
    public ResponseEntity<com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel> RemoveProfilePicture(HttpServletRequest request, HttpServletResponse response) {

        try {

            User userz = defaultUserDAOService.getUser(String.valueOf(1));

            userz.setPicture("profile-picture");

            defaultUserDAOService.updateUser(userz);

            return new ResponseEntity<>(new com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel(true, "Successful"), HttpStatus.OK);

        }catch (Exception ex){

            return new ResponseEntity<>(new com.gambeat.site.controllers.third_party.jsonmodel.ResponseModel(false, "Oops!!! Server error."), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/profile-update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> UpdateUserProfile(@RequestBody User usersNewDetails, HttpServletRequest request, HttpServletResponse response) {

        try {

            // Calls the userservice to find user by username
            User user = defaultUserDAOService.getUser(String.valueOf(1));

            user.setFirstName(usersNewDetails.getFirstName())
                    .setLastName(usersNewDetails.getLastName())
                    .setGender(usersNewDetails.getGender())
                    .setCountry(usersNewDetails.getCountry())
                    .setBio(usersNewDetails.getBio());

            defaultUserDAOService.updateUser(user);

            return new ResponseEntity<>(new ResponseModel(true, "Successfully updated"), HttpStatus.OK);


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new ResponseModel(false, "Oops! connection lost, please try again"), HttpStatus.UNAUTHORIZED);
        }

    }


    @RequestMapping(value = "/profile/update/username", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileUpdateModel> UpdateUsername(@RequestBody TemporaryStore temporaryStore, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = defaultUserDAOService.getUser(String.valueOf(1));

            if(user.getUserName().equalsIgnoreCase(temporaryStore.getValue())){

                return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "set username is the user's current user name")), HttpStatus.OK);

            }


            String token = defaultTokenService.generateToken(6);

            if (temporaryStore.getSendVia().equals(Enum.MessageSender.EMAIL)) {

                EmailStatus emailStatus = defaultEmailNotificationService.SendTokenViaEmail(user, token);

            } else {

                defaultSmsService.ComposeChangePasswordTokenSms(user, token);

            }

            temporaryStore.setToken(token)
                    .setUserName(user.getUserName())
                    .setValue(temporaryStore.getValue())
                    .setRequestedUpdate(Enum.RequestedUpdate.USERNAME)
                    .setAttempts(0);


            TemporaryStore savedTemporaryStore = defaultTemporaryStoreService.saveTemporaryStore(temporaryStore);


            return new ResponseEntity<>(new ProfileUpdateModel(savedTemporaryStore.getId(), new ResponseModel(true, "Successful, Token has been generated successfully")), HttpStatus.OK);


        } catch (Exception ex) {


            LOGGER.error(ex.getMessage());


            return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false,"Oops! connection lost, please try again")), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/profile/update/password", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileUpdateModel> UpdatePassword(@RequestBody TemporaryStore temporaryStore, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = defaultUserDAOService.getUser(String.valueOf(1));

            //temporaryStore.setPresentValue(bCryptPasswordEncoder.encode(temporaryStore.getPresentValue()));

            if(!bCryptPasswordEncoder.matches(temporaryStore.getPresentValue(), user.getPassword())){

                return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "Password mismatch, current password does'nt match what the users own")), HttpStatus.OK);

            }else if(bCryptPasswordEncoder.matches(temporaryStore.getValue(), user.getPassword())){

                return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "Current and new Password are the same")), HttpStatus.OK);

            }


            String token = defaultTokenService.generateToken(6);

            if (temporaryStore.getSendVia().equals(Enum.MessageSender.EMAIL)) {

                EmailStatus emailStatus = defaultEmailNotificationService.SendTokenViaEmail(user, token);

            } else {

                defaultSmsService.ComposeChangePasswordTokenSms(user, token);

            }

            temporaryStore.setUserName(user.getUserName())
                    .setValue(bCryptPasswordEncoder.encode(temporaryStore.getValue()))
                    .setToken(token)
                    .setRequestedUpdate(Enum.RequestedUpdate.PASSWORD)
                    .setAttempts(0);

            TemporaryStore savedTemporaryStore = defaultTemporaryStoreService.saveTemporaryStore(temporaryStore);

            return new ResponseEntity<>(new ProfileUpdateModel(savedTemporaryStore.getId(), new ResponseModel(true,"Successful, Token has been generated successfully")), HttpStatus.OK);



        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "Oops! an error has occurred")), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/profile/update/email", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileUpdateModel> UpdateEmail(@RequestBody TemporaryStore temporaryStore, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = defaultUserDAOService.getUser(String.valueOf(1));

            //check if the new mail is the same as the old mail
            if(user.getEmail().equalsIgnoreCase(temporaryStore.getValue())){

                return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "You are already using this mail")), HttpStatus.OK);
            }

            String token = defaultTokenService.generateToken(6);

            TemporaryStore savedTemporaryStore = temporaryStore.setUserName(user.getUserName())
                    .setToken(token)
                    .setRequestedUpdate(Enum.RequestedUpdate.EMAIL)
                    .setSendVia(Enum.MessageSender.SMS)
                    .setAttempts(0);


            //Send SMS token to the client
            defaultSmsService.ComposeChangeEmailTokenSms(user,token);

            defaultTemporaryStoreService.saveTemporaryStore(temporaryStore);

            return new ResponseEntity<>(new ProfileUpdateModel(savedTemporaryStore.getId(), new ResponseModel(true,"Successful, Token has been generated successfully")), HttpStatus.OK);


        } catch (Exception ex) {

            System.out.println(ex.getMessage());

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(false, "Oops! an error occurred")), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/profile/update/phonenumber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfileUpdateModel> UpdatePhoneNumber(@RequestBody TemporaryStore temporaryStore, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = defaultUserDAOService.getUser(String.valueOf(1));

            String token = defaultTokenService.generateToken(6);

            temporaryStore.setUserName(user.getUserName())
                    .setToken(token)
                    .setRequestedUpdate(Enum.RequestedUpdate.PHONENUMBER)
                    .setSendVia(Enum.MessageSender.EMAIL)
                    .setAttempts(0);

            EmailStatus emailStatus = defaultEmailNotificationService.SendTokenViaEmail(user, token);

            TemporaryStore savedTemporaryStore = defaultTemporaryStoreService.saveTemporaryStore(temporaryStore);

            return new ResponseEntity<>(new ProfileUpdateModel(savedTemporaryStore.getId(), new ResponseModel(true,"Successful, Token has been generated successfully" )), HttpStatus.OK);


        } catch (Exception ex) {

            System.out.println(ex.getMessage());

            LOGGER.error(ex.getMessage());

            return new ResponseEntity<>(new ProfileUpdateModel(String.valueOf(0), new ResponseModel(true,"Oops! connection lost, please try again" )), HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/profile/update/token/{tempStoreId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> SwallowToken(@PathVariable("tempStoreId") long tempStoreId, @RequestBody String token, HttpServletRequest request, HttpServletResponse response) {

        try {

            User user = defaultUserDAOService.getUser(String.valueOf(1));

            TemporaryStore temporaryStore = defaultTemporaryStoreService.getTemporaryStoreByIdAndToken(tempStoreId,token);

            if (Objects.isNull(temporaryStore)) {


                return new ResponseEntity<>(new ResponseModel(false, "You have entered an invalid token!"), HttpStatus.OK);

            } else {

                String requestedUpdate;

                if (user.getUserName().equals(temporaryStore.getUserName())) {

                    switch (temporaryStore.getRequestedUpdate()) {

                        case EMAIL:
                            user.setEmail(temporaryStore.getValue());
                            requestedUpdate = "email";
                            break;

                        case PASSWORD:
                            user.setPassword(temporaryStore.getValue());
                            requestedUpdate = "password";
                            break;

                        case USERNAME:
                            user.setUserName(temporaryStore.getValue());
                            requestedUpdate = "username";
                            break;

                        case PHONENUMBER:
                            user.setPhoneNumber(temporaryStore.getValue());
                            requestedUpdate = "phone number";
                            break;

                        default:

                            requestedUpdate = "nothing";
                    }

                    defaultTemporaryStoreService.delete(temporaryStore);

                    defaultUserDAOService.updateUser(user);

                    return new ResponseEntity<>(new ResponseModel(true,"Your " + requestedUpdate + " has been successfully updated!" ), HttpStatus.OK);

                } else {

                    return new ResponseEntity<>(new ResponseModel(false,"You have entered an invalid token!" ), HttpStatus.OK);

                }

            }

        } catch (Exception ex) {

            return new ResponseEntity<>(new ResponseModel(false, "Oops!, a connection error has occurred!"), HttpStatus.OK);

        }

    }


}
