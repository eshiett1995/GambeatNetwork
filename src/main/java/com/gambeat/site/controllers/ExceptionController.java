package com.gambeat.site.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Oto-obong on 25/08/2017.
 */

@Controller
public class ExceptionController {

    @RequestMapping(value = "/400", method = RequestMethod.GET)
    public String BadRequest(HttpServletRequest req) {

        return "page400";
    }

    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public String NotFound(HttpServletRequest req) {

        return "page404";
    }

    @RequestMapping(value = "/500", method = RequestMethod.GET)
    public String InternalServerError(HttpServletRequest req) {

        return "page500";
    }

    @RequestMapping(value = "/505", method = RequestMethod.GET)
    public String HttpVersionNotSupported(HttpServletRequest req) {

        return "error";
    }


}
