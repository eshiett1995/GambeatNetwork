package com.gambeat.site.entities;

public class MatchPair {

    String playerOne;

    String playerTwo;

    String winner;

    public String getPlayerOne() {
        return playerOne;
    }

    public MatchPair setPlayerOne(String playerOne) {
        this.playerOne = playerOne;
        return this;
    }

    public String getPlayerTwo() {
        return playerTwo;
    }

    public MatchPair setPlayerTwo(String playerTwo) {
        this.playerTwo = playerTwo;
        return this;
    }

    public String getWinner() {
        return winner;
    }

    public MatchPair setWinner(String winner) {
        this.winner = winner;
        return this;
    }
}
