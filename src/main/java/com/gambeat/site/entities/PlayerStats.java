package com.gambeat.site.entities;


/**
 * Created by Oto-obong on 29/09/2017.
 */

public class PlayerStats extends DefaultEntity{

    private int matchPlayed;

    private int wins;

    private int loss;

    private double wallet;

    private double profit;

    private double expenses;

    private int totalMessage;

    private int unreadMessage;

    private int readMessage;

    private int referrer;

    private int friends;

    private int referrals;


    public int getMatchPlayed() {
        return matchPlayed;
    }

    public PlayerStats setMatchPlayed(int matchPlayed) {
        this.matchPlayed = matchPlayed;
        return this;
    }

    public int getWins() {
        return wins;
    }

    public PlayerStats setWins(int wins) {
        this.wins = wins;
        return this;
    }

    public int getLoss() {
        return loss;
    }

    public PlayerStats setLoss(int loss) {
        this.loss = loss;
        return this;
    }

    public double getWallet() {
        return wallet;
    }

    public PlayerStats setWallet(double wallet) {
        this.wallet = wallet;
        return this;
    }

    public double getProfit() {
        return profit;
    }

    public PlayerStats setProfit(double profit) {
        this.profit = profit;
        return this;
    }

    public double getExpenses() {
        return expenses;
    }

    public PlayerStats setExpenses(double expenses) {
        this.expenses = expenses;
        return this;
    }

    public int getTotalMessage() {
        return totalMessage;
    }

    public PlayerStats setTotalMessage(int totalMessage) {
        this.totalMessage = totalMessage;
        return this;
    }

    public int getUnreadMessage() {
        return unreadMessage;
    }

    public PlayerStats setUnreadMessage(int unreadMessage) {
        this.unreadMessage = unreadMessage;
        return this;
    }

    public int getReadMessage() {
        return readMessage;
    }

    public PlayerStats setReadMessage(int readMessage) {
        this.readMessage = readMessage;
        return this;
    }

    public int getReferrer() {
        return referrer;
    }

    public PlayerStats setReferrer(int referrer) {
        this.referrer = referrer;
        return this;
    }

    public int getFriends() {
        return friends;
    }

    public PlayerStats setFriends(int friends) {
        this.friends = friends;
        return this;
    }

    public int getReferrals() {
        return referrals;
    }

    public PlayerStats setReferrals(int referrals) {
        this.referrals = referrals;
        return this;
    }


    public PlayerStats(){

        this.matchPlayed = 0;
        this.wins = 0;
        this.loss = 0;
        this.wallet = 0.00;
        this.profit = 0.00;
        this.expenses = 0.00;
        this.totalMessage = 0;
        this.unreadMessage = 0;
        this.readMessage = 0;
        this.referrer = 0;
        this.friends = 0;
        this.referrals = 0;
    }
}
