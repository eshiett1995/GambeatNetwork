package com.gambeat.site.entities;


import com.gambeat.site.utility.annotations.GsonRepellent;

import javax.persistence.*;
import javax.validation.Valid;


public class Transactions extends DefaultEntity {

    public enum TRANSACTIONTYPE{WITHDRAW, DEPOSIT, WIN, LOSS, DEBIT, REVERSAL}

    private User player;

    private double amount;

    private TRANSACTIONTYPE transactiontype;

    private String referenceID;

    private String detail;

    private String token;


    public TRANSACTIONTYPE getTransactiontype() {

        return transactiontype;

    }

    public Transactions setTransactiontype(TRANSACTIONTYPE transactiontype) {

        this.transactiontype = transactiontype;

        return this;
    }



    public double getAmount() {
        return amount;
    }

    public Transactions setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public String getReferenceID() {

        return referenceID;
    }

    public Transactions setReferenceID(String referenceID) {

        this.referenceID = referenceID;

        return this;

    }

    public String getDetail() {

        return detail;

    }

    public Transactions setDetail(String detail) {

        this.detail = detail;

        return this;
    }

    public String getToken() {

        return token;

    }

    public Transactions setToken(String token) {

        this.token = token;

        return this;

    }


    public User getPlayer() {
        return player;
    }

    public Transactions setPlayer(User player) {
        this.player = player;
        return this;
    }
}
