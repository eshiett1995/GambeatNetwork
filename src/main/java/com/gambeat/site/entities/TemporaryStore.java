package com.gambeat.site.entities;


import com.gambeat.site.entities.Enum.*;
import org.springframework.stereotype.Component;

/**
 * Created by Oto-obong on 28/10/2017.
 */

public class TemporaryStore extends DefaultEntity {

    private String userName;

    private String token;

    private RequestedUpdate requestedUpdate;

    private String value;

    private String presentValue;

    private MessageSender sendVia;

    private int attempts;

    public String getUserName() {
        return userName;
    }

    public TemporaryStore setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getToken() {
        return token;
    }

    public TemporaryStore setToken(String token) {
        this.token = token;
        return this;
    }

    public RequestedUpdate getRequestedUpdate() {
        return requestedUpdate;
    }

    public TemporaryStore setRequestedUpdate(RequestedUpdate requestedUpdate) {
        this.requestedUpdate = requestedUpdate;
        return this;
    }

    public String getValue() {
        return value;
    }

    public TemporaryStore setValue(String value) {
        this.value = value;
        return this;
    }

    public MessageSender getSendVia() {
        return sendVia;
    }

    public TemporaryStore setSendVia(MessageSender sendVia) {
        this.sendVia = sendVia;
        return this;
    }

    public int getAttempts() {
        return attempts;
    }

    public TemporaryStore setAttempts(int attempts) {
        this.attempts = attempts;
        return this;
    }

    public String getPresentValue() {
        return presentValue;
    }

    public TemporaryStore setPresentValue(String presentValue) {
        this.presentValue = presentValue;
        return this;
    }
}
