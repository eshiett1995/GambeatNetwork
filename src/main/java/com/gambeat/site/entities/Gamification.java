package com.gambeat.site.entities;



public class Gamification extends DefaultEntity {

    private User contestantOne;

    private User contestantTwo;

    private boolean isRead;

    private Competition competition;

    private String gamificationMessage;

    public User getContestantOne() {

        return contestantOne;

    }

    public Gamification setContestantOne(User contestantOne) {

        this.contestantOne = contestantOne;

        return this;

    }

    public User getContestantTwo() {

        return contestantTwo;
    }

    public Gamification setContestantTwo(User contestantTwo) {

        this.contestantTwo = contestantTwo;

        return this;
    }


    public Competition getCompetition() {
        return competition;
    }

    public Gamification setCompetition(Competition competition) {

        this.competition = competition;

        return this;
    }

    public boolean isRead() {

        return isRead;

    }

    public Gamification setRead(boolean read) {

        isRead = read;

        return this;

    }

    public String getGamificationMessage() {

        return gamificationMessage;

    }

    public Gamification setGamificationMessage(String gamificationMessage) {

        this.gamificationMessage = gamificationMessage;

        return this;
    }
}
