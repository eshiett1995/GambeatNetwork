package com.gambeat.site.entities;


import javax.persistence.Transient;
import javax.validation.Valid;
import com.gambeat.site.entities.Enum.FriendRequestStatus;
import org.springframework.stereotype.Component;

/**
 * Created by Oto-obong on 15/08/2017.
 */

public class FriendRequest extends DefaultEntity {


    private User sender;

    private FriendRequestStatus friendRequestStatus;

    private User recipient;

    @Transient
    private User friend;

    public User getSender() {
        return sender;
    }

    public FriendRequest setSender(User sender) {
        this.sender = sender;
        return this;
    }

    public User getRecipient() {
        return recipient;
    }

    public FriendRequest setRecipient(User recipient) {

        this.recipient = recipient;

        return this;
    }

    public FriendRequestStatus getFriendRequestStatus() {
        return friendRequestStatus;
    }

    public FriendRequest setFriendRequestStatus(FriendRequestStatus friendRequestStatus) {

        this.friendRequestStatus = friendRequestStatus;

        return this;
    }

    public User getFriend() {
        return friend;
    }

    public void setFriend(User friend) {
        this.friend = friend;
    }
}
