package com.gambeat.site.entities;


import com.sun.javafx.binding.StringFormatter;


public class Platform extends DefaultEntity {

    public Platform(){}

    public Platform(String name){

        this.name = name;
    }

    private String name;

    public String getName() {

        return name;

    }

    public Platform setName(String name) {

        this.name = name;

        return this;

    }
}
