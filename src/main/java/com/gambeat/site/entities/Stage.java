package com.gambeat.site.entities;

import java.util.ArrayList;
import java.util.List;

public class Stage {

    private int round;

    private List<MatchPair> pairs = new ArrayList<>();

    private String immunity;

    public int getRound() {

        return round;

    }

    public Stage setRound(int round) {

        this.round = round;

        return this;

    }

    public List<MatchPair> getPairs() {

        return pairs;

    }

    public Stage setPairs(List<MatchPair> pairs) {

        this.pairs = pairs;

        return this;

    }

    public String getImmunity() {

        return immunity;

    }

    public Stage setImmunity(String immunity) {

        this.immunity = immunity;

        return this;

    }
}
