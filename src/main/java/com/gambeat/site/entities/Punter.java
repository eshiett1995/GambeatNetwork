package com.gambeat.site.entities;



/**
 * Created by Oto-obong on 15/08/2017.
 */

public class Punter extends DefaultEntity {


    private User player;

    private boolean isWinner;

    private boolean isEliminated = false;

    private double winAmount;

    private double risk;

    public User getPlayer() {
        return player;
    }

    public Punter setPlayer(User player) {

        this.player = player;

        return this;
    }


    public boolean isWinner() {
        return isWinner;
    }

    public Punter setWinner(boolean winner) {

        isWinner = winner;

        return this;
    }

    public double getRisk() {
        return risk;
    }

    public Punter setRisk(double risk) {

        this.risk = risk;

        return this;
    }

    public boolean isEliminated() {

        return isEliminated;

    }

    public Punter setEliminated(boolean eliminated) {

        isEliminated = eliminated;

        return this;
    }

    public double getWinAmount() {

        return winAmount;

    }

    public Punter setWinAmount(double winAmount) {

        this.winAmount = winAmount;

        return this;
    }
}
