package com.gambeat.site.entities;

import com.gambeat.site.controllers.restcontroller.jsonmodel.ResponseModel;
import com.gambeat.site.validations.Email;
import com.gambeat.site.validations.NotBlank;
import com.gambeat.site.entities.Enum.*;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oto-obong on 15/08/2017.
 */

public class User extends DefaultEntity {


    private String firstName;

    private String lastName;

    private String userName;

    private String email;

    private String password;

    private Gender gender;

    private String country;

    private String picture;

    private String phoneNumber;

    private String bio;

    private OnlineStatus onlineStatus;

    private UserType userType;

    private double money;

    private Boolean isFriend;

    private PlayerStats playerStats;

    private Boolean locked;

    private Boolean activated;

    private int unreadMessagesCount;

    private List<String> fcmToken = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public User setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public Enum.Gender getGender() {
        return gender;
    }

    public User setGender(Enum.Gender gender) {
        this.gender = gender;
        return this;
    }
    

    public String getCountry() {
        return country;
    }

    public User setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getPicture() {
        return picture;
    }

    public User setPicture(String picture) {
        this.picture = picture;
        return this;
    }

    public String getBio() {
        return bio;
    }

    public User setBio(String bio) {
        this.bio = bio;
        return this;
    }

    public Enum.OnlineStatus getOnlineStatus() {
        return onlineStatus;
    }

    public User setOnlineStatus(Enum.OnlineStatus onlineStatus) {
        this.onlineStatus = onlineStatus;
        return this;
    }

    public Enum.UserType getUserType() {
        return userType;
    }

    public User setUserType(Enum.UserType userType) {
        this.userType = userType;
        return this;
    }

    public double getMoney() {
        return money;
    }

    public User setMoney(double money) {
        this.money = money;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public User setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }


    public PlayerStats getPlayerStats() {
        return playerStats;
    }

    public User setPlayerStats(PlayerStats playerStats) {
        this.playerStats = playerStats;
        return this;
    }

    public Boolean getLocked() {
        return locked;
    }

    public User setLocked(Boolean locked) {

        this.locked = locked;

        return this;
    }


    public Boolean getActivated() {
        return activated;
    }

    public User setActivated(Boolean activated) {

        this.activated = activated;

        return this;
    }

    public int getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public User setUnreadMessagesCount(int unreadMessagesCount) {
        this.unreadMessagesCount = unreadMessagesCount;
        return this;
    }

    public Boolean getFriend() {
        return isFriend;
    }

    public User setFriend(Boolean friend) {
        isFriend = friend;
        return this;
    }

    public List<String> getFcmToken() {
        return fcmToken;
    }

    public User setFcmToken(List<String> fcmToken) {

        this.fcmToken = fcmToken;

        return this;
    }
}
