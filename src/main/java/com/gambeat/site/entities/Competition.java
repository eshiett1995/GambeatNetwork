package com.gambeat.site.entities;

import java.util.ArrayList;
import java.util.List;


public class Competition extends DefaultEntity {

    private String competitionName;

    private Game game;

    private List<Punter> punters = new ArrayList<>();

    private List<Punter> winners = new ArrayList<>();

    private double winningAmount;

    private Enum.CompetitionStatus competitionStatus;

    private double entryFee;

    private List<Stage> stages = new ArrayList<>();

    public String getCompetitionName() {
        return competitionName;
    }

    public Competition setCompetitionName(String competitionName) {

        this.competitionName = competitionName;

        return this;

    }

    public Game getGame() {

        return game;

    }

    public Competition setGame(Game game) {

        this.game = game;

        return this;

    }

    public List<Punter> getPunters() {
        return punters;
    }

    public Competition setPunters(List<Punter> punters) {

        this.punters = punters;

        return this;
    }

    public List<Punter> getWinners() {
        return winners;
    }

    public Competition setWinners(List<Punter> winners) {

        this.winners = winners;

        return this;
    }

    public List<Punter> setWinners() {

        return this.winners;
    }


    public double getWinningAmount() {

        return winningAmount;

    }

    public Competition setWinningAmount(double winningAmount) {

        this.winningAmount = winningAmount;

        return this;

    }

    public double getEntryFee() {
        return entryFee;
    }

    public Competition setEntryFee(double entryFee) {
        this.entryFee = entryFee;
        return this;
    }

    public Enum.CompetitionStatus getCompetitionStatus() {
        return competitionStatus;
    }

    public Competition setCompetitionStatus(Enum.CompetitionStatus competitionStatus) {

        this.competitionStatus = competitionStatus;

        return this;
    }

    public List<Stage> getStages() {

        return stages;

    }

    public Competition setStages(List<Stage> stages) {

        this.stages = stages;

        return this;

    }

}
