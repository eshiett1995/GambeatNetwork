package com.gambeat.site.entities;


import org.springframework.data.annotation.Id;

public class Activations extends DefaultEntity {

    public User gambeat_user;

    public String activationToken;

    public Activations(User user, String activationToken) {
        this.gambeat_user = user;
        this.activationToken = activationToken;
    }

    public User getGambeat_user() {
        return gambeat_user;
    }

    public Activations setGambeat_user(User gambeat_user) {
        this.gambeat_user = gambeat_user;
        return this;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public Activations setActivationToken(String activationToken) {

        this.activationToken = activationToken;
        return this;

    }
}
