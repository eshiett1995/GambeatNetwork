package com.gambeat.site.entities;



public class PlatformLink extends DefaultEntity {

    private Platform platform;

    private String url;

    public Platform getPlatform() {

        return platform;

    }

    public PlatformLink setPlatform(Platform platform) {

        this.platform = platform;

        return this;

    }

    public String getUrl() {

        return url;

    }

    public PlatformLink setUrl(String url) {

        this.url = url;

        return this;

    }


}

