package com.gambeat.site.entities;


public class DownloadLink extends DefaultEntity{

    private Platform platform;

    private String url;

    public Platform getPlatform() {

        return platform;

    }

    public DownloadLink setPlatform(Platform platform) {

        this.platform = platform;

        return this;

    }

    public String getUrl() {

        return url;

    }

    public DownloadLink setUrl(String url) {

        this.url = url;

        return this;

    }

}
