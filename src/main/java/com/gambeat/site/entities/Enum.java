package com.gambeat.site.entities;

/**
 * Created by Oto-obong on 15/08/2017.
 */
public class Enum {

    public enum Gender
    {
        MALE,
        FEMALE,
        UNSPECIFIED
    }

    public enum OnlineStatus
    {
        ONLINE,
        OFFLINE
    }


    public enum UserType
    {
        PLAYER,
        CUSTOMERCARE
    }

    public enum Role
    {
        ROLE_USER,

        ROLE_CUSTOMERCARE,

        ROLE_ADMIN
    }


    public enum MessageStatus
    {
        UNDELIVERED,
        DELIVERED,
        FAILED,
        UNREAD,
        READ
    }

    public enum FriendRequestStatus
    {
        PENDING,
        ACCEPTED,
        DECLINED
    }

    public enum RequestedUpdate
    {
        USERNAME,
        PASSWORD,
        EMAIL,
        PHONENUMBER
    }

    public enum MessageSender
    {
        EMAIL,
        SMS
    }

    public enum RedirectPage
    {
        WELCOMEPAGE,
        OTHERS,
        LOCKEDPAGE
    }

    public enum CompetitionType
    {
        ELIMINATION,
        FREEFORALL,
        ROYALRUMBLE,
    }

    public enum CompetitionStatus
    {
        STARTED,
        OPEN,
        ENDED,
    }

    public enum MatchStatus
    {
        STARTED,
        OPEN,
        ENDED,
    }

    public enum NotificationType
    {
        FRIENDREQUESTACCEPTED,
        FRIENDREQUESTGOTTEN,
        PLAYERKNOCKEDOUT,
        TRANSACTION,
        PLAYERWINSMATCH,
        TOURNAMENTSTARTED,
        FRIENDSMATCH,
        TOURNAMENTWINNER,
        TOURNAMENTCOULDNOTSTART,
        TOURNAMENTENDED,
        FRIENDELIMINATED,
    }


}
