package com.gambeat.site.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class Game extends DefaultEntity {

    private String name;

    private List<Genre> Genres = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private Enum.CompetitionType competitionType;

    private String description;

    private List<Platform> platforms = new ArrayList<>();

    private List<DownloadLink> downloadLinks = new ArrayList<>();

    private List<PlatformLink> platformLinks = new ArrayList<>();

    private String createCompetitionUrl;

    private String joinCompetitionUrl;

    public String getName() {

        return name;

    }

    public Game setName(String name) {

        this.name = name;

        return this;

    }

    public List<Genre> getGenres() {

        return Genres;

    }

    public Game setGenres(List<Genre> genres) {

        Genres = genres;

        return this;

    }

    public Enum.CompetitionType getCompetitionType() {

        return competitionType;

    }

    public Game setCompetitionType(Enum.CompetitionType competitionType) {

        this.competitionType = competitionType;

        return this;

    }

    public String getDescription() {

        return description;

    }

    public Game setDescription(String description) {

        this.description = description;

        return this;

    }

    public List<Platform> getPlatforms() {

        return platforms;

    }

    public Game setPlatforms(List<Platform> platforms) {

        this.platforms = platforms;

        return this;

    }

    public List<DownloadLink> getDownloadLinks() {

        return downloadLinks;

    }

    public Game setDownloadLinks(List<DownloadLink> downloadLinks) {

        this.downloadLinks = downloadLinks;

        return this;

    }

    public List<PlatformLink> getPlatformLinks() {
        return platformLinks;
    }

    public void setPlatformLinks(List<PlatformLink> platformLinks) {
        this.platformLinks = platformLinks;
    }

    public String getCreateCompetitionUrl() {
        return createCompetitionUrl;
    }

    public void setCreateCompetitionUrl(String createCompetitionUrl) {
        this.createCompetitionUrl = createCompetitionUrl;
    }

    public String getJoinCompetitionUrl() {
        return joinCompetitionUrl;
    }

    public void setJoinCompetitionUrl(String joinCompetitionUrl) {
        this.joinCompetitionUrl = joinCompetitionUrl;
    }
}
