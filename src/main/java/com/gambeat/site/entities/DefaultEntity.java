package com.gambeat.site.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.gambeat.site.utility.CustomerDateAndTimeDeserializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Oto-obong on 15/08/2017.
 */

public class DefaultEntity implements Serializable {

    @Id
    private String id;

    private Date dateCreated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {

            dateCreated = dateFormat.parse(dateFormat.format(new Date()));



        } catch (ParseException e) {

            e.printStackTrace();

        }

    }
}
