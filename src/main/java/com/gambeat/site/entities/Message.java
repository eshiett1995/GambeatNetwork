package com.gambeat.site.entities;


import com.gambeat.site.entities.Enum.*;

/**
 * Created by Oto-obong on 15/08/2017.
 */

public class Message extends DefaultEntity {


    private User sender;

    private User receiver;


    private String message;

    private MessageStatus messageStatus;

    private Boolean isRead;


    public User getSender() {
        return sender;
    }

    public Message setSender(User sender) {

        this.sender = sender;

        return this;
    }

    public User getReceiver() {
        return receiver;
    }

    public Message setReceiver(User receiver) {

        this.receiver = receiver;

        return this;
    }

    public String getMessage() {
        return message;
    }

    public Message setMessage(String message) {

        this.message = message;

        return this;
    }

    public MessageStatus getMessageStatus() {
        return messageStatus;
    }

    public Message setMessageStatus(MessageStatus messageStatus) {

        this.messageStatus = messageStatus;

        return this;
    }

    public Boolean getRead() {
        return isRead;
    }

    public Message setRead(Boolean read) {

        isRead = read;

        return this;
    }

}
