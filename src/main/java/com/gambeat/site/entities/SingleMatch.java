package com.gambeat.site.entities;

import java.util.ArrayList;
import java.util.List;

public class SingleMatch extends DefaultEntity {

    private String matchEntryKey;

    private Game game;

    private List<Punter> punters = new ArrayList<>();

    private List<Punter> winners = new ArrayList<>();

    private double winningAmount;

    private Enum.CompetitionStatus competitionStatus;

    private Boolean isCustom;

    private long competitorsRange;

    private double entryFee;

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<Punter> getPunters() {
        return punters;
    }

    public void setPunters(List<Punter> punters) {
        this.punters = punters;
    }

    public List<Punter> getWinners() {
        return winners;
    }

    public void setWinners(List<Punter> winners) {
        this.winners = winners;
    }

    public double getWinningAmount() {
        return winningAmount;
    }

    public void setWinningAmount(double winningAmount) {
        this.winningAmount = winningAmount;
    }

    public Enum.CompetitionStatus getCompetitionStatus() {
        return competitionStatus;
    }

    public void setCompetitionStatus(Enum.CompetitionStatus competitionStatus) {
        this.competitionStatus = competitionStatus;
    }

    public double getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(double entryFee) {
        this.entryFee = entryFee;
    }
}
