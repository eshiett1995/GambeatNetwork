package com.gambeat.site.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class Notifications extends DefaultEntity {

    private User user;

    private String notificationHeader;

    private String notificationMessage;

    private Enum.NotificationType notificationType;

    private User reference;

    private Competition competition;

    private boolean isRead;

    public User getUser() {

        return user;

    }

    public Notifications setUser(User user) {

        this.user = user;

        return this;

    }

    public String getNotificationHeader() {

        return notificationHeader;

    }

    public Notifications setNotificationHeader(String notificationHeader) {

        this.notificationHeader = notificationHeader;

        return this;

    }

    public String getNotificationMessage() {

        return notificationMessage;

    }

    public Notifications setNotificationMessage(String notificationMessage) {

        this.notificationMessage = notificationMessage;

        return this;

    }

    public Enum.NotificationType getNotificationType() {

        return notificationType;

    }

    public Notifications setNotificationType(Enum.NotificationType notificationType) {

        this.notificationType = notificationType;

        return this;

    }

    public boolean isRead() {

        return isRead;

    }

    public Notifications setRead(boolean read) {

        isRead = read;

        return this;
    }

    public User getReference() {
        return reference;
    }

    public Notifications setReference(User reference) {

        this.reference = reference;

        return this;

    }

    public Competition getCompetition() {
        return competition;
    }

    public Notifications setCompetition(Competition competition) {

        this.competition = competition;

        return this;

    }
}
