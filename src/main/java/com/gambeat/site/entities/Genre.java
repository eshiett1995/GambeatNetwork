package com.gambeat.site.entities;


public class Genre extends DefaultEntity {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
