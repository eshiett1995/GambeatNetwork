package com.gambeat.site.models;

public class GenerateStageStrategy {

    public GenerateStageStrategy(long columnSize, long rowSize, long startNumber, long endNumber, boolean isDecimal) {
        this.columnSize = columnSize;
        this.rowSize = rowSize;
        this.startNumber = startNumber;
        this.endNumber = endNumber;
        this.isDecimal = isDecimal;
    }

    private long columnSize;
    private long rowSize;
    private long startNumber;
    private long endNumber;
    private boolean isDecimal;

    public long getColumnSize() {
        return columnSize;
    }

    public void setColumnSize(long columnSize) {
        this.columnSize = columnSize;
    }

    public long getRowSize() {
        return rowSize;
    }

    public void setRowSize(long rowSize) {
        this.rowSize = rowSize;
    }

    public long getStartNumber() {
        return startNumber;
    }

    public void setStartNumber(long startNumber) {
        this.startNumber = startNumber;
    }

    public long getEndNumber() {
        return endNumber;
    }

    public void setEndNumber(long endNumber) {
        this.endNumber = endNumber;
    }

    public boolean isDecimal() {
        return isDecimal;
    }

    public void setDecimal(boolean decimal) {
        isDecimal = decimal;
    }
}
