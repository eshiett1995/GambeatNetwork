package com.gambeat;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.corundumstudio.socketio.protocol.JacksonJsonSupport;
import com.gambeat.site.controllers.restcontroller.WelcomeController;
import com.gambeat.site.controllers.restcontroller.jsonmodel.FriendAndMessagesModel;
import com.gambeat.site.entities.*;
import com.gambeat.site.entities.Enum;
import com.gambeat.site.exceptions.registrar.CustomErrorPageRegistrar;
import com.gambeat.site.pushnotification.firebase.models.response.FirebaseResponseModel;
import com.gambeat.site.pushnotification.firebase.service.DefaultFirebaseService;
import com.gambeat.site.services.implementation.*;
import com.gambeat.site.websocket.socketIO.models.SocketIOLogin;
import com.twilio.Twilio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.ErrorPageRegistrar;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.*;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import retrofit2.Response;

import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Objects;


@Configuration
@EnableAsync(proxyTargetClass = true)
@PropertySource(value = {"classpath:application.properties"})
@SpringBootApplication(scanBasePackages={"com"},exclude = {ErrorMvcAutoConfiguration.class,SecurityAutoConfiguration.class })
public class GambeatNetworkApplication{

    @Autowired
    DefaultUserDAOService defaultUserDAOService;


    @Autowired
    DefaultTransactionService transactionService;

    @Autowired
    DefaultTemporaryStoreService temporaryStoreService;

    @Autowired
	DefaultFriendRequestService defaultFriendRequestService;

    @Autowired
	DefaultMessageService defaultMessageService;

    @Autowired
	SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    DefaultPlatformService defaultPlatformService;

    @Autowired
    DefaultGameService defaultGameService;


    public static SocketIOServer server =  null;

	private static final Logger LOGGER = LoggerFactory.getLogger(WelcomeController.class);

	private final static String ACCOUNT_SID = "ACdd8844832e7a146b31e59ab3ab4bf52e";
	private final static String AUTH_ID = "e0e9cbc64817179f8533d9ebc84536f1";

	static {
		Twilio.init(ACCOUNT_SID, AUTH_ID);
	}


	@Bean(name="processExecutor")
	public TaskExecutor workExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setThreadNamePrefix("Async-");
		threadPoolTaskExecutor.setCorePoolSize(3);
		threadPoolTaskExecutor.setMaxPoolSize(3);
		threadPoolTaskExecutor.setQueueCapacity(600);
		threadPoolTaskExecutor.afterPropertiesSet();
		LOGGER.info("ThreadPoolTaskExecutor set");
		return threadPoolTaskExecutor;
	}


	public static void main(String[] args) {

		SpringApplication.run(GambeatNetworkApplication.class, args);



    }


	/**@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("static/");
		resolver.setSuffix(".html");
		return resolver;
	} **/


	@Bean
	public CommandLineRunner loadData() {
		return (args) -> {







			com.corundumstudio.socketio.Configuration config = new com.corundumstudio.socketio.Configuration();
			config.setHostname("localhost");
			config.setJsonSupport(new JacksonJsonSupport());
			config.setPort(9090);

			server = new SocketIOServer(config);

			server.addConnectListener(socketIOClient -> {

				System.out.println("Client connected");

			});

			server.addEventListener("join chat server", SocketIOLogin.class, new DataListener<SocketIOLogin>() {
				@Override
				public void onData(SocketIOClient client, SocketIOLogin data, AckRequest ackRequest) {

					System.out.println(data.getUsername() + " Someone Joined chat server");

					client.set("name", data.getUsername());
				}
			});

			/**
			 * Message coming in from the mobile app
			 */
			server.addEventListener("sending message", Message.class, new DataListener<Message>() {
				@Override
				public void onData(SocketIOClient client, Message message, AckRequest ackRequest) {

					User sender =  defaultUserDAOService.getUser(message.getSender().getId());

					User recipient = defaultUserDAOService.getUser(message.getReceiver().getId());

					message.setSender(sender).setReceiver(recipient);

					message.setDateCreated();

					message.setMessageStatus(Enum.MessageStatus.DELIVERED);

					Message savedMessage = defaultMessageService.save(message);

					FriendAndMessagesModel friendAndMessagesModel = new FriendAndMessagesModel();

					friendAndMessagesModel.setFriend(savedMessage.getSender()
							.setUnreadMessagesCount(defaultMessageService.countUnreadMessagesFromSender(savedMessage.getSender(), savedMessage.getReceiver(), false)))
							.getMessages().add(savedMessage);

					simpMessagingTemplate.convertAndSendToUser(recipient.getUserName(),"/topic/messaging/receive/message", friendAndMessagesModel);

					simpMessagingTemplate.convertAndSendToUser(recipient.getUserName(),"/topic/quick-chat/receive/message", savedMessage);

					if(Objects.nonNull(server)){

						SocketIOClient foundClient = server.getAllClients().stream().filter(

								socketIOClient -> String.valueOf(socketIOClient.get("name")).equals(recipient.getUserName())

						).findFirst().get();

						foundClient.sendEvent("receive message", savedMessage);
					}

				}
			});



			/**
			 * Message coming in from the mobile app
			 */
			server.addEventListener("message is read", Message.class, (client, message, ackRequest) -> {

				Message foundMessage = defaultMessageService.findById(message.getId());

				foundMessage.setRead(true);

                Message updatedMessage = defaultMessageService.update(message);

				simpMessagingTemplate.convertAndSendToUser(message.getSender().getUserName(),"/topic/quick-chat/sent/message/seen", updatedMessage);

                if(Objects.nonNull(server)){

                    SocketIOClient foundClient = server.getAllClients().stream().filter(

                            socketIOClient -> String.valueOf(socketIOClient.get("name")).equals(updatedMessage.getSender().getUserName())

                    ).findFirst().get();

                    foundClient.sendEvent("sent message seen", updatedMessage);
                }

            });


			server.addEventListener("message delivered", Message.class, (client, message, ackRequest) -> {

				Message foundMessage = defaultMessageService.findById(message.getId());

				foundMessage.setMessageStatus(Enum.MessageStatus.DELIVERED);

				Message updatedMessage = defaultMessageService.update(message);

				simpMessagingTemplate.convertAndSendToUser(message.getSender().getUserName(),"/topic/quick-chat/sent/message/delivered", updatedMessage);

				if(Objects.nonNull(server)){

					SocketIOClient foundClient = server.getAllClients().stream().filter(

							socketIOClient -> String.valueOf(socketIOClient.get("name")).equals(updatedMessage.getSender().getUserName())

					).findFirst().get();

					foundClient.sendEvent("sent message delivered", updatedMessage);
				}

			});



			server.addDisconnectListener(socketIOClient -> {

            });


			server.start();

			try {

				Thread.sleep(Integer.MAX_VALUE);

			} catch (InterruptedException e) {

				e.printStackTrace();

			}

			server.stop();






			Platform iosPlatform = new Platform().setName("IOS");

			Platform androidPlatform = new Platform().setName("Android");

			defaultPlatformService.save(iosPlatform);

			defaultPlatformService.save(androidPlatform);





			Game game = new Game();

			game.setName("Tappy Bird");

			game.setCompetitionType(Enum.CompetitionType.ELIMINATION);

			game.setCreateCompetitionUrl("http://localhost:5000/third-party/register/competition");

			game.setDescription(
					"Tappy bird is a remake of the popular mobile game " + "\"Flappy Bird\" Created by Dong Nguyen." + "The game is " +
							"details are about using a bird to hoop over obstacles and increasing your high score, in a situation where " +
							"you hit and obstacle or you touch the ground, the bird dies."
			);

			game.setJoinCompetitionUrl("http://localhost:5000/third-party/register/competition");

			game.getPlatforms().add(defaultPlatformService.findPlatformByName("IOS"));

			game.getPlatforms().add(defaultPlatformService.findPlatformByName("Android"));

			game.getPlatformLinks().add(new PlatformLink().setPlatform(
			        defaultPlatformService.findPlatformByName("Android"))
                    .setUrl("com.gambeat.flappybird")
            );



			defaultGameService.save(game);





			User user = new User();

			user.setPassword("iam2fresh")
					.setUserType(Enum.UserType.PLAYER)
					.setPicture("eshiett1995")
					.setFirstName("Oto-obong")
                    .setPicture("eshiett.jpeg")
					.setCountry("Nigeria")
					.setLocked(false)
					.setOnlineStatus(Enum.OnlineStatus.OFFLINE)
					.setBio("Just doing it")
					.setEmail("eshiett1995@gmail.com")
					.setLastName("Eshiett")
					.setGender(Enum.Gender.MALE)
                    .setMoney(99000)
					.setUserName("eshiett1995")
					.setActivated(true)
					.setPhoneNumber("08098516969");
                    //PlayerStats stats2 = new PlayerStats().setWallet(99000);
                    //stats2.setDateCreated();
					//user.setPlayerStats(stats2)
					//.setDateCreated();

			User savedone = defaultUserDAOService.saveUser(user);

			User user2 = new User();

			user2.setPassword("iam2fresh")
					.setUserType(Enum.UserType.PLAYER)
					.setPicture("eshiett1994")
					.setFirstName("Mfoniso")
					.setCountry("Nigeria")
                    .setPicture("eshiett.jpeg")
					.setLocked(false)
					.setOnlineStatus(Enum.OnlineStatus.OFFLINE)
					.setBio("Just doing it")
					.setEmail("eshiett1994@gmail.com")
					.setLastName("Eshiett")
					.setGender(Enum.Gender.MALE)
					.setMoney(99000)
					.setUserName("eshiett1994")
					.setActivated(true)
					.setPhoneNumber("08023179774");
					PlayerStats stats = new PlayerStats().setWallet(99000);
					stats.setDateCreated();
					user2.setPlayerStats(stats)
					.setDateCreated();

			User savedone2 = defaultUserDAOService.saveUser(user2);

			User user3 = new User();

			user3.setPassword("iam2cool")
					.setUserType(Enum.UserType.PLAYER)
					.setPicture("eshiett1994")
					.setFirstName("Andrew")
					.setCountry("Nigeria")
					.setPicture("eshiett.jpeg")
					.setLocked(false)
					.setOnlineStatus(Enum.OnlineStatus.OFFLINE)
					.setBio("Just doing it")
					.setEmail("andrew@gmail.com")
					.setLastName("Okesokun")
					.setGender(Enum.Gender.MALE)
					.setMoney(99000)
					.setUserName("mr_andys")
					.setActivated(true)
					.setPhoneNumber("08023179772");
			PlayerStats stats3 = new PlayerStats().setWallet(99000);
			stats3.setDateCreated();
			user3.setPlayerStats(stats3)
					.setDateCreated();

			User savedone3 = defaultUserDAOService.saveUser(user3);


            User user4 = new User();

            user4.setPassword("iam2cool")
                    .setUserType(Enum.UserType.PLAYER)
                    .setPicture("eshiett1994")
                    .setFirstName("Samuel")
                    .setCountry("Nigeria")
                    .setPicture("eshiett.jpeg")
                    .setLocked(false)
                    .setOnlineStatus(Enum.OnlineStatus.ONLINE)
                    .setBio("Just doing it")
                    .setEmail("sam@gmail.com")
                    .setLastName("Dickley")
                    .setGender(Enum.Gender.MALE)
                    .setMoney(99000)
                    .setUserName("samzzz")
                    .setActivated(true)
                    .setPhoneNumber("08023079776");
            PlayerStats stats4 = new PlayerStats().setWallet(99000);
            stats3.setDateCreated();
            user4.setPlayerStats(stats4)
                    .setDateCreated();

            User savedone4 = defaultUserDAOService.saveUser(user4);



			User user5 = new User();

			user5.setPassword("cooleer")
					.setUserType(Enum.UserType.PLAYER)
					.setPicture("eshiett1994")
					.setFirstName("Dongz")
					.setCountry("Nigeria")
					.setPicture("eshiett.jpeg")
					.setLocked(false)
					.setOnlineStatus(Enum.OnlineStatus.ONLINE)
					.setBio("Just doing it")
					.setEmail("dongo@gmail.com")
					.setLastName("Dongo")
					.setGender(Enum.Gender.MALE)
					.setMoney(99000)
					.setUserName("dongo")
					.setActivated(true)
					.setPhoneNumber("08023079376");
			PlayerStats stats5 = new PlayerStats().setWallet(99000);
			stats5.setDateCreated();
			user5.setPlayerStats(stats5)
					.setDateCreated();

			User savedone5 = defaultUserDAOService.saveUser(user5);

			FriendRequest friendRequest5 = new FriendRequest();

			friendRequest5.setSender(savedone5)
					.setRecipient(savedone)
					.setFriendRequestStatus(Enum.FriendRequestStatus.PENDING);
			friendRequest5.setDateCreated();

			defaultFriendRequestService.save(friendRequest5);


			FriendRequest friendRequest = new FriendRequest();

			friendRequest.setSender(savedone)
					.setRecipient(savedone2)
					.setFriendRequestStatus(Enum.FriendRequestStatus.PENDING);
			friendRequest.setDateCreated();

			defaultFriendRequestService.save(friendRequest);

            FriendRequest friendRequest2 = new FriendRequest();

            friendRequest2.setSender(savedone)
                    .setRecipient(savedone3)
                    .setFriendRequestStatus(Enum.FriendRequestStatus.PENDING);
            friendRequest2.setDateCreated();

            defaultFriendRequestService.save(friendRequest2);


            FriendRequest friendRequest3 = new FriendRequest();

            friendRequest3.setSender(savedone4)
                    .setRecipient(savedone)
                    .setFriendRequestStatus(Enum.FriendRequestStatus.ACCEPTED);
            friendRequest3.setDateCreated();

            defaultFriendRequestService.save(friendRequest3);


            Transactions transactions = new Transactions();

			transactions.setPlayer(savedone)
                    .setToken("xxxxx")
                    .setAmount(5000)
                    .setTransactiontype(Transactions.TRANSACTIONTYPE.DEPOSIT)
                    .setDetail("normal depo")
                    .setReferenceID("efefef");

			transactions.setDateCreated();

			transactionService.save(transactions);

			Message message = new Message();
			message.setReceiver(savedone)
					.setMessageStatus(Enum.MessageStatus.DELIVERED)
					.setSender(savedone2)
                    .setRead(false)
					.setMessage("cool")
					.setDateCreated();

            defaultMessageService.save(message);

            Message message2 = new Message();
            message2.setReceiver(savedone3)
                    .setMessageStatus(Enum.MessageStatus.DELIVERED)
                    .setSender(savedone)
                    .setRead(false)
                    .setMessage("hello Mr Andz")
                    .setDateCreated();

            defaultMessageService.save(message2);

            Message message2b = new Message();
            message2b.setReceiver(savedone)
                    .setMessageStatus(Enum.MessageStatus.DELIVERED)
                    .setSender(savedone3)
                    .setRead(false)
                    .setMessage("hello Mr Samuel")
                    .setDateCreated();

            defaultMessageService.save(message2b);

            Message message3 = new Message();
            message3.setReceiver(savedone)
                    .setMessageStatus(Enum.MessageStatus.DELIVERED)
                    .setSender(savedone4)
                    .setRead(false)
                    .setMessage("hello oto, you are to sexy to be sitting all by yoursef, lemme help you and yake all the pain away. i love uo a" +

					"and want to panshh you, have you heard or are u just hearing me, you better here ooo baba")
                    .setDateCreated();

            defaultMessageService.save(message3);



            TemporaryStore temporaryStore = new TemporaryStore();

            temporaryStore.setValue("xxxx")
                    .setPresentValue("xxxxxx")
                    .setToken("token")
                    .setRequestedUpdate(Enum.RequestedUpdate.EMAIL)
                    .setSendVia(Enum.MessageSender.EMAIL)
                    .setAttempts(0)
                    .setUserName("eshiett1995");

            temporaryStore.setDateCreated();


            TemporaryStore temporaryStore2 = new TemporaryStore();

            temporaryStore2.setValue("xxxx")
                    .setPresentValue("xxxxxx")
                    .setToken("token")
                    .setRequestedUpdate(Enum.RequestedUpdate.EMAIL)
                    .setSendVia(Enum.MessageSender.EMAIL)
                    .setAttempts(0)
                    .setUserName("eshiett1995");

            temporaryStore.setDateCreated();

            temporaryStoreService.saveTemporaryStore(temporaryStore);


            temporaryStoreService.saveTemporaryStore(temporaryStore2);


		};
	}

	@Bean
	@Primary
	@ConfigurationProperties("spring.data.mongodb")
	public DataSourceProperties mainDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	@Primary
	@ConfigurationProperties("spring.data.mongodb")
	public DataSource mainDataSource() {
		return mainDataSourceProperties().initializeDataSourceBuilder().build();
	}




	@Bean
	@ConfigurationProperties("session.datasource")
	public EmbeddedDatabase sessionDataSource() {
		return new EmbeddedDatabaseBuilder()
				.setType(EmbeddedDatabaseType.H2)
				.addScript("org/springframework/session/jdbc/schema-h2.sql").build();
	}


	@Bean(name = "springSessionJdbcOperations")
	public JdbcTemplate springSessionJdbcOperations(@Qualifier("sessionDataSource") DataSource secondaryDataSource) {

		return new JdbcTemplate(secondaryDataSource);
	}


	@Bean
	public ErrorPageRegistrar errorPageRegistrar(){
		return new CustomErrorPageRegistrar();
	}



	@Bean//(name = "commonsMultipartResolver")
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}


	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();

		factory.setMaxFileSize("1MB");
		factory.setMaxRequestSize("1MB");

		return factory.createMultipartConfig();
	}


	@Bean
	public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory,
									   MongoMappingContext context) {

		MappingMongoConverter converter =
				new MappingMongoConverter(new DefaultDbRefResolver(mongoDbFactory), context);
		converter.setTypeMapper(new DefaultMongoTypeMapper(null));

		MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory, converter);

		return mongoTemplate;

	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}


}
