/**
 * Created by Oto-obong on 09/09/2017.
 */

var app = angular.module('WelcomePageApp',[]);

app.controller('WelcomePage',function($scope,$http,swithToIndexUrl) {

    // initializes the User model
    $scope.User = {};
    // show sign up for on start of the welcome page
    $scope.showSignupForm = true;
    // shows success form when the sign up is successful, it shares the same modal box with the sign uo form.
    $scope.showSuccessForm = false;
    // The header of the signup form, it can be changed depending on the response the server sends to the user, it changes to "Success" on a successful login.
    $scope.signupModalTitle = 'Sign Up'
    // This is the second header message that notifies the user
    $scope.signUpBoxMsg = 'Sign Up-It\'s free';
    // This changes the background color of the buttons and the header to represent failure/ error notifications or Success
    $scope.backgroundColor = 'green';
    // This is the message shown on the Sign up button
    $scope.signupBtnMsg = 'Sign Up';
    // This is the message shown on the Login button
    $scope.loginBtnMsg = 'Log In';


    $scope.LoginUser = function (user) {

        $http.post(base_url + 'welcome/login', JSON.stringify(user))

            .then(
                function(response){
                    // success callback

                    if(response.data.isSuccessful) {

                        $scope.test = swithToIndexUrl.switch();

                    }else {


                    }

                },
                function(response){
                    // failure callback


                }
            );

    };



    $scope.backToMenu = function () {

        $scope.showSignupForm = true;

        $scope.showSuccessForm = false;

        $scope.signupModalTitle = 'Sign Up'

        $scope.signUpBoxMsg = 'Sign Up-It\'s free';

        $scope.backgroundColor = 'green';

        $scope.signupBtnMsg = 'Sign Up';

        $scope.User = "";

        $scope.User = {};

    }




    $scope.AddUser = function (user) {

        user.userType = 0; // sets the user type to player
        user.bio = '';
        user.picture = 'generic-avatar.jpg';
        user.DateCreated = new Date();



        if(user.password !== user.confirmPassword){

            $scope.backgroundColor = 'red';

            $scope.signUpBoxMsg = 'Inconsistent password';

            $scope.signupBtnMsg = 'Inconsistent password';

            return;

        }

        //alert(player);
      /** var formData = new FormData();

        formData.append('User',user)
        formData.append('file',$scope.Photo)

        **/
        
        $http.post(base_url + 'welcome/signup', JSON.stringify(user))

            .then(
                function(response){
                    // success callback

                    if(response.data.isSuccessful) {

                        $scope.showSignupForm = false;

                        $scope.showSuccessForm = true;

                        $scope.signupModalTitle = 'Success'

                    }else {

                        $scope.backgroundColor = 'red';

                        $scope.signUpBoxMsg = response.data.responseMessage;

                        $scope.signupBtnMsg = response.data.responseMessage;

                    }

                },
                function(response){
                    // failure callback

                    $scope.backgroundColor = 'red';

                    $scope.signUpBoxMsg = response.data.responseMessage;

                    $scope.signupBtnMsg = response.data.responseMessage

                }
            );
    }

});

// this service redirects the logged in user to the index page
app.service('swithToIndexUrl', function($http) {

    this.switch = function () {

        window.location.href = "/";

       /** $http.get('http://localhost:8080/switch/to/index')

            .then(
                function (response) {

                },
                function (response) {

        });**/
    }
});
