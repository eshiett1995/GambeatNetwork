/**
 * Created by Oto-obong on 05/10/2017.
 */

dashboardapp.factory('Friends', ['$http', function ($http) {

    var friendsModel = {};

    friendsModel.getFriends = function () {

        $http.get("/dashboard/friends").then(function(response) {

            return response.data;

        }, function(response) {

            return response.data;

        });

    }

    friendsModel.getFriendsOnline = function () {

        $http.get("/dashboard/friendsonline").then(function(response) {

            return response.data;

        }, function(response) {

            return response.data;

        });

    }

    return friendsModel;

}]);

