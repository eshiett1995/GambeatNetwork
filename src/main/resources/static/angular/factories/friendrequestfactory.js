/**
 * Created by Oto-obong on 05/10/2017.
 */

dashboardapp.factory('FriendRequests', ['$http', function ($http) {

    var friendRequestsModel = {};


    // methods to get friend requests
    friendRequestsModel.getFriendRequests = function () {

        $http.get("/dashboard/friendRequest").then(function(response) {

            if(response.data.isSuccessful){

                return response.data;

            }else{


            }

        }, function(response) {

            if(response.data.isSuccessful){

                return response.data;

            }else{


            }

        });

    }


    // methods to accept a friends requests
    friendRequestsModel.acceptFriendRequest = function (username) {

        $http.get("/dashboard/friendrequest/accept/" + username ).then(function(response) {

            if(response.data.isSuccessful){

                return response.data;

            }else{


            }

        }, function(response) {

            if(response.data.isSuccessful){

                return response.data;

            }else{


            }

        });

    }

    // methods to reject a friends requests
    friendRequestsModel.acceptFriendRequest = function (username) {

        $http.get("/dashboard/friendrequest/accept/" + username ).then(function(response) {

            if(response.data.isSuccessful){

                return response.data;

            }else{


            }

        }, function(response) {

            if(response.data.isSuccessful){

                return response.data;

            }else{


            }

        });

    }

    return friendRequestsModel;

}]);