/**
 * Created by Oto-obong on 11/10/2017.
 */

dashboardapp.directive('flipCard',
    function(){
        return {
            restrict: 'EA',
            link: function ($scope, element,attrs) {
                $('.card.hover').hover(function(){
                    $(this).addClass('flip');
                },function(){
                    $(this).removeClass('flip');
                });

                $(".animate-number").each(function() {
                    var value = $(this).data('value');
                    var duration = $(this).data('animation-duration');

                    $(this).animateNumbers(value, true, duration, "linear");

                });

                //animate progress bars
                $('.animate-progress-bar').each(function(){
                    var progress =  $(this).data('percentage');

                    $(this).css('width', progress);
                });



            }
        };
    });
