/**
 * Created by Oto-obong on 29/09/2017.
 */

dashboardapp.config(['$routeProvider','$locationProvider',function($routeProvider, $locationProvider) {
    $routeProvider
        .when("/home", {

            templateUrl : "angular/partials/home.html",
            controller : "HomePage"
        })

    .when("/social/profile", {
            templateUrl : "angular/partials/my-profile.html"
        })

    .when("/social/friends", {

            templateUrl : "angular/partials/friends.html"
        })

    .when("/message", {

            templateUrl : "angular/partials/message.html"
        })

        .otherwise({
        redirectTo: 'home'
    });

     $locationProvider.hashPrefix('');
     //$locationProvider.html5Mode({enabled:true, requireBase:false });


}]);