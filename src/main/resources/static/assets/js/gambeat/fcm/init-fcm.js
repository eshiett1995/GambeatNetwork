
var config = {
    apiKey: "AIzaSyDMb72FpGKWiqz1jrP3WjQ4wGRCT-GWCAA",
    authDomain: "gambeat-network.firebaseapp.com",
    databaseURL: "https://gambeat-network.firebaseio.com",
    projectId: "gambeat-network",
    storageBucket: "gambeat-network.appspot.com",
    messagingSenderId: "522848658418"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

navigator.serviceWorker.register('assets/js/gambeat/fcm/firebase-messaging-sw.js').then(registration => {
    messaging.useServiceWorker(registration);
});

messaging.usePublicVapidKey("BF1-Kx34MGw09Ax-ESL08NRd3f_K7ovD-y4Ygvy0e0Lb6Q9vlpnxR3vo2xmeYKr74gVdnwxzoGwHuini9gZfChs");

messaging.requestPermission().then(function() {
    console.log('Notification permission granted.');

    if(isTokenSentToServer()){

        getRegistrationToken();

    }else{

        getRegistrationToken();

    }

}).catch(function(err) {
    console.log('Unable to get permission to notify.', err);
});


function getRegistrationToken() {

    messaging.getToken().then(function(currentToken) {
        if (currentToken) {

            console.log(currentToken);

            setTokenSentToServer(true);

            sendTokenToServer(currentToken);

        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            setTokenSentToServer(false);
        }
    }).catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
        setTokenSentToServer(false);
    });
}


function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}

function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') === '1';
}

function sendTokenToServer(currentToken) {

    var firebaseModel = {};

    firebaseModel.token = currentToken;

    post(base_url + "firebase/register/token", firebaseModel, function (data) {

        console.log(data);

    })

}

