
    var baseUrl = "http://localhost:8080";

    var self;

    var app = $.sammy('#content', function () {

        this.use(Sammy.JSON);


        this.get('#/home', function (context) {

            startProgressBar();
            self = this;
            $.ajax({
                url: base_url + "home/stats",
                type: 'GET',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {


                    console.log(data);

                    saveUserDetailsOffline(data.user);
                    context.app.swap('');
                    context.load('partials/home.html')
                        .appendTo(context.$element())
                        .then(function (content) {

                             ko.applyBindings(homeModel(data.playerStats),document.getElementById('home'));
                             initCard();
                             genericFunctions();
                             //setInterval(getPlayerStats(), 600000);
                             endProgressBar();

                        });

                },

                error: function (msg) {

                    endProgressBar();

                }
            });
        });

        this.get('#/activities/match-stats', function (context) {

            startProgressBar();

            self = this;

                    context.app.swap('');
                    context.load('partials/match-stats.html')
                        .appendTo(context.$element())
                        .then(function (content) {

                                $('#basicDataTable').DataTable({
                                    "processing":true,
                                    "serverSide":true,
                                    "ajax": {
                                        url: base_url + "activities/match-stats",
                                        cache: false,
                                        type: 'GET',
                                    }

                                });

                           /** ko.applyBindings(homeModel(data.playerStats),document.getElementById('match-stats'));
                            initCard();
                            genericFunctions();
                            setInterval(getPlayerStats(), 600000); **/
                           endProgressBar();

                        });


        });

        this.get("#/social/profile", function (context) {

            startProgressBar();
            $.ajax({
                url: base_url + "social/profile",
                type: 'GET',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {
                    context.app.swap('');
                    context.load('partials/my-profile.html')
                        .appendTo(context.$element())
                        .then(function (content) {
                            ko.applyBindings(PlayerProfileModel(data.user),document.getElementById('my-profile'));
                            initCard();
                            genericFunctions();
                            endProgressBar();
                        });
                }
            });

        });

        this.get("#/social/friends", function (context) {
            startProgressBar();
            $.ajax({
                url: base_url + "social/friends/0",
                type: 'GET',
                contentType:'application/json',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {

                    if(data.numberOfUsers > 0) {

                        context.app.swap('');
                        context.load('partials/friends.html')
                            .appendTo(context.$element())
                            .then(function (content) {

                                var FM = new friendsListModel(data);

                                FM.init(data);

                                ko.applyBindings(FM,document.getElementById('friends'));

                                genericFunctions();

                                endProgressBar();

                                $('.friendsPage').twbsPagination({

                                    totalPages: data.numberOfPages,

                                    visiblePages: 5,

                                    onPageClick: function (event, page) {

                                        FM.pagination(page);

                                    }
                                });
                            });

                    }else{

                        context.app.swap('');
                        context.load('partials/no-friends.html')
                            .appendTo(context.$element())
                            .then(function (content) {
                                ko.applyBindings(noFriendsListModel(),document.getElementById('no-friends'));

                                initChatBox();

                                genericFunctions();

                                endProgressBar();

                            });

                    }

                }
            });
        });

        this.get("#/social/referrals", function (context) {

            startProgressBar();

            $.ajax({
                url: baseUrl + "/social/referrals/0",
                type: 'GET',
                contentType:'application/json',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {

                    if(data.numberOfUsers > 0) {

                        context.app.swap('');
                        context.load('partials/referrals.html')
                            .appendTo(context.$element())
                            .then(function (content) {
                                ko.applyBindings(referralsListModel(data),document.getElementById('referrals'));
                                initChatBox();
                                genericFunctions();
                                paginationFunction("referralsPage", data.numberOfPages);
                            });
                    }else{

                        context.app.swap('');
                        context.load('partials/no-referrals.html')
                            .appendTo(context.$element())
                            .then(function (content) {
                                ko.applyBindings(referralsListModel(data),document.getElementById('no-referrals'));
                                initChatBox();
                                genericFunctions();
                                endProgressBar();
                            });

                    }
                }
            });
        });

        this.get("#/message", function (context) {
            startProgressBar();
            $.ajax({
                url: base_url + "message/all/0",
                type: 'GET',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {
                    context.app.swap('');
                    context.load('partials/message.html')
                        .appendTo(context.$element())
                        .then(function (content) {
                            ko.applyBindings(messageModel(data),document.getElementById('message'));

                            initChatBox();

                            genericFunctions();

                            endProgressBar();
                        });
                }
            });
        });

        this.get("#/message/:username", function (context) {
            startProgressBar();
            $.ajax({
                url: base_url + "message/all/0",
                type: 'GET',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {
                    context.app.swap('');
                    context.load('partials/message.html')
                        .appendTo(context.$element())
                        .then(function (content) {
                            ko.applyBindings(messageModel(data),document.getElementById('message'));

                            initChatBox();

                            genericFunctions();

                            endProgressBar();
                        });
                }
            });
        });

        this.get("#/social/referrer", function (context) {
            startProgressBar();
            $.ajax({
                url: base_url + "social/referrer",
                type: 'GET',
                contentType:'application/json',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {
                    if(data.responseMessage !== "No referrer found!") {

                        context.app.swap('');
                        context.load('partials/referrers-profile.html')
                            .appendTo(context.$element())
                            .then(function (content) {
                                ko.applyBindings(referrersProfileModel(data), document.getElementById('referrers-profile'));

                                initCard();

                                genericFunctions();

                                endProgressBar();
                            });

                    }else{

                        context.app.swap('');
                        context.load('partials/no-referrer.html')
                            .appendTo(context.$element())
                            .then(function (content) {
                                ko.applyBindings(referrersProfileModel(data), document.getElementById('no-referrer'));

                                genericFunctions();

                                endProgressBar();

                            });

                    }
                }
            });
        });

        this.get("#/social/friends/:username", function (context) {
            startProgressBar();
            $.ajax({
                url: base_url + "social/users/" + this.params['username'] ,
                type: 'GET',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {

                    context.app.swap('');
                    context.load('partials/users-profile.html')
                        .appendTo(context.$element())
                        .then(function (content) {

                            ko.applyBindings(usersProfileModel(data), document.getElementById('users-profile'));

                            initCard();

                            genericFunctions();

                            endProgressBar();
                        });

                }
            });
        });

        this.get("#/social/users/:username", function (context) {
            startProgressBar();
            $.ajax({
                url: base_url + "social/users/" + this.params['username'],
                type: 'GET',
                contentType:'application/json',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {
                    context.app.swap('');
                    context.load('partials/users-profile.html')
                        .appendTo(context.$element())
                        .then(function (content) {
                            ko.applyBindings(usersProfileModel(data),document.getElementById('users-profile'));
                            initCard();

                            genericFunctions();

                            endProgressBar();
                        });
                }
            });
        });

        this.get("#/search/query::username", function (context) {

            startProgressBar();

            var searchKeyWord = this.params['username'];


            $.ajax({
                url: base_url + "dashboard/search/" + this.params['username'] + "/0",
                type: 'GET',
                contentType:'application/json',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {
                    context.app.swap('');
                    context.load('partials/search.html')
                        .appendTo(context.$element())
                        .then(function (content) {


                            var SM = new searchModel();

                            SM.init(data);

                            ko.applyBindings(SM,document.getElementById('search'));
                            initCard();
                            genericFunctions();

                            $('.searchPage').twbsPagination({
                                totalPages: data.totalPages,
                                visiblePages: 5,
                                onPageClick: function (event, page) {

                                    startProgressBar();

                                    SM.pagination(searchKeyWord, page);
                                }
                            });
                        });
                }
            });

        });

        this.get("#/logout", function (context) {
            startProgressBar();
            $.ajax({
                url: base_url + "signout",
                type: 'GET',
                contentType:'application/json',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {

                    if(data.isSuccessful){

                        endProgressBar();

                        window.location.href = "/";

                    }else{

                        endProgressBar();

                        self.redirect('#/home');

                        showNotification('top-right','error','connection error',350,  2000);
                    }
                }
            });
        });

        this.get("#/account/deposit", function (context) {
            startProgressBar();
            context.app.swap('');
            context.load('partials/deposit.html')
                .appendTo(context.$element())
                .then(function (content) {
                    ko.applyBindings(depositModel(),document.getElementById('deposit'));

                    genericFunctions();

                    endProgressBar();
                });
        });

        this.get("#/account/withdraw", function (context) {

            startProgressBar();

            $.ajax({
                url: base_url + "account/withdraw",
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function (data) {
                    console.log(data);
                    context.app.swap('');
                    context.load('partials/withdraw.html')
                        .appendTo(context.$element())
                        .then(function (content) {
                            ko.applyBindings(withdrawModel(data), document.getElementById('deposit'));

                            genericFunctions();

                            endProgressBar();
                        });
                }
            });
        });

        this.get('#/account/transactions', function (context) {
            startProgressBar();
            self = this;
            $.ajax({
                url: base_url + "home/stats",
                type: 'GET',
                dataType: 'json',
                headers: createAuthorizationTokenHeader(),
                success: function(data) {



                    saveUserDetailsOffline(data.user);
                    context.app.swap('');
                    context.load('partials/transactions.html')
                        .appendTo(context.$element())
                        .then(function (content) {

                            ko.applyBindings(transactionsModel(),document.getElementById('transactions'));
                             initCard();

                             genericFunctions();

                             setInterval(getPlayerStats(), 600000);

                             endProgressBar();

                        });

                }
            });
        });


        this.notFound = function(){

            self.redirect('#/home')
        }
    });

    app.run('#/home');

