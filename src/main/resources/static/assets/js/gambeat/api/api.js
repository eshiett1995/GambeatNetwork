


    function getPaginatedFriendsList(pageNumber) {

        $.ajax({
            url: baseUrl + "/social/friends/" + pageNumber,
            type: 'GET',
            contentType:'application/json',
            dataType: 'json',
            success: function(data) {
                ko.applyBindings(friendsListModel(data),document.getElementById('friends'));
                genericFunctions();
                paginationFunction("friendsPage",data.numberOfPages);
            }
        });

    }

    function getPaginatedReferralsList(pageNumber) {



        $.ajax({
            url: baseUrl + "/social/referrals/" + pageNumber,
            type: 'GET',
            contentType:'application/json',
            dataType: 'json',
            success: function(data) {
                ko.applyBindings(friendsListModel(data),document.getElementById('referrals'));
                paginationFunction("referralsPage", data.numberOfPages);

                endProgressBar();
            }
        });

    }

    function getPlayerStats() {

        $.ajax({
            url: base_url + "home/stats",
            type: 'GET',
            dataType: 'json',
            success: function(data) {

                homeModel(data.playerStats);

            }
        });

    }

    function getPlayerProfile() {

        $.ajax({
            url: base_url + "social/profile",
            type: 'GET',
            dataType: 'json',
            success: function(data) {

                PlayerProfileModel(data.user);
            }
        });
    }

    function performBasicUpdate(basicInfo) {

        $.ajax({
            url: base_url + "social/profile/update",
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: basicInfo,
            success: function(response) {

                // updates the view (on success or fail ) to show changes have been made or to revert changes
                getPlayerProfile();
                return response;

            }
        });

    }


