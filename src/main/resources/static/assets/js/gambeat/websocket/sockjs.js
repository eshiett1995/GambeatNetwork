



var stompClient = null;


function connect() {

    var socket = new SockJS('/websocket-example');

    stompClient = Stomp.over(socket);

    stompClient.connect({},function (frame) {

        console.log('Connected: ' + frame);

        stompClient.subscribe('/user/topic/friend/online', function (message) {

            ko.postbox.publish("friend-is-online", message.body);

        });

        stompClient.subscribe('/user/topic/friend/offline', function (message) {

            ko.postbox.publish("friend-has-gone-offline", message.body);

        });

        stompClient.subscribe('/user/topic/quick-chat/receive/message', function (message) {

            ko.postbox.publish("quick-chat-receive-message", message.body);


        });


        stompClient.subscribe('/user/topic/friend-request/receive/request', function (message) {

            ko.postbox.publish("new-friend-request", message.body);

        });


    },
   );
}

//connect();

function disconnect() {

    if (stompClient !== null) {

        stompClient.disconnect();
    }

}

function onError(error) {

}


