

var test_server = "http://213.199.135.14:8080/";

var localbox = "http://localhost:8080/";

var test_server_socket = "http://213.199.135.14:4000/";

var localbox_socket = "http://localhost:4000/";

var base_url =  localbox;

var socket_url = localbox_socket;

var countries = ["Åland Islands","Afghanistan","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua And Barbuda","Argentina","Bahamas","Bahrain","Bangladesh",
    "Barbados", "Belarus", "Belgium", "Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia","Botswana","Bouvet","Brazil","British Indian Ocean Territory","Brunei","Bulgaria","Burkina Faso","Burundi",
    "Cambodia","Cameroon", "Canada","Cape Verde","Central African Republic","Chad","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Congo","Congo, Democractic Republic",
    "Cook Islands","Costa Rica","Cote D'Ivoire (Ivory Coast)","Croatia (Hrvatska)","Cuba", "Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt",
    "El Salvador","Equatorial Guinea","Eritrea", "Estonia","Ethiopia", "Falkland Islands (Islas Malvinas)","Faroe Islands", "Fiji Islands","Finland","France","France, Metropolitan","French Guiana",
    "French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland", "Grenada", "Guadeloupe","Guam", "Guatemala", "Guernsey", "Guinea",
    "Guinea-Bissau", "Guyana", "Haiti", "Heard and McDonald Islands","Honduras", "Hong Kong S.A.R.","Hungary", "Iceland","India", "Indonesia","Iran", "Iraq","Ireland", "Isle of Man", "Israel", "Italy",
    "Jamaica","Japan", "Jersey","Jordan","Kazakhstan","Kenya", "Kiribati","Korea", "Korea, North","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia", "Libya","Liechtenstein",
    "Lithuania","Luxembourg", "Macau S.A.R.","Macedonia","Madagascar","Malawi", "Malaysia","Maldives", "Mali","Malta","Marshall Islands","Martinique", "Mauritania","Mauritius", "Mayotte","Mexico",
    "Micronesia","Moldova", "Monaco", "Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand",
    "Nicaragua","Niger","Nigeria","Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestinian Territory, Occupied","Panama","Papua new Guinea", "Paraguay", "Peru",
    "Philippines", "Pitcairn Island", "Poland",  "Portugal", "Puerto Rico", "Qatar","Reunion","Romania","Russia","Rwanda","Saint Helena","Saint Kitts And Nevis","Saint Lucia",
    "Saint Pierre and Miquelon","Saint Vincent And The Grenadines","Samoa","San Marino","Sao Tome and Principe", "Saudi Arabia","Senegal", "Serbia","Seychelles","Sierra Leone","Singapore",
    "Sint Maarten","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia And The South Sandwich Islands","Spain", "Sri Lanka","Sudan","Suriname",
    "Svalbard And Jan Mayen Islands","Swaziland","Sweden","Switzerland", "Syria","Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo","Tokelau","Tonga", "Trinidad And Tobago",
    "Tunisia", "Turkey","Turkmenistan","Turks And Caicos Islands", "Tuvalu",	"Uganda",	"Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands",
    "Uruguay","Uzbekistan","Vanuatu","Vatican City State (Holy See)","Venezuela","Vietnam","Virgin Islands (British)","Virgin Islands (US)","Wallis And Futuna Islands","WESTERN SAHARA","Yemen","Zambia",
    "Zimbabwe",
];