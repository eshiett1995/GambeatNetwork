
var userLocalStorageKey = "user";

function saveToLocalStorage(storageName, storageObject) {

    localStorage.setItem(storageName, storageObject);
}

function getFromLocalStorage(storageName) {

    return localStorage.getItem(storageName);

}

function removeFromLocalStorage(storageName) {

    localStorage.removeItem(storageName);

}


function saveUserToLocalStorage(userObject) {

    saveToLocalStorage(userLocalStorageKey, userObject);

}

function getUserFromLocalStorage() {

    return getFromLocalStorage(userLocalStorageKey);

}


function removeUserFromLocalStorage() {

    removeFromLocalStorage(userLocalStorageKey);

}