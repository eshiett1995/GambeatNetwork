
// VARIABLES =============================================================
var TOKEN_KEY = "jwtToken";



function setJwt(token) {

    localStorage.setItem(TOKEN_KEY, token);
}

function getJwt() {

    return localStorage.getItem(TOKEN_KEY);
}


function createAuthorizationTokenHeader() {

    var token = getJwt();

    if (token) {

        return {"Authorization": "Bearer " + token};

    } else {

        return {};

    }
}

function shake(element) {

    $(element).shake({
        count: 4,
        distance: 15,
        duration: 100
    });

}

function toast(type, title, timer ) {

    const toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: timer
    });

    toast({
        type: type,
        title: title
    })

}

function post(url,data,callback) {

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        headers: createAuthorizationTokenHeader(),
        data: data,

        success: function(response) {

            callback(true,response);

        },

        error: function (err) {

            callback(false,err);

        }
    });

}





function gambeatGetRequest(url, callback){

    alert(createAuthorizationTokenHeader());

    $.ajax({
        url: url,
        type: 'GET',
        contentType:'application/json',
        dataType: 'json',
        headers: createAuthorizationTokenHeader(),
        success: function(data) {

            console.log(data);

            callback(true, data);

        },

        error: function (msg) {

            alert("here")

            console.log(msg)

            callback(false, msg);

        }
    });

}


function initCard() {

    $('.card.hover').hover(function () {
        $(this).addClass('flip');
    }, function () {
        $(this).removeClass('flip');
    });

    $(".animate-number").each(function () {
        var value = $(this).data('value');
        var duration = $(this).data('animation-duration');

        $(this).animateNumbers(value, true, duration, "linear");

    });

    //animate progress bars
    $('.animate-progress-bar').each(function () {
        var progress = $(this).data('percentage');

        $(this).css('width', progress);
    });

}

function initChatBox() {

    var contentHeight = $('#content').height();
    var chatInboxHeight = contentHeight - 178;
    var chatContentHeight = contentHeight - 178 - 200;

    var setChatHeight = function () {
        $('.chat-inbox').css('height', chatInboxHeight);
        $('#chat-content').css('height', chatContentHeight);
    };

    setChatHeight();

    $(window).resize(function () {
        contentHeight = $('#content').height();
        chatInboxHeight = contentHeight - 178;
        chatContentHeight = contentHeight - 178 - 200;

        setChatHeight();
    });

    $(".chat-inbox").niceScroll({
        cursorcolor: '#000000',
        zindex: 999999,
        bouncescroll: true,
        cursoropacitymax: 0.4,
        cursorborder: '',
        cursorborderradius: 0,
        cursorwidth: '5px'
    });

    $("#chat-content").niceScroll({
        cursorcolor: '#000000',
        zindex: 999999,
        bouncescroll: true,
        cursoropacitymax: 0.4,
        cursorborder: '',
        cursorborderradius: 0,
        cursorwidth: '5px'
    });

    $('.chat-inbox .chat-actions > span').tooltip({
        placement: 'top',
        trigger: 'hover',
        html: true,
        container: 'body'
    });

    $('#initialize-search').click(function () {
        $('.chat-search').toggleClass('active').focus();
    });

    $(document).click(function (e) {

        /*<![CDATA[*/
        if (($(e.target).closest("#initialize-search").attr("id") != "initialize-search") && $(e.target).closest("#chat-search").attr("id") != "chat-search") {
            $('#chat-search').removeClass('active');
        }
        /*]]>*/
    });

    $(window).mouseover(function () {
        $(".chat-inbox").getNiceScroll().resize();
        $("#chat-content").getNiceScroll().resize();
    });


}

function genericFunctions() {

    // Toggles the mmenu side chat and the left side chat bar, so they dont open up together

    $('#mmenu').on(
        "opened.mm",
        function () {

            $(".box").css({"right": -15});

            if ($("#sidebar, #navbar").hasClass("collapsed")) {


            } else {

                /*<![CDATA[*/

                $("#sidebar").addClass("collapsed"),
                    $("#navbar").addClass("collapsed"),

                    isMobile.any() ? $("#content").css($("#sidebar").hasClass("collapsed") ? {
                        paddingLeft: "55px",
                        display: "block"
                    } : {
                        paddingLeft: "265px",
                        display: "none"
                    }) : $("#sidebar").hasClass("collapsed") ? $(window).width() < 1024 ? $("#content").animate({left: "0px"}, 150) : $("#content").animate({paddingLeft: "55px"}, 150) : $(window).width() < 1024 ? $("#content").animate({left: "210px"}, 150) : $("#content").animate({paddingLeft: "265px"}, 150)

                /*]]>*/


            }


        }
    );

    $('#mmenu').on(
        "closed.mm",
        function () {

            $(".box").css({"right": 0});

            /*<![CDATA[*/

            if ($("#sidebar, #navbar").hasClass("collapsed")) {

                $("#sidebar").removeClass("collapsed"),
                    $("#navbar").removeClass("collapsed"),

                    isMobile.any() ? $("#content").css($("#sidebar").hasClass("collapsed") ? {
                        paddingLeft: "55px",
                        display: "block"
                    } : {
                        paddingLeft: "265px",
                        display: "none"
                    }) : $("#sidebar").hasClass("collapsed") ? $(window).width() < 1024 ? $("#content").animate({left: "0px"}, 150) : $("#content").animate({paddingLeft: "55px"}, 150) : $(window).width() < 1024 ? $("#content").animate({left: "210px"}, 150) : $("#content").animate({paddingLeft: "265px"}, 150)

            }

            /*]]>*/
        }
    );

}

function paginationFunction(page, pageTotal) {

    if(page === "referralsPage") {

        var obj = $('.referralsPage').twbsPagination({
            totalPages: pageTotal,
            visiblePages: 2,
            onPageClick: function (event, page) {

                getPaginatedReferralsList(page-1);

            }
        });

    }else{



    }

}

function referralsListModel(data) {

    var referralsListPage = ko.mapping.fromJS(data);

    referralsListPage.name = ko.observable(localStorage.name);

    referralsListPage.groupedReferrals = ko.computed(function() {

        var rows = [];

        referralsListPage.referrals().forEach(function(user, i) {
            // whenever i = 0, 3, 6 etc, we need to initialize the inner array
            if (i % 3 == 0)
                rows[i/3] = [];

            rows[Math.floor(i/3)][i % 3] = user;
        });

        return rows;
    });



    return referralsListPage;

}

function referrersProfileModel(data) {

    var usersProfile = ko.mapping.fromJS(data);

    usersProfile.user.fullName = ko.pureComputed(function() {

        return usersProfile.user.firstName() + " " + usersProfile.user.lastName();

    });

    usersProfile.user.playerStats.percentageWin = ko.pureComputed(function() {

        if(usersProfile.user.playerStats.matchPlayed()=== 0){

            return 0;

        }else {

            return (usersProfile.user.playerStats.wins() / usersProfile.user.playerStats.matchPlayed()) * 100;
        }

    });

    usersProfile.user.playerStats.percentageProfit =  ko.pureComputed(function() {

        if(usersProfile.user.playerStats.wallet() > 0.00){

            return (usersProfile.user.playerStats.profit() / usersProfile.user.playerStats.wallet()) * 100;

        }else {

            return 0;

        }

    });

    usersProfile.mutualFriendsPercentage =  ko.pureComputed(function() {

        if(usersProfile.mutualFriendsCount() > 0){

            return (usersProfile.mutualFriendsCount() / usersProfile.friendsCount()) * 100;

        }else {

            return 0;

        }

    });

    usersProfile.addFriend = function() {

        showNotification('top-right','info','Awaiting response', 250, 2000);

    };


    return usersProfile;

}

function showSuccessfulAlert(title, notification) {

    swal({
        title: title,
        type: 'success',
        text: notification,
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Close',
        allowOutsideClick: false
    })

}

function showFailureAlert(title, notification) {

    swal({
        title: title,
        type: 'error',
        text: notification,
        showCancelButton: false,
        confirmButtonColor: '#d63948',
        confirmButtonText: 'Close',
        allowOutsideClick: false
    })

}

function showNotification(position,type,text, width, timer) {

    swal({
        position: position,
        type: type,
        text: text,
        width: width,
        showConfirmButton: false,
        timer: timer
    }).catch(swal.noop)
}

function showSimpleMessage(title,message) {

    swal(title,message);

}

function saveUserDetailsOffline(user) {

    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.

        //console.log(user);

        localStorage.name = user.firstName;

        localStorage.lastname = user.lastName;

        localStorage.username = user.userName;

        localStorage.id = user.id;

    } else {
        // Sorry! No Web Storage support..
    }

}


var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
}

function nextPrev(n) {

    console.log(n == 1 && !validateForm());
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:

    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
        //...the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function validateForm() {

    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");

    for (i = 0; i < y.length; i++) {

        if (y[i].value == "") {

            y[i].className += " invalid";

            valid = false;
        }
    }

    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
}

function fixStepIndicator(n) {


    console.log("it got ere");

    var i, x = document.getElementsByClassName("step");


    for (i = 0; i < x.length; i++) {
        console.log("hello");
        x[i].className = x[i].className.replace(" active", "");
    }

    x[n].className += " active";
}

function startProgressBar() {

    NProgress.start();
    
}

function endProgressBar() {

    NProgress.done();
    
}

