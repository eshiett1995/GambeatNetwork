ko.bindingHandlers.widget = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var elemObservable = valueAccessor();
        if (ko.isObservable(elemObservable)) {
            elemObservable(element);
        }
    }
};


ko.bindingHandlers.hasBeenSeen = {
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        // First get the latest data that we're bound to
        var value = valueAccessor();

        // Next, whether or not the supplied model property is observable, get its current value
        var valueUnwrapped = ko.unwrap(value);

        $("#chat-content").scroll(function (event) {

            var container = $(".chat-content");
            var contHeight = container.height();
            var contTop = container.scrollTop();
            var contBottom = contTop + contHeight ;

            var elemTop = $(element).offset().top - container.offset().top;
            var elemBottom = elemTop + $(element).height();

            var isTotal = (elemTop >= 0 && elemBottom <=contHeight);
            //var isPart = ((elemTop < 0 && elemBottom > 0 ) || (elemTop > 0 && elemTop <= container.height())) && partial ;

            if(isTotal){

                var message = ko.utils.arrayFirst(bindingContext.$root.friendsAndMessagesModelPage, function (item) {

                    if(Number(item.friend.id()) === Number(bindingContext.$root.presentChat().id())){

                        alert("food")

                        // check for the  message in the messaging model
                        ko.utils.arrayFirst(item.messages(), function (message) {

                            if(Number(message.id()) === Number(valueUnwrapped.id())){

                                alert("closure")

                                // if the message is unread do something
                                if(message.read() === false){

                                    alert("destiny")

                                    var updatedMessage = message;

                                    updatedMessage.read(true);

                                    item.messages.replace(message,updatedMessage);

                                    console.log("done");

                                    bindingContext.$root.totalNumberOfUnreadMessages(bindingContext.$root.totalNumberOfUnreadMessages() -1);

                                    item.unreadMessagesCount(item.unreadMessagesCount() -1);

                                }else{

                                    alert("sexy back 2")

                                }

                            }else{

                                alert("sexy back")
                            }

                        })

                    }else{

                        alert("didnt work")

                    }

                });

            }

        });


    }
};


ko.bindingHandlers.makeActive = {

    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        // First get the latest data that we're bound to
        var value = valueAccessor();

        // Next, whether or not the supplied model property is observable, get its current value
        var valueUnwrapped = ko.unwrap(value);



        if(Number(bindingContext.$root.presentChat().id())  === Number(valueUnwrapped.friend.id()) && String(bindingContext.$root.presentChat().userName())  === String(valueUnwrapped.friend.userName())){


            $('#chat-inbox').scrollTo(element, 1000, { axis:'y' });

            $("#chat-inbox a").removeClass("active");

            $(element).addClass("active");

        }else {

            console.log("so i am trying");

            if (valueUnwrapped.isNewButNoMessageHasBeenSent() && valueUnwrapped.hasInitMessage() && Number(valueUnwrapped.messages.length) < Number(1)) {

                bindingContext.$root.messageModels.remove(function (item) {

                    return Number(item.friend.id()) === Number(valueUnwrapped.friend.id());

                });

                $(element).parent().remove();


            } else if (valueUnwrapped.isNewButNoMessageHasBeenSent() && valueUnwrapped.hasInitMessage() && Number(valueUnwrapped.messages.length) > Number(0)) {

                // todo rearrange message

                bindingContext.$root.messageModels.sort(function (left, right) {
                    return Number(left.messages()[left.messages().length - 1].dateCreated()) === Number(right.messages()[right.messages().length - 1].dateCreated()) ? 0 : (Number(left.messages()[left.messages().length - 1].dateCreated()) < Number(right.messages()[right.messages().length - 1].dateCreated()) ? -1 : 1)

                });

            }


        }
    }
};


ko.bindingHandlers.executeOnEnter = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var callback = valueAccessor();
        $(element).keypress(function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
            if (keyCode === 13) {
                callback.call(viewModel);
                return false;
            }
            return true;
        });
    }
};

ko.bindingHandlers.chaii = {

    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        // First get the latest data that we're bound to
        var value = valueAccessor();

        // Next, whether or not the supplied model property is observable, get its current value
        var valueUnwrapped = ko.unwrap(value);


        var friend = ko.utils.arrayFirst(bindingContext.$root.messageModels(), function (item) {

            if(Number(item.friend.id()) === Number(valueUnwrapped.friend.id())){

                console.log("it go errrrr");

               return item;

            }

        });

        if(friend.messages() === undefined || friend.messages().length <1){

            $(element).text("");

        }else{

            $(element).text(friend.messages()[friend.messages().length - 1].message())
        }




    }
};
