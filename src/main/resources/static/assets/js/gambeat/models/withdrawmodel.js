/**
 * Created by Oto-obong on 9/10/2018.
 */

/**
 *
initWithdrawal: ƒ ()
transactionModel:
    responseModel: ƒ c()
transaction: ƒ c()
withdrawModel:
    accountNumber: ƒ c()
availableBalance: ƒ c()
bankCode: ƒ c()
viaEmail: ƒ c()
withdrawalAmount: ƒ c()
__proto__: Object
__ko_mapping__: {ignore: Array(0), include: Array(1), copy: Array(0), observe: Array(0), mappedProperties: {…}, …}
__proto__: Object
__proto__: Object
 *
 **/


var withdrawModel = function(data){


    var c = [1,2,3,4,5,5];

    var model = {};

    model.transactionModel = ko.mapping.fromJS(data);

    console.log(model.transactionModel);

    model.transactionModel.withdrawModel.viaEmail("true");

    model.showInputBankDetailsDialog = function () {

        $.confirm({

            title: 'Bank detail',
            content: function(){

                var self = this;

                self.setContent('');

                return $.ajax({
                    url: "https://api.payant.ng/banks",
                    dataType: 'json',
                    method: 'get'
                }).done(function (response) {

                }).fail(function(){

                    toast("error", "Oops! an error occurred",5000)

                }).always(function(){

                });
            },


            contentLoaded: function(data, status, xhr){

                this.setContentAppend(
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Enter something here</label>' +
                    '<br>' +
                    '<select id="banks-dropdown" class="bank-dropdown form-control">' +
                    '</select>' +
                    '<label>Account Number</label>' +
                    '<input type="number" class="account-number form-control" required />' +
                    '</div>' +
                    '</form>'
                );

                var bankSelectElement = document.getElementById("banks-dropdown");
                data.data.forEach(function (element) {

                    var option = document.createElement("option");
                    option.value= element.bankCode;
                    option.innerHTML = element.bankName;
                    bankSelectElement.appendChild(option);
                })

            },
            onContentReady: function(){

            },

            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-blue',
                    action: function () {
                        var accountNumber = this.$content.find('.account-number').val();

                        var selectedBank = this.$content.find('.bank-dropdown').val();

                        console.log(selectedBank);

                        if(selectedBank === null){

                            $.alert('Select a bank');

                            return false;

                        }else if(!accountNumber){

                            $.alert('provide an account number');

                            return false;


                        }else{

                            model.transactionModel.withdrawModel.bankCode(selectedBank);

                            model.transactionModel.withdrawModel.accountNumber(accountNumber);

                            console.log(model.transactionModel);

                            model.finalizeWithdrawalTransaction();

                        }


                    }
                },
                cancel: function () {
                    //close
                },
            },
        });

    };

    model.finalizeWithdrawalTransaction = function () {

        $.confirm({
            type: 'dark', theme:'material center-buttons',
            content: function() {
                var self = this;
                return $.ajax({
                    url: "http://localhost:8080/account/finalize/withdrawal",
                    dataType: 'json',
                    method: 'post',
                    contentType: "application/json",
                    headers: createAuthorizationTokenHeader(),
                    data: ko.toJSON(model.transactionModel)
                }).done(function (data){
                    alert("it works");
                }).fail(function (){
                    alert("it doesnt works");
                }).always(function (){
                    // xxxxxxx
                });
            },
        });




    };

    model.initWithdrawal = function () {

        post(base_url + "account/initiate/withdrawal", ko.toJSON(model.transactionModel), function (isSuccessful,response) {

            if(isSuccessful){

              console.log(response);

              model.transactionModel.transaction(response.transaction);

              model.showInputBankDetailsDialog();

            }else{

                toast("error","Oops! an error has occurred");

            }

        })

    };

    return model;

};