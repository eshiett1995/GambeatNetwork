/**
 * Created by Oto-obong on 11/11/2017.
 */


var searchModel = function(){

    var self = this;

    self.totalElements = ko.observable('');

    self.usersOnScreen = ko.observableArray();

    self.groupedUsers = ko.computed(function() {

        var rows = [];

        self.usersOnScreen().forEach(function(user, i) {
            // whenever i = 0, 3, 6 etc, we need to initialize the inner array
            if (i % 3 == 0)
                rows[i/3] = [];

            rows[Math.floor(i/3)][i % 3] = user;
        });

        return rows;


    },this);

    self.pagination = function(searchKeyWord, page){

        $.get(base_url + "dashboard/search/" + searchKeyWord + "/" + (page - 1), function(data, status){

            self.usersOnScreen(data.content);

            endProgressBar();
        });

    };

    self.init = function(data){

            self.totalElements(data.totalElements);

            self.usersOnScreen(data.content);

    }



};