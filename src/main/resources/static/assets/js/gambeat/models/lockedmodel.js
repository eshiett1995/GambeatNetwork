
function lockedModel() {

    var model = {};

    model.name = JSON.parse(getUserFromLocalStorage()).firstName;

    model.password = ko.observable('');

    model.unlock = function () {

        $("#unlock-btn").attr('disabled','disabled');

        $('#unlock-btn').empty();

        $('#unlock-btn').append('unlocking... <i class="fa fa-spinner fa-spin" style="margin-left: 5px"></i>');


        var password = model.password();


        post(base_url + "locked/unlock", password, function (isSuccessful, response) {

                if(isSuccessful){

                    if(response.isSuccessful){

                        window.location.href = "/";

                    }else{

                        toast("error", response.responseMessage, 5000);

                    }

                }else{

                    $("#unlock-btn").removeAttr('disabled');

                    $('#unlock-btn').empty();

                    $('#unlock-btn').append('unlock');


                    toast("error", "Ooops! an error occurred", 5000);

                }
        })
    };
    
    model.useDifferentAccount = function () {

        $("#unlock-btn").attr('disabled','disabled');

        $('#unlock-btn').empty();

        $('#unlock-btn').append('logging out... <i class="fa fa-spinner fa-spin" style="margin-left: 5px"></i>');


        get(base_url + "signout", function (isSuccessful, response) {

            if(isSuccessful){

                if(response.isSuccessful){

                    window.location.href = "/";

                }else{

                    toast("error", response.responseMessage, 5000);

                }


            }else{

                $("#unlock-btn").removeAttr('disabled');

                $('#unlock-btn').empty();

                $('#unlock-btn').append('Use different account');


                toast("error", "Ooops! an error occurred", 5000);

            }

        })
        
    };

    return model;
}
