function transactionsModel(data) {

    var model = {};

    model.initializeDatatable = function () {

    };

    $('#basicDataTable').DataTable({
        processing: true,
        serverSide: true,
        "ajax": {
            "url": base_url + "account/query/data-table/transaction",
            headers: createAuthorizationTokenHeader(),
            "data": function ( data ) {

            }
        },

        "columns": [
            { "data": "id", "name" : "ID", "title" : "ID"  },
            { "data": "transactiontype", "name" : "Type" , "title" : "Type"},
            { "data": "amount", "name" : "Amount" , "title" : "Amount"},
            { "data": "referenceID", "name" : "reference ID" , "title" : "reference ID"},


            { "class":"",
                "orderable": false,
                "data": "detail",
                "render": function ( data, type, full, meta ) {

                    var actionhtml='<button id="deleteBtn" style="margin: auto; display: block; background-color: hotpink "  type="button" class="btn purple-gradient btn-sm" >' + 'view detail' + '</button>';
                    return actionhtml;

                }},

        ]

    });


    $('#basicDataTable tbody').on('click', '#deleteBtn', function (){

        var $row = $(this).closest('tr');

        var data =  $('#basicDataTable').DataTable().row($row).data();

        showSimpleMessage("Detail",data['detail']);

       // showSuccessfulAlert('Detail', 'Your profile has been successfully updated!');

    });

    return model;

}


