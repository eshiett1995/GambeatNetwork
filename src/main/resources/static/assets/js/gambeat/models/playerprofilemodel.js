
function PlayerProfileModel(data) {

    var playerProfile = ko.mapping.fromJS(data);

    console.log(playerProfile);

    playerProfile.fullName = ko.pureComputed(function() {

        return playerProfile.firstName() + " " + playerProfile.lastName();

    });

    playerProfile.playerStats.percentageWin = ko.pureComputed(function() {

        if(playerProfile.playerStats.matchPlayed()=== 0){

            return 0;

        }else {

            return (playerProfile.playerStats.wins() / playerProfile.playerStats.matchPlayed()) * 100;
        }

    });


    playerProfile.playerStats.percentageLoss = ko.pureComputed(function () {

        if(playerProfile.playerStats.matchPlayed()=== 0){

            return 0;

        }else {

            return (playerProfile.playerStats.loss() / playerProfile.playerStats.matchPlayed()) * 100;
        }

    });

    playerProfile.playerStats.percentageProfit =  ko.pureComputed(function() {

        if(playerProfile.playerStats.wallet() > 0.00){

            return (playerProfile.playerStats.profit() / playerProfile.playerStats.wallet()) * 100;

        }else {

            return 0;

        }

    });

    playerProfile.playerStats.percentageExpenses =  ko.pureComputed(function() {

        if(playerProfile.playerStats.wallet() > 0.00){

            return (playerProfile.playerStats.expenses() / playerProfile.playerStats.wallet()) * 100;

        }else {

            return 0;
        }

    });

    playerProfile.currentPassword = ko.observable('');

    playerProfile.newPassword = ko.observable('');

    playerProfile.repeatNewPassword = ko.observable('');

    playerProfile.newEmail = ko.observable('');

    playerProfile.newPhoneNumber = ko.observable('');

    playerProfile.sendVia_Password = ko.observable("EMAIL");

    playerProfile.newUserName = ko.observable('');

    playerProfile.sendVia_UserName = ko.observable("EMAIL");







    playerProfile.basicUpdate = function () {

        var basicInfo = JSON.stringify(
            {
                firstName : playerProfile.firstName(),
                lastName : playerProfile.lastName(),
                gender : playerProfile.gender(),
                address : playerProfile.address(),
                country : playerProfile.country(),
                bio : playerProfile.bio()
            }); // prepare request data

        swal({
            text: 'Do you want to save the changes ?',
            type: 'question',
            showCancelButton: true,
            cancelButtonText:'No',
            customClass: '.',
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {

                    $.ajax({
                        url: base_url + "social/profile/update",
                        type: 'POST',
                        dataType: 'json',
                        headers: createAuthorizationTokenHeader(),
                        contentType: "application/json; charset=utf-8",
                        data: basicInfo,
                        success: function(response) {

                            // updates the view (on success or fail ) to show changes have been made or to revert changes
                            getPlayerProfile();

                            resolve(response)

                        }
                    });

                })
            },
            allowOutsideClick: false
        }).then(function (response) {

            if(response.value.isSuccessful){

                showSuccessfulAlert('Success', 'Your profile has been successfully updated!');

            }else{

                showFailureAlert('Failed',response.value.responseMessage )
            }

        })

    };

    playerProfile.updatePassword = function () {

        if(playerProfile.newPassword().trim().length === 0){

            toast("error", 'new password cannot be empty', 5000);

            return;
        }


        if(playerProfile.newPassword() !== playerProfile.repeatNewPassword()){

            toast("error",'the new passwords do not correlate ',5000);

            return;
        }

        var TemporaryStore = JSON.stringify(
            {
                presentValue : playerProfile.currentPassword(),

                value: playerProfile.newPassword(),

                sendVia: playerProfile.sendVia_Password()
            });

        var device = playerProfile.sendVia_Password() === 'EMAIL' ? 'email' : 'phone';

        swal({

            title: 'Do you want to update your password?',
            text: 'An OTP would be sent to your' + ' ' + device,
            type: 'question',
            showCancelButton: true,
            cancelButtonText:'No',
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {

                    $.ajax({
                        url: base_url + "social/profile/update/password",
                        type: 'POST',
                        dataType: 'json',
                        headers: createAuthorizationTokenHeader(),
                        contentType: "application/json; charset=utf-8",
                        data: TemporaryStore,
                        success: function(response) {

                            // updates the view (on success or fail ) to show changes have been made or to revert changes
                            getPlayerProfile();

                            resolve(response)

                        }
                    });

                })
            },
            allowOutsideClick: false
        }).then(function (response) {

            console.log(response)

            if(response.value.responseModel.isSuccessful){

                swal({
                    title: 'Enter your token',
                    input: 'text',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: function (token) {
                        return new Promise(function (resolve, reject) {

                            $.ajax({
                                url: base_url + "social/profile/update/token/" + response.value.temporaryStoreId,
                                type: 'POST',
                                dataType: 'json',
                                headers: createAuthorizationTokenHeader(),
                                contentType: "application/json; charset=utf-8",
                                data: token,
                                success: function(response) {

                                    // updates the view (on success or fail ) to show changes have been made or to revert changes
                                    getPlayerProfile();

                                    resolve(response)

                                }
                            });
                        })
                    },
                    allowOutsideClick: false
                }).then(function (response) {

                    if(response.value.isSuccessful){

                        showSuccessfulAlert("Successful", response.value.responseMessage);

                    }else{

                        showFailureAlert("Failed", response.value.responseMessage);

                    }
                })


            }else{

                toast('error',response.value.responseModel.responseMessage, 5000);
            }

        })



    };

    playerProfile.updateEmail = function () {

        if(playerProfile.newEmail().trim().length === 0){

            toast("error", 'please enter an email', 5000);

            return;
        }


        var TemporaryStore = JSON.stringify(
            {
                presentValue : "",

                value: playerProfile.newEmail(),

                sendVia: "SMS"
            });


        swal({

            title: 'Do you want to update your email?',
            text: 'An OTP would be sent to your mobile phone',
            type: 'question',
            showCancelButton: true,
            cancelButtonText:'No',
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {

                    $.ajax({
                        url: base_url + "social/profile/update/email",
                        type: 'POST',
                        dataType: 'json',
                        headers: createAuthorizationTokenHeader(),
                        contentType: "application/json; charset=utf-8",
                        data: TemporaryStore,
                        success: function(response) {

                            // updates the view (on success or fail ) to show changes have been made or to revert changes
                            getPlayerProfile();

                            resolve(response)

                        }
                    });

                })
            },
            allowOutsideClick: false
        }).then(function (response) {

            console.log(response)

            if(response.value.responseModel.isSuccessful){

                swal({
                    title: 'Enter your token',
                    input: 'text',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: function (token) {
                        return new Promise(function (resolve, reject) {

                            $.ajax({
                                url: base_url + "social/profile/update/token/" + response.value.temporaryStoreId,
                                type: 'POST',
                                dataType: 'json',
                                headers: createAuthorizationTokenHeader(),
                                contentType: "application/json; charset=utf-8",
                                data: token,
                                success: function(response) {

                                    // updates the view (on success or fail ) to show changes have been made or to revert changes
                                    getPlayerProfile();

                                    resolve(response)

                                }
                            });
                        })
                    },
                    allowOutsideClick: false
                }).then(function (response) {

                    if(response.value.isSuccessful){

                        showSuccessfulAlert("Successful", response.value.responseMessage);

                    }else{

                        showFailureAlert("Failed", response.value.responseMessage);

                    }
                })


            }else{

                toast('error',response.value.responseModel.responseMessage, 5000);
            }

        })



    };

    playerProfile.updatePhoneNumber = function () {

        if(playerProfile.newPhoneNumber().trim().length === 0){

            toast("error", 'please enter a phone number', 5000);

            return;
        }


        var TemporaryStore = JSON.stringify(
            {

                value: playerProfile.newPhoneNumber(),

            });


        swal({

            title: 'Do you want to update your phone number?',
            text: 'An OTP would be sent to your email',
            type: 'question',
            showCancelButton: true,
            cancelButtonText:'No',
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {

                    $.ajax({
                        url: base_url + "social/profile/update/phonenumber",
                        type: 'POST',
                        dataType: 'json',
                        headers: createAuthorizationTokenHeader(),
                        contentType: "application/json; charset=utf-8",
                        data: TemporaryStore,
                        success: function(response) {

                            // updates the view (on success or fail ) to show changes have been made or to revert changes
                            getPlayerProfile();

                            resolve(response)

                        }
                    });

                })
            },
            allowOutsideClick: false
        }).then(function (response) {

            if(response.value.responseModel.isSuccessful){

                swal({
                    title: 'Enter your token',
                    input: 'text',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: function (token) {
                        return new Promise(function (resolve, reject) {

                            $.ajax({
                                url: base_url + "social/profile/update/token/" + response.value.temporaryStoreId,
                                type: 'POST',
                                dataType: 'json',
                                headers: createAuthorizationTokenHeader(),
                                contentType: "application/json; charset=utf-8",
                                data: token,
                                success: function(response) {

                                    // updates the view (on success or fail ) to show changes have been made or to revert changes
                                    getPlayerProfile();

                                    resolve(response)

                                }
                            });
                        })
                    },
                    allowOutsideClick: false
                }).then(function (response) {

                    if(response.value.isSuccessful){

                        showSuccessfulAlert("Successful", response.value.responseMessage);

                    }else{

                        showFailureAlert("Failed", response.value.responseMessage);

                    }
                })


            }else{

                toast('error',response.value.responseModel.responseMessage, 5000);
            }

        })



    };

    playerProfile.updateUserName = function () {

        if(playerProfile.newUserName().trim().length === 0){

            toast("error", 'new user name cannot be empty', 5000);

            return;
        }


        var TemporaryStore = JSON.stringify(
            {
                value: playerProfile.newUserName(),

                sendVia: playerProfile.sendVia_UserName()

            });

        var device = playerProfile.sendVia_Password() === 'EMAIL' ? 'email' : 'phone';

        swal({

            title: 'Do you want to update your username?',
            text: 'An OTP would be sent to your' + ' ' + device,
            type: 'question',
            showCancelButton: true,
            cancelButtonText:'No',
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {

                    $.ajax({
                        url: base_url + "social/profile/update/username",
                        type: 'POST',
                        dataType: 'json',
                        headers: createAuthorizationTokenHeader(),
                        contentType: "application/json; charset=utf-8",
                        data: TemporaryStore,
                        success: function(response) {

                            // updates the view (on success or fail ) to show changes have been made or to revert changes
                            getPlayerProfile();

                            resolve(response)

                        }
                    });

                })
            },
            allowOutsideClick: false
        }).then(function (response) {

            console.log(response)

            if(response.value.responseModel.isSuccessful){

                swal({
                    title: 'Enter your token',
                    input: 'text',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: function (token) {
                        return new Promise(function (resolve, reject) {

                            $.ajax({
                                url: base_url + "social/profile/update/token/" + response.value.temporaryStoreId,
                                type: 'POST',
                                dataType: 'json',
                                headers: createAuthorizationTokenHeader(),
                                contentType: "application/json; charset=utf-8",
                                data: token,
                                success: function(response) {

                                    // updates the view (on success or fail ) to show changes have been made or to revert changes
                                    getPlayerProfile();

                                    resolve(response)

                                }
                            });
                        })
                    },
                    allowOutsideClick: false
                }).then(function (response) {

                    if(response.value.isSuccessful){

                        showSuccessfulAlert("Successful", response.value.responseMessage);

                    }else{

                        showFailureAlert("Failed", response.value.responseMessage);

                    }
                })


            }else{

                toast('error',response.value.responseModel.responseMessage, 5000);
            }

        })



    };




    return playerProfile;
}