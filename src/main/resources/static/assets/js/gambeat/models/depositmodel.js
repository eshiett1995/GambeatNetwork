/**
 * Created by Oto-obong on 9/10/2018.
 */


var depositModel = function(){

    var model = {};

    model.depositAmount = ko.observable(200);

    model.transactionModel =  {

        transaction : {

            amount: ko.observable(200),

        },

    };

    model.depositMoney = function (){

        post(base_url + "account/deposit/save", ko.toJSON(model.transactionModel), function (isSuccessful, transactionModel) {

            alert("here");

            console.log(transactionModel);

            if(isSuccessful){

                payWithPayant(transactionModel, function (response) {

                    transactionModel.transaction.referenceID = response.reference_code;

                    post(base_url + "account/deposit/update", JSON.stringify(transactionModel), function (isSuccessful, transactionModel) {

                        // reset the default amount depositable back to normal
                        model.transactionModel.transaction.amount(200);

                        toast("success", "Deposit successful", 5000)

                    })

                });

            }else{

                toast("error", "Oops! an error just occurred");

            }

        } )

    };

    return model;

};