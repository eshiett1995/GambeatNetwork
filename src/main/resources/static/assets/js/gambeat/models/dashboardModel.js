
const Clock = (freq) => {
    let active = null;
    const now = ko.observable(moment());
    const tick = () => now(moment());
    const loop = () => {
        active = setTimeout(loop, freq);
        tick();
    }

    return {
        now: ko.pureComputed(now), // read only
        start: loop,
        stop: () => clearInterval(active)
    }
};

var onlineFriends;

var dashboardNavModels;


function friendsOnlineModel(data,mapping) {

    onlineFriends = ko.mapping.fromJS(data,mapping);


    onlineFriends.scrollElement =  $(".direct-chat-messages");

    onlineFriends.sending_message = ko.observable("");
    
    onlineFriends.scrolling = function (data,event) {

        var element = event.target;

        if(element.scrollTop === 0){

            if(onlineFriends.presentChat().pageable().number + 1 < onlineFriends.presentChat().pageable().totalPages){

                onlineFriends.loadMessages(onlineFriends.presentChat().id(),onlineFriends.presentChat().pageable().number + 1);

            }

        }

        var gridTop = 0,
            gridBottom = $(element).outerHeight();

            // On each scroll check if `td` is in interested viewport
            $('#direct-chat-holder .received-message').each(function() {
                var thisTop = $(this).offset().top;

                // Check if this element is in the interested viewport
                if (thisTop >= gridTop && (thisTop - $(this).height()) <= gridBottom) {
                    $(this).css('background', 'red');

                    var boundedMessage = ko.dataFor(this);

                    if(!boundedMessage.read){

                        //console.log(boundedMessage)

                        var friend = ko.utils.arrayFirst(onlineFriends.friendsOnline(), function (item) {

                            return item.id() === onlineFriends.presentChat().id();

                        });

                        if(friend){

                            var readBoundedMessage = boundedMessage;

                            readBoundedMessage.read = true;

                            readBoundedMessage.dateCreated = onlineFriends.convertTimeForServer(readBoundedMessage.dateCreated);

                            readBoundedMessage.receiver.dateCreated = onlineFriends.convertTimeForServer(readBoundedMessage.receiver.dateCreated);

                            readBoundedMessage.receiver.playerStats.dateCreated = onlineFriends.convertTimeForServer(readBoundedMessage.receiver.playerStats.dateCreated);

                            readBoundedMessage.sender.dateCreated = onlineFriends.convertTimeForServer(readBoundedMessage.sender.dateCreated);

                            readBoundedMessage.sender.playerStats.dateCreated = onlineFriends.convertTimeForServer(readBoundedMessage.sender.playerStats.dateCreated);

                            friend.messages.replace(boundedMessage, readBoundedMessage);

                            // since the server already pulls out a list of unread messages count, i just had to minus it by 1, noting that this has been read
                            friend.unreadMessagesCount(friend.unreadMessagesCount() - 1);

                            // since the server already pulls out a list of unread messages count, i just had to minus it by 1, noting that this has been read
                            onlineFriends.presentChat().unreadmessages_count(friend.unreadMessagesCount());

                            ko.postbox.publish("new-read-message", "1");

                            onlineFriends.updateMessageOnServerAsRead(readBoundedMessage);
                        }

                    }

                }
            });

    };

    onlineFriends.presentChat = ko.computed(function () {

        var message = {

            chatstart: ko.observable(false),

            loadingMessages: ko.observable(false),

            id: ko.observable(0),

            friend : ko.observable(),

            username : ko.observable(""),

            messages : ko.observableArray([]),

            pageable : ko.observable(''),

            unreadmessages_count : ko.observable(0)

        };

        return message;

    });




    onlineFriends.sendMessage = function () {



        var friend = ko.mapping.toJS(onlineFriends.presentChat().friend());

        friend.userType = friend.userType.$name;

        friend.onlineStatus = friend.onlineStatus.$name;

        friend.gender = friend.gender.$name;


        var user = JSON.parse(getUserFromLocalStorage());

        user.userType = user.userType.$name;

        user.onlineStatus = user.onlineStatus.$name;

        user.gender = user.gender.$name;

        var message = {

            receiver : friend,

            sender: user,

            read: false,

            message : onlineFriends.sending_message(),

            messageStatus: "DELIVERED",
        };

        stompClient.send("/app/quick-chat/send/message",{},JSON.stringify(message));

        message.read = true;

        message.dateCreated = new Date().getTime();


        onlineFriends.presentChat().messages.push(message);

        onlineFriends.sending_message("");

        onlineFriends.scrollToBottomOfChatPanel();

    };

    onlineFriends.updateMessageOnServerAsRead = function (message) {

        stompClient.send("/app/quick-chat/seen/message",{},JSON.stringify(message))

    };

    onlineFriends.startChat = function(friend) {

        onlineFriends.presentChat().id(friend.id());

        onlineFriends.presentChat().friend(friend);

        onlineFriends.presentChat().messages([]);

        if(friend.hasInitMessage()){

            onlineFriends.presentChat().messages(friend.messages());

        }else{

            onlineFriends.loadMessages(friend.id(),0);


        }

    };

    onlineFriends.loadMessages = function(id,page) {


        var friend = ko.utils.arrayFirst(onlineFriends.friendsOnline(), function (item) {

            return item.id() === id;

        });


        /**
         * responsible for showing messages the user has presently while fetching messages
         * from the server.
         */
        onlineFriends.presentChat().messages(friend.messages());

        onlineFriends.presentChat().loadingMessages(true);

        $.get(base_url + "dashboard/message/" + id + "/" + page, function(data, status){


          if(data.successful) {

              friend.hasInitMessage(true);

              friend.presentPageDetail(data.messagesPage);

              if(page === 0){

                  /**
                   * Checks if this is the first page to load and if it is clears
                   * all the messages the present user has and pushes the new page in.
                   */
                  onlineFriends.presentChat().messages([]);

                  friend.messages([]);

                  data.messagesPage.content.forEach(function (item) {

                      friend.messages.unshift(item);

                  });


              }else {

                  data.messagesPage.content.forEach(function(item) {

                      friend.messages.unshift(item);

                  });



              }


              onlineFriends.presentChat().messages(friend.messages());

              if(page === 0){onlineFriends.scrollToBottomOfChatPanel();}

              onlineFriends.presentChat().unreadmessages_count(friend.unreadMessagesCount());

              onlineFriends.presentChat().pageable(friend.presentPageDetail());

          }else {}

            onlineFriends.presentChat().loadingMessages(false);

        });

    };

    onlineFriends.convertTime = function (milliseconds) {

        if(moment(milliseconds).isSame(new Date().getTime(), 'day')){

            return moment(milliseconds).format("hh:mm a");

        }else{

            if(moment(milliseconds).isSame(new Date().getTime(), 'year') === true) {

                return moment(milliseconds).format("DD MMM hh:mm a")

            }else{ return moment(milliseconds).format("DD MMM YYYY hh:mm a") }

        }

    };

    onlineFriends.convertTimeForServer = function (milliseconds) {

        return moment(milliseconds).format("DD-MM-YYYY HH:mm:ss");

    };

    onlineFriends.scrollToBottomOfChatPanel = function () {

        //methods enables the chat panel to automatically scroll down when a message appears on the chat box

        $(".direct-chat-messages").scrollTo("max");

    };





    ko.postbox.subscribe("friend-is-online", function(message) {

        var mapping = {
            'friendsOnline': {
                create: function(options) {

                    return new friendModel(options.data);
                }
            }
        };

        var model = {friendsOnline : [JSON.parse(message)]};

        var mappedObservable = ko.mapping.fromJS(model,mapping);


        ko.utils.arrayForEach(mappedObservable.friendsOnline(), function (item) {

            onlineFriends.friendsOnline.push(item);

        });


    }, onlineFriends, true);

    ko.postbox.subscribe("friend-has-gone-offline", function(message) {

        var friend = JSON.parse(message);

        ko.utils.arrayForEach(onlineFriends.friendsOnline(), function (item) {

            if(Number(item.id()) === Number(friend.id)){

                onlineFriends.friendsOnline.remove(item);

            }

        });


    }, onlineFriends, true);

    ko.postbox.subscribe("quick-chat-receive-message", function(message) {


        $.playSound('assets/audio/notify.mp3');

        var receivedMessage = JSON.parse(message);


        if(Number(onlineFriends.presentChat().id()) === Number(receivedMessage.sender.id)){

            ko.utils.arrayForEach(onlineFriends.friendsOnline(), function (friend) {

                if(Number(friend.id()) === Number(receivedMessage.sender.id)){

                    friend.messages.push(receivedMessage);

                    onlineFriends.presentChat().messages(friend.messages());

                    onlineFriends.scrollToBottomOfChatPanel();
                }

            });

        }else {

            ko.utils.arrayForEach(onlineFriends.friendsOnline(), function (friend) {

                if (Number(friend.id()) === Number(receivedMessage.sender.id)) {

                    friend.messages.push(receivedMessage);

                    /**
                     * since the server already pulled out a list of unread messages fromt his guy, i am just adding one to it,
                     *cause the message is new and his chat box isnt on this present user
                     **/
                    friend.unreadMessagesCount(friend.unreadMessagesCount() + 1);

                    ko.postbox.publish("new-unread-message", "1");

                } else {



                }
            });
        }



    }, onlineFriends, true);

    return onlineFriends;

}










































function friendModel(data) {

        ko.mapping.fromJS(data, {}, this);

        var friend = ko.mapping.fromJS(data);

        friend.hasInitMessage = ko.observable(false);

        this.presentPageDetail = ko.observable();

        friend.messages = ko.observableArray([]);

        friend.countUnreadMessages = function() {

            var unreadMsg = [];

            ko.utils.arrayForEach(friend.messages(), function (item) {


                if(item.read === false){

                    unreadMsg.push(item);
                }

            });

            friend.unreadMessagesCount(unreadMsg.length);

        };

    }











function dashboardNavModel(data) {


    data.chats.forEach(function (chat) {

        var date = moment(chat.dateCreated).format();

        chat.dateCreated = moment(date).format("x");

    });

    var mapping = {
        'chats': {
            create: function(options) {

                return new chatModel(options.data);
            }
        }
    };


    dashboardNavModels = ko.mapping.fromJS(data, mapping);

    console.log(dashboardNavModels);

    dashboardNavModels.clock = Clock(1000);
    dashboardNavModels.creationTime = moment();
    dashboardNavModels.launchTime = moment().add(1, "minute");



    saveUserToLocalStorage(ko.mapping.toJSON(dashboardNavModels.user));

    dashboardNavModels.searchTxt = ko.observable('');

    //dashboardNavModels.topFiveFriendRequests = ko.observableArray([]);

    dashboardNavModels.search = function () {

        if (dashboardNavModels.searchTxt() === '') {


        } else {

            window.location.replace("#/search/query:" +  dashboardNavModels.searchTxt());

        }

    };


    dashboardNavModels.friendrequest_count = ko.computed(function() {

        return dashboardNavModels.friendRequests().length;

    });

    dashboardNavModels.acceptFriendRequest = function(friendRequest) {

        $.get( base_url + "dashboard/friendrequest/accept/" + friendRequest.sender.userName(), function(data, status){


            if(data.isSuccessful){


                dashboardNavModels.friendRequests.remove(friendRequest);

            }else {

                // todo here you put a error catch

            }

        });

    };

    dashboardNavModels.rejectFriendRequest = function(friendRequest) {

        $.get(base_url + "dashboard/friendrequest/decline/" + friendRequest.sender.userName(), function(data, status){

            if(data.isSuccessful){

                dashboardNavModels.topFiveFriendRequests.remove(friendRequest);

            }else {

                // todo here you put a error catch
            }

        });

    };

    dashboardNavModels.convertTimeFromServer = function (datetime) {

        var date = moment(datetime).format();

        return moment(date).format("x");

    };

    dashboardNavModels.openChatMessage = function (chat) {

        alert("it worked");

        console.log(chat);

    };





    /**
     *  general topic to update model on any new unread message
     */
    ko.postbox.subscribe("new-unread-message", function(message) {

        dashboardNavModels.unreadMessagesCount(dashboardNavModels.unreadMessagesCount() + 1)

    }, dashboardNavModels, true);


    /**
     *  general topic to update model on any new read message
     */
    ko.postbox.subscribe("new-read-message", function(message) {

        dashboardNavModels.unreadMessagesCount(dashboardNavModels.unreadMessagesCount() - 1)

    }, dashboardNavModels, true);


    /**
     *  general topic to update model on any new friend request
     */
    ko.postbox.subscribe("new-friend-request", function(message) {

        dashboardNavModels.friendRequests.unshift(ko.mapping.fromJSON(message));

    }, dashboardNavModels, true);

    dashboardNavModels.clock.start();

    return dashboardNavModels;
}




function chatModel(data) {

    ko.mapping.fromJS(data, {}, this);

    this.relativeTime = ko.pureComputed(function () {

        dashboardNavModels.launchTime.from(dashboardNavModels.clock.now());

        return moment(parseInt(this.dateCreated())).from(dashboardNavModels.clock.now());

    }, this);



}
