/**
 * Created by Oto-obong on 11/11/2017.
 */


    function homeModel(data) {

        var playerStats = ko.mapping.fromJS(data);

        playerStats.percentageWin =  ko.pureComputed(function() {

            if(playerStats.matchPlayed()=== 0){

                return 0;

            }else {

                return (playerStats.wins() / playerStats.matchPlayed()) * 100;
            }

        });

        playerStats.percentageLoss = ko.pureComputed(function () {

            if(playerStats.matchPlayed()=== 0){

                return 0;

            }else {

                return (playerStats.loss() / playerStats.matchPlayed()) * 100;
            }

        });

        playerStats.percentageProfit =  ko.pureComputed(function() {

            if(playerStats.wallet() > 0.00){

                return (playerStats.profit() / playerStats.wallet()) * 100;

            }else {

                return 0;

            }

        });

        playerStats.percentageExpenses =  ko.pureComputed(function() {

            if(playerStats.wallet() > 0.00){

                return (playerStats.expenses() / playerStats.wallet()) * 100;

            }else {

                return 0;
            }

        });

        playerStats.percentageReadMessages =  ko.pureComputed(function() {

            if(playerStats.totalMessage() === 0){

                return 0;

            }else{

                return (playerStats.readMessage() / playerStats.totalMessage()) * 100;
            }

        });

        playerStats.percentageUnreadMessages =  ko.pureComputed(function() {

            if(playerStats.totalMessage() === 0){

                return 0;

            }else {

                return (playerStats.unreadMessage() / playerStats.totalMessage()) * 100;
            }

        });

        return playerStats;

    }


