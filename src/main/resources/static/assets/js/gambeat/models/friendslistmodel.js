
var friendsListModel = function(data) {

    var friendsListPage =  ko.mapping.fromJS(data);

    friendsListPage.friendsOnScreen = ko.observableArray();

    friendsListPage.numberOfUsers = ko.observable('');

    friendsListPage.name = ko.observable(localStorage.name);

    friendsListPage.groupedFriends = ko.computed(function() {

        var rows = [];

        friendsListPage.friendsOnScreen().forEach(function(user, i) {
            // whenever i = 0, 3, 6 etc, we need to initialize the inner array
            if (i % 3 == 0)
                rows[i/3] = [];

            rows[Math.floor(i/3)][i % 3] = user;
        });

        return rows;
    });


    friendsListPage.pagination = function(page){

        startProgressBar();

        $.get(base_url + "/social/friends/" + (page - 1), function(data, status){

            friendsListPage.friendsOnScreen(data.friends);

            endProgressBar();

        });



    }

    friendsListPage.init = function(data){

        friendsListPage.friendsOnScreen(data.friends);

        friendsListPage.numberOfUsers(data.numberOfUsers);

    };



    return friendsListPage;

};


var noFriendsListModel = function() {

    var friendsListPage = this;


    return friendsListPage;

}