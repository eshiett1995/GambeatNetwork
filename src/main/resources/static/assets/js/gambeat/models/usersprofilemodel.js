
function usersProfileModel(data) {

    var usersProfile = ko.mapping.fromJS(data);

    usersProfile.user.fullName = ko.pureComputed(function() {

        return usersProfile.user.firstName() + " " + usersProfile.user.lastName();

    });

    usersProfile.user.playerStats.percentageWin = ko.pureComputed(function() {

        if(usersProfile.user.playerStats.matchPlayed()=== 0){

            return 0;

        }else {

            return (usersProfile.user.playerStats.wins() / usersProfile.user.playerStats.matchPlayed()) * 100;

        }

    });


    usersProfile.user.playerStats.percentageProfit =  ko.pureComputed(function() {

        if(usersProfile.user.playerStats.wallet() > 0.00){

            return (usersProfile.user.playerStats.profit() / usersProfile.user.playerStats.wallet()) * 100;

        }else {

            return 0;

        }

    });

    usersProfile.mutualFriendsPercentage =  ko.pureComputed(function() {

        if(usersProfile.mutualFriendsCount() > 0){

            return (usersProfile.mutualFriendsCount() / usersProfile.friendsCount()) * 100;

        }else {

            return 0;

        }

    });

    usersProfile.pendingFriend = function(parent) {

        showNotification('top-right','info','Request pending', 250, 2500);

    };

    usersProfile.addFriend = function(parent) {

        swal({

            toast:true,
            position: 'top-right',
            width: 250,
            title: 'Requesting...',
            text: 'please hold on.',
            onOpen: () => {
                swal.showLoading()
            }
        }).then((result) => {

        });

        $.get( base_url + "dashboard/friendrequest/send/" + parent.user.userName(), function(data, status){

            console.log(data);

            if(data.isSuccessful){

                usersProfile.friendRequestStatus('PENDING');

                showNotification('top-right','success','request successful', 250, 2500);

            }else{

                showNotification('top-right','error','An error has occurred', 250, 2500);
            }

        });

    };

    usersProfile.removeFriend = function(parent) {

        swal({

            toast:true,
            position: 'top-right',
            width: 250,
            title: 'Removing...',
            text: 'please hold on.',
            onOpen: () => {
                swal.showLoading()
            }
        }).then((result) => {

        });

        $.get(base_url + "/dashboard/friendrequest/remove/" + parent.user.userName(), function(data, status){

            if(data.isSuccessful){

                usersProfile.friendRequestStatus('DECLINED');

                showNotification('top-right','success','user has been removed', 250, 2500);

            }else{

                showNotification('top-right','error','An error has occurred', 250, 2500);
            }

        });

    };


    return usersProfile;

}