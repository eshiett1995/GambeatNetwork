
function welcomePageModel() {

    var model = {};

    model.image = ko.observable('');

    model.user = {

        firstName: ko.observable(''),

        lastName: ko.observable(''),

        userName: ko.observable(''),

        email: ko.observable(''),

        DOB : ko.observable(''),

        password: ko.observable(''),

        re_password : ko.observable(''),

        gender: ko.observable("UNSPECIFIED"),

        address: ko.observable(''),

        country: ko.observable(''),

        picture: ko.observable('profile-photo.jpg'),

        phoneNumber: ko.observable(''),

        bio: ko.observable(''),

    };

    //

    /**model.user.address.subscribe(function (text) {

        if(text.charAt(0).trim() === ""){

            model.user.address(text.trim());

        }else{

            alert(text.charAt(0));
        }

    });**/




    model.countries = ko.observableArray(countries);

    model.genders =  ko.observableArray(['MALE', 'FEMALE',]);

    model.callingServer = {

        login : ko.observable(false),

        registration : ko.observable(false),

    };


    model.imageClickMethod = function () {

        $('#imageFilePickerInput').click()

    };

    model.fileUpload = function(data, e)
    {

        var _URL = window.URL || window.webkitURL;
        var file    = e.target.files[0];
        var reader  = new FileReader();
        img = new Image();

        img.onload = function () {

           /** if(Number(this.width) === Number(512) && Number(this.height) === Number(512)){

            }else{

                model.user.picture('');

                toast("error", "image dimension must be 512x512");

            } **/

        };

        reader.onloadend = function (onloadend_e)
        {
            var result = reader.result; // Here is your base 64 encoded file. Do with it what you want.

            //console.log(reader.result);

            //console.log(result);

        };

        if(file)
        {
            reader.readAsDataURL(file);

            img.src = _URL.createObjectURL(file);

            $("#imagePlaceHolder").attr("src",img.src);

            model.image(file);
        }
    };


    model.validateLogin = function (callback) {

        if(model.user.userName().trim() === ""){

            return callback(false,"Enter a user name");

        }

        if(model.user.password().trim() === ""){

            return callback(false,"Enter a password");

        }

        return callback(true,"validated!");

    };

    model.validateRegistration = function () {

        if(model.user.userName().trim() === ""){

            return callback(false,"Enter a user name");

        }

        if(model.user.userName().trim() === ""){

            return callback(false,"Enter a user name");

        }

        if(model.user.password().trim() === ""){

            return callback(false,"Enter a password");

        }

        return callback(true,"validated!");

    };



    
    model.login = function () {

        model.callingServer.login(true);

        model.validateLogin(function (isSuccessful, message) {

            if(!isSuccessful){

                toast(error, message, 3000);

                model.callingServer.login(false);

            }else{

                post(base_url + "welcome/login", ko.toJSON(model.user), function (isSuccessful,response) {

                    // checks if the call was successful from your box
                    if(isSuccessful) {

                        if (response.isSuccessful) {

                            setJwt(response.jwtToken);

                            window.location.href = "/";

                        } else {

                            toast("error", "User name and password is incorrect", 5000);

                        }

                    }else{

                        toast("error", "User name and password is incorrect", 5000);
                    }

                    model.callingServer.login(false);

                });


            }



        })


    };


    model.register = function (item, event) {

        model.callingServer.registration(true);

        var data = new FormData();

        data.append( 'file', model.image());

        data.append('model',JSON.stringify(ko.toJS(model.user)));

        $.ajax({

            url: base_url +"welcome/signup",
            type: 'POST',
            contentType: false,
            processData: false,
            data: data,

            success: function(response) {


                model.callingServer.registration(false);

                if(response.isSuccessful) {

                    $("#signup").modal("hide");

                    model.resetUserModel();

                    showNotification('center','success',response.responseMessage, 250, 10000);

                }else {

                    //this if statement was added because KO/thymeleaf disable reverts to when the previous button is hidden and next button is not hidden
                    if(model.currentTab() + 1 === document.getElementsByClassName("tab").length){

                        document.getElementById("prevBtn").style.display = "inline";

                        document.getElementById("nextBtn").innerHTML = "Submit";
                    }

                    toast("error",response.responseMessage, 5000);

                }


            },

            error: function (response) {

                model.callingServer.registration(false);

                toast("error",response.responseMessage, 5000);

                //this if statement was added because KO/thymeleaf disable reverts to when the previous button is hidden and next button is not hidden
                if(model.currentTab() + 1 === document.getElementsByClassName("tab").length){

                    document.getElementById("prevBtn").style.display = "inline";

                    document.getElementById("nextBtn").innerHTML = "Submit";
                }

            }



        });

    };



    // Current tab is set to be the first tab (0)
    model.currentTab = ko.observable(0);

     // Display the current tab

    model.showTab = function(n) {


        // This function will display the specified tab of the form ...
        var tabElementsArray = document.getElementsByClassName("tab");

        tabElementsArray[n].style.display = "block";
        // ... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (tabElementsArray.length - 1)) {

            document.getElementById("nextBtn").innerHTML = "Submit";

        }else if(n >= tabElementsArray.length){

            alert("this");


        }else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        // ... and run a function that displays the correct step indicator:
        model.fixStepIndicator(n)
    };

    model.nextPrev = function(n) {

        // This function will figure out which tab to display
        var tabElementsArray = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:

        if (n == 1 && ! model.validateForm()) return false;

        console.log("current tab " + model.currentTab() );

        // if you have reached the end of the form... :
        if (model.currentTab() + n >= tabElementsArray.length) {
            //...the form gets submitted:
            model.register();


        }else{

            // Hide the current tab:
            tabElementsArray[model.currentTab()].style.display = "none";
            // Increase or decrease the current tab by 1:
            model.currentTab(model.currentTab() + n);

            // Otherwise, display the correct tab:
            model.showTab(model.currentTab());
        }

    };

    model.validateForm = function() {

        var tabElementsArray, formInputsArray, index, valid = true;

        tabElementsArray = document.getElementsByClassName("tab");

        formInputsArray = tabElementsArray[model.currentTab()].querySelectorAll('input,select,textarea');

        for (index = formInputsArray.length - 1; index >= 0; index--) {

            // checks if the first character in the string is a white space
            if(formInputsArray[index].value.charAt(0).trim() === ""){

                shake('#signup-content');

                toast('error', 'input for' + " " + formInputsArray[index].getAttribute('id') + " " + "cannot start with a blank space", 5000);

                formInputsArray[index].className += " invalid";

                valid = false;

            };

            //check if password correlate
            if(formInputsArray[index].getAttribute('id') === 'password' || formInputsArray[index].getAttribute('id') === 'confirm password field'){

                if(model.user.password() != model.user.re_password()){

                    console.log(model.user.password());

                    console.log(model.user.re_password());

                    shake('#signup-content');

                    toast('error', 'inputs for password do not correlate', 5000);

                    formInputsArray[index].className += " invalid";

                    valid = false;

                }
            }

            formInputsArray[index].value.trim();

            if (formInputsArray[index].value.trim() == "" || !formInputsArray[index].checkValidity()) {

                model.showRegistrationFormValidationMessages(formInputsArray[index]);

                formInputsArray[index].className += " invalid";

                valid = false;
            }

        }

        if (valid) {

            document.getElementsByClassName("step")[model.currentTab()].className += " finish";
        }
        return valid; // return the valid status
    };

    model.fixStepIndicator = function(n) {

        var index, stepElementsArray = document.getElementsByClassName("step");


        for (index = 0; index < stepElementsArray.length; index++) {

            stepElementsArray[index].className = stepElementsArray[index].className.replace(" active", "");
        }

        stepElementsArray[n].className += " active";
    };

    model.showRegistrationFormValidationMessages = function (element) {

        if(element.validity.valueMissing){

            toast('error', 'input for' + " " + element.getAttribute('id') + " " + "cannot be empty", 5000);

        }else if(element.validity.rangeOverflow){

            toast('error', 'input for' + " " + element.getAttribute('id') + " " + "surpasses required range", 5000);

        }else if(element.validity.rangeUnderflow){

            toast('error', 'input for' + " " + element.getAttribute('id') + " " + "does not meet required range", 5000);

        }else if(element.validity.tooLong){

        toast('error', 'input for' + " " + element.getAttribute('id') + " " + "is longer that expected", 5000);

        }else if(element.validity.typeMismatch){

            toast('error', 'input for' + " " + element.getAttribute('id') + " " + "is of an invalid type", 5000);

        }else {

            toast('error', 'input for' + " " + element.getAttribute('id') + " " + "is invalid", 5000);

        }

    };

    model.resetUserModel = function () {

        model.user.firstName('');

        model.user.lastName('');

        model.user.userName('');

        model.user.email('');

        model.user.DOB('');

        model.user.password('');

        model.user.re_password('');

        model.user.gender("UNSPECIFIED");

        model.user.address('');

        model.user.country('');

        model.user.picture('profile-photo.jpg');

        model.user.phoneNumber('');

        model.user.bio('');

        document.getElementById("imageFilePickerInput").value = "";

    };



    model.showTab(model.currentTab());

    return model;
}