

function messageModel(data) {

    console.log(data);

    var friendAndMessageMapping = {

        'content': {
            create: function(options) {

                return new myFriendsAndMessages(options.data);
            }
        }

    };

    var messageModelMapping = {
        'friendsAndMessagesModelPage': {
            create: function(friendsAndMessagesModelPageData) {
                return ko.mapping.fromJS(friendsAndMessagesModelPageData.data, friendAndMessageMapping, {});
            }
        }
    };

    var model = ko.mapping.fromJS(data, messageModelMapping);

    console.log(model);


    /***************************************************************************************************/
    /// this are the observable to make the loader image show when loading a new set of inbox messages///
    /***************************************************************************************************/

    // this toggles between the Friends Panel and The recent message panel
    model.showFriendsPanel = ko.observable(false);

    model.loadingInboxMessages = ko.observable(false);

    model.loadingFriendsList = ko.observable(false);

    model.hideMessagesPanel = function () {

        model.showFriendsPanel(true);

    };

    model.showMessagesPanel = function () {

        model.showFriendsPanel(false);

    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////


    model.friendsModel = {totalFriendsCount : ko.observable(0), friends : ko.observableArray([]), pageDetail : ko.observable()};

    model.pressEnterToSend = ko.observable(false);

    model.messageToBeSent = ko.observable("");


    model.presentChat = ko.computed(function () {

        var pc = {

            loadingMessages: ko.observable(false),

            id: ko.observable(0),

            userName: ko.observable(''),

            friend : ko.observable(),

            messages: ko.observableArray([]),

            presentPageDetail: ko.observable(''),

            unreadmessages_count: ko.observable(0)

        };

        return pc;

    });

    model.startChat = function (friendAndMessageModel,event) {

        //$("#chat-inbox a").removeClass("active");

        //$(event.currentTarget).addClass( "active" );

            model.presentChat().loadingMessages(false);

            model.presentChat().id(friendAndMessageModel.friend.id());

            model.presentChat().friend(friendAndMessageModel.friend);

            model.presentChat().messages([]);

            if(friendAndMessageModel.hasInitMessage()){

                model.presentChat().presentPageDetail(friendAndMessageModel.presentPageDetail());

                model.presentChat().messages(friendAndMessageModel.messages());

                $("#chat-content").scrollTo("max");

            }else{

                model.presentChat().presentPageDetail('');

                model.loadMessages(friendAndMessageModel.friend.id(),-1, function (isSuccessful) {

                    if(isSuccessful){

                        $("#chat-content").scrollTo("max");
                    }
                    
                });

            }

            model.presentChat().unreadmessages_count(0);

    };

    model.startNewChat = function (friend,event) {

        console.log(friend)

        var friendAndMessage = ko.utils.arrayFirst(model.friendsAndMessagesModelPage.content(), function (item) {

            return Number(item.friend.id()) === Number(friend.id()) && String(item.friend.userName()) === String(friend.userName());

        });

        console.log(friendAndMessage)

        if(friendAndMessage != undefined || friendAndMessage != null){ // meaning there is already a messageModel that exists of this user

            model.presentChat().id(friendAndMessage.friend.id());

            model.presentChat().friend(friendAndMessage.friend);

            model.presentChat().messages([]);

            /**
             * hasInitMessage() is a method in each friendAndMessage Object which tells if the
             * friendAndMessage object has every fetched previous messages from the server before
             * now
             */
            if(friendAndMessage.hasInitMessage()) {

                model.presentChat().messages(friendAndMessage.messages());

                model.presentChat().presentPageDetail(friendAndMessage.presentPageDetail());

                model.showMessagesPanel(); // this function shows the message panel

            }else{

                model.loadMessages(friendAndMessage.friend.id(),-1,function (isSuccessful) {

                    if(isSuccessful){

                        model.friendsAndMessagesModelPage.content.remove(friendAndMessage);

                        model.friendsAndMessagesModelPage.content.unshift(friendAndMessage);

                        model.showMessagesPanel(); // this function shows the message panel

                        model.presentChat().id(friendAndMessage.friend.id());


                    }else{}

                })

            }

        }else{

            alert("it got here")

            model.presentChat().id(friend.id());

            model.presentChat().messages([]);

            //THIS METHOD TURNS THE FRIEND OBSERVABLE TO A MESSAGE OBSERVABLE SO IT CAN BE SHOWED THE LIST OF MESSAGES

            var newMessageModelObject = new Object();

            newMessageModelObject.friend = ko.mapping.fromJS(friend);

            newMessageModelObject.messages = ko.observableArray([]);


            var dummyObject = {content : ko.observableArray()};

            dummyObject.content.unshift(newMessageModelObject);



            var mapping = {

                'content': {
                    create: function(options) {

                        return new myFriendsAndMessages(options.data);
                    }
                }

            };

            var newMessagingModelMapping = ko.mapping.fromJS(dummyObject, mapping);

            alert("here")

            console.log(newMessagingModelMapping)

            ko.utils.arrayForEach(newMessagingModelMapping.content(), function (item) {

                item.isNewButNoMessageHasBeenSent(true);

                model.friendsAndMessagesModelPage.content.unshift(item);


            });

            //////////////////////////END OF PROGRAM/////////////////////////



        }

        model.showMessagesPanel(); // this function shows the message panel

    };

    model.convertTime = function (milliseconds) {

        if(moment(milliseconds).isSame(new Date().getTime(), 'day')){

            return moment(milliseconds).format("hh:mm a");

        }else{

            if(moment(milliseconds).isSame(new Date().getTime(), 'year') === true) {

                return moment(milliseconds).format("DD MMM hh:mm a")

            }else{ return moment(milliseconds).format("DD MMM YYYY hh:mm a") }

        }

    };

    model.convertTimeToDate = function (milliseconds) {

        if(moment(milliseconds).isSame(new Date().getTime(), 'day')){

            return "Today";

        }else{

            return moment(milliseconds).format("DD MMM YYYY");

        }

    };

    model.loadMessages = function(id,page,callback) {

        model.presentChat().loadingMessages(true);


        gambeatGetRequest(base_url + "message/" + id + "/" + page, function(isSuccessful,data){


            if(isSuccessful) {

                if (data.successful) {

                    var friendAndMessage = ko.utils.arrayFirst(model.friendsAndMessagesModelPage.content(), function (item) {

                        return Number(item.friend.id()) === Number(id);

                    });

                    model.presentChat().presentPageDetail(data.messagesPage);

                    friendAndMessage.presentPageDetail(data.messagesPage);

                    friendAndMessage.hasInitMessage(true);

                    console.log(friendAndMessage);

                    if (Number(page) === Number(-1)) {

                        //reverse the array so it would be be in descending form
                        data.messagesPage.content.reverse();

                        data.messagesPage.content.forEach(function (item) {

                            if (friendAndMessage.messages().length === 0) {

                                friendAndMessage.messages.unshift(ko.mapping.fromJS(item));

                            } else {

                                if (Number(item.id) != Number(friendAndMessage.messages()[0].id)) {

                                    //covert the item to observable or else there would be an error ;

                                    friendAndMessage.messages.unshift(ko.mapping.fromJS(item));

                                }

                            }

                        });

                    } else {

                        data.messagesPage.content.slice().reverse().forEach(function (item) {

                            //covert the item to observable or else there would be an error ;

                            friendAndMessage.messages.unshift(ko.mapping.fromJS(item));


                        });

                    }

                    //friendAndMessage.messages()
                    model.presentChat().messages(friendAndMessage.messages());

                    model.presentChat().loadingMessages(false);

                    callback(data.successful);

                } else {

                    model.presentChat().loadingMessages(false);

                    callback(data.successful);

                }

            }


        });

    };

    model.loadInboxMessage = function(page,callback) {

        model.loadingInboxMessages(true);

        $.get(base_url  + "message/all/" + page, function(data, status){

            if(data.successful) {

                model.messagesPage = ko.mapping.fromJS(data.messagesPage);


                var mapping = {

                    'messageModels': {
                        create: function(options) {

                            return new messagingModel(options.data);
                        }
                    }

                };

                var dummyMapping = ko.mapping.fromJS(data, mapping);

                ko.utils.arrayForEach(dummyMapping.messageModels(), function (item) { model.messageModels.push(item);});

                model.loadingInboxMessages(false);

                callback(data.successful);

            }else {
                // todo display the error message or write connection error.

                model.loadingInboxMessages(false);

                callback(data.successful);

            }

        });

    };

    model.loadFriends = function (page) {

        model.loadingFriendsList(true);

        gambeatGetRequest(base_url + "message/friends/" + page, function(isSuccessful,data){

            if(isSuccessful) {

                if (data.successful) {


                    model.friendsModel.pageDetail(ko.mapping.fromJS(data.friendsPage));

                    model.friendsModel.totalFriendsCount(data.numberOfUsers);


                    data.friends.forEach(function (item) {

                        model.friendsModel.friends.push(ko.mapping.fromJS(item));

                    });

                    model.loadingFriendsList(false)

                } else {


                }
            }

            //model.presentChat().loadingMessages(false);

        });

    };

    model.scrollingMessageBox = function (data,event) {

        var element = event.target;

       /** $.each( $(".chat-list .media"),function(i,e){
            model.checkInView($(e),false);
        }); **/


        if ($(element).scrollTop() === 0) //scrollTop is 0 based
        {
            if(model.presentChat().presentPageDetail().number - 1 < 0){

                alert("here 1")

            }else{

                alert("here 2")

                model.loadMessages(model.presentChat().id(),model.presentChat().presentPageDetail().number - 1);

            }


        }


    };

    model.scrollingInboxMessagesBox = function (data,event) {

        var element = event.target;

        /** $.each( $(".chat-list .media"),function(i,e){
            model.checkInView($(e),false);
        }); **/


        if (Number(element.scrollHeight - element.scrollTop -1) <= Number(element.offsetHeight)) //scrollTop is 0 based
        {
            if(model.messagesPage.number() - 1 < 0){


            }else{

                model.loadInboxMessage(model.messagesPage.number() - 1);

            }


        }


    };

    model.scrollingFriendsBox = function (data,event) {

        var element = event.target;

        if (Number(element.scrollHeight - element.scrollTop -1) <= Number(element.offsetHeight)) //scrollTop is 0 based
        {
            if(Number(model.friendsModel.pageDetail().number() + 1) ===  Number(model.friendsModel.pageDetail().totalPages())){



            }else{

                model.loadFriends(model.friendsModel.pageDetail().number() + 1);

            }


        }


    };

    model.isFirstConversationMessage = function (message) {


        var firstConversationMessage = model.presentChat().messages()[0];

        // todo this is temporary



            if (firstConversationMessage === message) {

                return true;

            } else {

                false;

            }

        
    };

    model.isFirstMessageOfANewDay = function (message) {

        var index = model.presentChat().messages.indexOf(message)

        if(index != -1 || index != 0){

            var dateToCompare = new Date(model.presentChat().messages()[index -1].dateCreated());

            var currentDate = new Date(message.dateCreated());


            if( dateToCompare.getFullYear() === currentDate.getFullYear() &&
                dateToCompare.getMonth() === currentDate.getMonth() &&
                dateToCompare.getDate() === currentDate.getDate())

            {

                return false;

            }else{ return true; }

        }

    };



    model.sendMessageOnEnterKeyPressed = function (data,event) {


            console.log(model.messageToBeSent());


    };

    model.sendMessage = function (data,event) {

        var user = JSON.parse(getUserFromLocalStorage());

        user.userType = user.userType.$name;

        user.onlineStatus = user.onlineStatus.$name;

        user.gender = user.gender.$name;

        var message = {

            id: 0,

            receiver : {

                id: model.presentChat().id(),
            },

            sender: user,

            message : model.messageToBeSent(),

            read: false,

        };

        stompClient.send("/app/messaging/send/message",{},JSON.stringify(message));

        message.read = true;

        message.dateCreated = new Date().getTime();

       //model.presentChat().messages.push(ko.mapping.fromJS(message));

        var friend = ko.utils.arrayFirst(model.messageModels(), function (item) {

            return Number(item.friend.id()) === Number(model.presentChat().id());

        });

        friend.messages.push(ko.mapping.fromJS(message));

        model.presentChat().messages(friend.messages());

        model.messageToBeSent("");

    };

    //calls this method calls the first set of friends.
    model.loadFriends(0);

    stompClient.connect({}, function (frame) {

        onlineFriends.getMessage = stompClient.subscribe('/user/topic/messaging/receive/message', function (message) {

            $.playSound('assets/audio/notify.mp3');

            var receivedFriendAndMessage = JSON.parse(message.body);

            //check if there is an existing friendandmessages model
            var foundFriendAndMessage = ko.utils.arrayFirst(model.messageModels(), function (item) {

                return Number(item.friend.id()) === Number(receivedFriendAndMessage.friend.id()) && String(item.friend.userName()) === String(receivedFriendAndMessage.friend.userName());

            });

            //if not found push it to top of the list (message is new so current user would be able to see it)

            if(foundFriendAndMessage === undefined){

                model.messageModels.unshift(foundFriendAndMessage);

            }else{

                // pushes the message from the friendandmessage model into the found friendandmessage model's messages
                foundFriendAndMessage.messages().push(receivedFriendAndMessage.messages[0]);


                model.friendsAndMessagesModelPage.content.slice(0,0,foundFriendAndMessage);

            }

        });

    });

    return model;

}



function myFriendsAndMessages(data) {

    ko.mapping.fromJS(data, {}, this);

    var messageModel = ko.mapping.fromJS(data);

    this.hasInitMessage = ko.observable(false);

    this.presentPageDetail = ko.observable('');

    this.isNewButNoMessageHasBeenSent = ko.observable(false);

}