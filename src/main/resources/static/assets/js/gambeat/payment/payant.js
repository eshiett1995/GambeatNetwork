

var payant_test_url = "https://api.demo.payant.ng";

function payantGetRequest(url, callback) {

    $.ajax({
        url: url,
        //data: { signature: authHeader },
        type: "GET",
       // beforeSend: function(xhr){xhr.setRequestHeader('X-Test-Header', 'test-value');},
        success: function(response) { callback(response); },

        error: function(response) { callback(response); }
    });
}


function payWithPayant(transactionModel, callback) {

    var handler = Payant.invoice({

        "key": "e87f1665d633277850a60282a7aff46bcc85cc27",

        "client": {
            "first_name": transactionModel.transaction.player.firstName,
            "last_name": transactionModel.transaction.player.lastName,
            "email": transactionModel.transaction.player.email,
            "phone": transactionModel.transaction.player.phoneNumber
        },
        "due_date": "12/30/2016",
        "fee_bearer": "account",
        "items": [
            {
                "item": "Gambeat",
                "description": "Gambeat Deposit",
                "unit_cost": transactionModel.transaction.amount,
                "quantity": "1"
            }
        ],
        callback: function(response) {

            callback(response);

        },
        onClose: function() {
            console.log('Window Closed.');
        }
    });

    handler.openIframe();
}

function getBanks(callback) {

    payantGetRequest("https://api.payant.ng/banks", function (result) {

        callback(result.status,result.data);

    })
}