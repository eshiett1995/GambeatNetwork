/**
 * Created by Oto-obong on 19/10/2017.
 */
$(document).ready(function() {




    var app = $.sammy('#content', function () {

        this.use(Sammy.JSON)


        this.get('#/home', function (context) {

            /**var loadOptions = { type: 'get', dataType: 'json', data: {query: variable}, };


             this.load('/home/stats', loadOptions)
             .then(function(items) {
                            $.each(this.json(items), function(i, item) {
                                console.log(item);
                            });
                        });**/




            /** $.ajax({
                      url: "localhost:8080/home/stats",
                      type: 'GET',
                      dataType: 'json', // added data type
                      success: function(res) {
                          console.log(res);
                          alert(res);
                      }
                  });**/



            /**$.get("localhost:8080/home/stats", function(data, status){

                        context.app.swap('');

                        context.render('partials/home.html')

                            .appendTo(context.$element())

                            .then(function(content) {

                                getFunctionality();

                                getPlayerStats();

                            });

                        if(data.isSuccessful){

                            dashBoardModel(data.friendsOnline)

                        }else{

                             /////////////////////////////////////////note here put growl notification error message

                        }

                    },"json");
             **/


            $('#content').load("partials/home.html");


        });

        this.get("#/social/profile", function (context) {
            context.app.swap('');
            context.load('partials/my-profile.html')
                .appendTo(context.$element())
                .then(function(content) {
                    getFunctionality();

                });
        });

        this.get("#/social/friends", function (context) {
            context.app.swap('');
            context.load('partials/friends.html')
                .appendTo(context.$element())
                .then(function(content) {
                    getFunctionality();

                });
        });

        this.get("#/message", function (context) {
            context.app.swap('');
            context.load('partials/message.html')
                .appendTo(context.$element())
                .then(function(content) {
                    getFunctionality();

                });
        });

        this.get("#/social/friends/:username", function (context) {
            context.log("another page with id = " + context.params["username"]);
            context.load('partials/users-profile.html')
                .appendTo(context.$element())
                .then(function(content) {
                    getFunctionality();

                });
        });

        this.get("#/social/users/:username", function (context) {
            context.log("another page with id = " + context.params["username"]);
            context.load('partials/users-profile.html')
                .appendTo(context.$element())
                .then(function(content) {
                    getFunctionality();

                });
        });
    });

    app.run('#/home')

    function getFunctionality () {


        $('.card.hover').hover(function(){
            $(this).addClass('flip');
        },function(){
            $(this).removeClass('flip');
        });

        $(".animate-number").each(function() {
            var value = $(this).data('value');
            var duration = $(this).data('animation-duration');

            $(this).animateNumbers(value, true, duration, "linear");

        });

        //animate progress bars
        $('.animate-progress-bar').each(function(){
            var progress =  $(this).data('percentage');

            $(this).css('width', progress);
        });

        // Toggles the mmenu side chat and the left side chat bar, so they dont open up together

        $('#mmenu').on(
            "opened.mm",
            function()
            {

                if($("#sidebar, #navbar").hasClass("collapsed")){


                }else{

                    $("#sidebar").addClass("collapsed"),
                        $("#navbar").addClass("collapsed"),

                        isMobile.any()?$("#content").css($("#sidebar").hasClass("collapsed")?{paddingLeft:"55px",display:"block"}:{paddingLeft:"265px",display:"none"}):$("#sidebar").hasClass("collapsed")?$(window).width()<1024?$("#content").animate({left:"0px"},150):$("#content").animate({paddingLeft:"55px"},150):$(window).width()<1024?$("#content").animate({left:"210px"},150):$("#content").animate({paddingLeft:"265px"},150)


                }


            }
        );

        $('#mmenu').on(
            "closed.mm",
            function()
            {

                if($("#sidebar, #navbar").hasClass("collapsed")){

                    $("#sidebar").removeClass("collapsed"),
                        $("#navbar").removeClass("collapsed"),

                        isMobile.any()?$("#content").css($("#sidebar").hasClass("collapsed")?{paddingLeft:"55px",display:"block"}:{paddingLeft:"265px",display:"none"}):$("#sidebar").hasClass("collapsed")?$(window).width()<1024?$("#content").animate({left:"0px"},150):$("#content").animate({paddingLeft:"55px"},150):$(window).width()<1024?$("#content").animate({left:"210px"},150):$("#content").animate({paddingLeft:"265px"},150)

                }
            }
        );


        var contentHeight = $('#content').height();
        var chatInboxHeight = contentHeight - 178;
        var chatContentHeight = contentHeight - 178 - 200;

        var setChatHeight = function() {
            $('#chat-inbox').css('height', chatInboxHeight);
            $('#chat-content').css('height', chatContentHeight);
        };

        setChatHeight();

        $(window).resize(function() {
            contentHeight = $('#content').height();
            chatInboxHeight = contentHeight - 178;
            chatContentHeight = contentHeight - 178 - 200;

            setChatHeight();
        });

        $("#chat-inbox").niceScroll({
            cursorcolor: '#000000',
            zindex: 999999,
            bouncescroll: true,
            cursoropacitymax: 0.4,
            cursorborder: '',
            cursorborderradius: 0,
            cursorwidth: '5px'
        });

        $("#chat-content").niceScroll({
            cursorcolor: '#000000',
            zindex: 999999,
            bouncescroll: true,
            cursoropacitymax: 0.4,
            cursorborder: '',
            cursorborderradius: 0,
            cursorwidth: '5px'
        });

        $('#chat-inbox .chat-actions > span').tooltip({
            placement: 'top',
            trigger: 'hover',
            html : true,
            container: 'body'
        });

        $('#initialize-search').click(function(){
            $('#chat-search').toggleClass('active').focus();
        });

        $(document).click(function(e) {
            if (($(e.target).closest("#initialize-search").attr("id") != "initialize-search") && $(e.target).closest("#chat-search").attr("id") != "chat-search") {
                $('#chat-search').removeClass('active');
            }
        });

        $(window).mouseover(function() {
            $("#chat-inbox").getNiceScroll().resize();
            $("#chat-content").getNiceScroll().resize();
        });





    }
    function getPlayerStats() {

        $.get(base_url + "/home/stats", function(data, status){

            if(data.isSuccessful){

                dashBoardModel(data.friendsOnline)

            }else{

            }
        });

    }
    function playerStatsModel(data) {

        var statsModel = ko.mapping.fromJS(data);

    }



















    window.MyCallback = function (data) {
        console.log(data);
    };

    crossroads.addRoute('/home', function () {


        $.ajax({
            url: base_url + "home/stats?callback?",
            type: 'GET',
            contentType: "application/javascript",
            dataType: 'jsonp', // added data type
            jsonCallback: "jsonp",
            crossDomain: true,
            data: {},
            success: function(res) {
                console.log(res);
                alert(res);
            }
        });


        $('#content').load("partials/home.html", function () {

            initcards();
        });

    });

    crossroads.addRoute('/social/profile', function () {

        $('#content').load('partials/my-profile.html');

    });

    window.addEventListener("hashchange", function () {
        var route = '/home';
        var hash = window.location.hash;
        if (hash.length > 0) {

            route = hash.split('#').pop();
        }

        crossroads.parse(route);
    });

    window.dispatchEvent(new CustomEvent("hashchange"));

    function initcards() {

        $('.card.hover').hover(function(){
            $(this).addClass('flip');
        },function(){
            $(this).removeClass('flip');
        });

        $(".animate-number").each(function() {
            var value = $(this).data('value');
            var duration = $(this).data('animation-duration');

            $(this).animateNumbers(value, true, duration, "linear");

        });

        //animate progress bars
        $('.animate-progress-bar').each(function(){
            var progress =  $(this).data('percentage');

            $(this).css('width', progress);
        });
    }










});

